'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DOMWrapper = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _wrapper = require('./wrapper.es6');

Object.defineProperty(exports, 'DOMWrapper', {
  enumerable: true,
  get: function get() {
    return _wrapper.DOMWrapper;
  }
});

var _slider = require('./slider.es6');

var _slider2 = _interopRequireDefault(_slider);

var _legacy = require('./legacy.es6');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _slider2.default;


if ((0, _legacy.hasjQuery)()) {
  var _window = window;
  var jQuery = _window.jQuery;

  jQuery.fn.plasmic = function (opt) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    for (var i = 0; i < this.length; i++) {
      if ((typeof opt === 'undefined' ? 'undefined' : _typeof(opt)) === 'object' && opt !== null || typeof opt === 'undefined') {
        this[i].plasmic = new _slider2.default(this[i], opt);
      } else if (typeof opt === 'string') {
        var _i$plasmic;

        if (opt.indexOf('slick') !== -1) {
          opt = opt.replace('slick', '');
          opt = opt[0].toLowerCase() + opt.slice(1);
        }

        return (_i$plasmic = this[i].plasmic)[opt].apply(_i$plasmic, args);
      }
    }

    return this;
  };

  if (!jQuery.fn.slick) jQuery.fn.slick = function (opt) {
    if ((typeof opt === 'undefined' ? 'undefined' : _typeof(opt)) === 'object' && opt !== null) opt.slickCompatible = true;

    for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    return this.plasmic.apply(this, [opt].concat(args));
  };
}