'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Event handling module. It allows any object or function to communicate by sending events.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * When possible, a native handling will be used (DOM events).
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * @module ./events.es6
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


exports.default = events;

var _utils = require('./utils.es6');

var _domhelpers = require('./domhelpers.es6');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var whitespaces = /\s+/;

function events() {
  var storage = new _utils.WeakStorage();

  var Handler = function () {
    /**
     * Handler.constructor();
     *
     * @param {Object|Function|HTMLElement} target    - object event handling will be attached to.
     * @param {Boolean}                     localOnly - indicated whether the native events (DOM events) will be dispatched
     */
    function Handler(target, localOnly) {
      _classCallCheck(this, Handler);

      if (!target || typeof target !== 'function' && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) !== 'object') throw new TypeError('Invalid elem passed');

      this.target = target;
      this.isElement = (0, _domhelpers.isHTMLElement)(target);
      this.listeners = {};
      this.localStorage = new _utils.WeakStorage();
      this.localOnly = localOnly && this.isElement;
    }

    /**
     * Handler.on() - adds an event listener.
     * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
     * @type {Function} listener - function called once an event was triggered
     * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
     * @return {Handler}
     */


    _createClass(Handler, [{
      key: 'on',
      value: function on(event, listener) {
        var _this = this;

        var data = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

        events.parseEvents(event).forEach(function (_ref) {
          var original = _ref.original;
          var formatted = _ref.formatted;

          var matchedListeners = _this.localStorage.get(listener);
          var boundListener = function boundListener() {
            var e = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

            e.data = data;
            listener.call(this, e);
          };

          if (!matchedListeners) _this.localStorage.set(listener, []);
          _this.localStorage.get(listener).push(boundListener);
          (_this.listeners[original] || (_this.listeners[original] = [])).push(boundListener);
          if (_this.isElement && !_this.localOnly) _this.target.addEventListener(formatted, boundListener);
        });

        return this;
      }

      /**
       * Handler.once() - adds an event listener that will be removed once it is called for a first time
       * It takes the same arguments as the Handler.on() method.
       *
       * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {Function} listener - function called once an event was triggered
       * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
       * @return {Handler}
       */

    }, {
      key: 'once',
      value: function once(event, listener, data) {
        var _this2 = this;

        var callback = function callback() {
          listener();
          _this2.off(event, callback);
        };

        return this.on(event, callback, data);
      }

      /**
       * Handler.off() - removes a previously attached listener or listeners
       *
       * @type {String}   event     - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {Function} [listener] - when defined, function will try to detach only that linked listener. Otherwise it removes all connected listeners.
       * @return {Handler}
       */

    }, {
      key: 'off',
      value: function off(event, listener) {
        var _this3 = this;

        events.parseEvents(event).forEach(function (_ref2) {
          var original = _ref2.original;
          var formatted = _ref2.formatted;

          var listeners = _this3.listeners[original];
          if (!listeners) return;

          if (!listener) {
            while (listener = listeners.shift()) {
              if (!_this3.localOnly) _this3.target.removeEventListener(formatted, listener);
            }
          } else {
            var matchedListeners = _this3.localStorage.get(listener);
            if (matchedListeners) {
              matchedListeners.forEach(function (listener) {
                var index = listeners.indexOf(listener);
                if (!_this3.localOnly) _this3.target.removeEventListener(formatted, listener);
                if (index !== -1) listeners.splice(index, 1);
              });
            }
          }
        });
        return this;
      }

      /**
       * Handler.trigger() - triggers each listener connected with a specified event.
       * It worth to mention that it **does not** fire the DOM event, so any listeners attached not using the built-in Handler's methods will not be executed.
       *
       * @type {String}   event      - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {*} [args] - additional arguments passed to the function
       * @return {Handler}
       */

    }, {
      key: 'trigger',
      value: function trigger(event, args) {
        var _this4 = this;

        events.parseEvents(event).forEach(function (_ref3) {
          var original = _ref3.original;

          (_this4.listeners[original] || []).forEach(function (listener) {
            listener.apply(_this4.target, args);
          });
        });
        return this;
      }

      /**
       * Handler.trigger() - triggers each listener connected with a specified event using dispatchEvent.
       * Unlike Handler.trigger() any listeners attached using addEventListener() will be executed.
       * Currently there is no support for optional arguments.
       *
       * @type {String}   event      - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @return {Handler}
       */

    }, {
      key: 'dispatchEvent',
      value: function dispatchEvent(event) {
        var _this5 = this;

        if (!this.localOnly) events.parseEvents(event).forEach(function (_ref4) {
          var formatted = _ref4.formatted;

          _this5.target.dispatchEvent(new Event(formatted));
        });
        return this;
      }
    }]);

    return Handler;
  }();

  /**
   * This function constructs Handler, so the meaning of arguments its takes is defined in the Handler's constructor.
   * @return {Handler}
   */


  return function (obj, localOnly) {
    // We don't construct Handler each time this metod is executed,
    // instead we get it from a WeakStorage object, when found.
    if (storage.has(obj)) {
      return storage.get(obj);
    } else {
      // We store the Handler in the WeakStorage instance.
      var handler = new Handler(obj, localOnly);
      storage.set(obj, handler);
      return handler;
    }
  };
}

/**
 * events.parseEvents() - parses given string and returns an array with formatted events' names.
 *
 * @param  {String} events - string containing an event (or events) name. May inlcude a bunch of events seperated by any whitespace character, as well as namespaces preceded by a dot.
 * @return {Array}  formattedEvents - array containing object literal with parsed event.
 */
events.parseEvents = function (events) {
  if (typeof events !== 'string') return [];

  var splitted = events.split(whitespaces).filter(function (event) {
    return event.length;
  }); // filter out empty strings
  var formatted = new Array(splitted.length);
  splitted.forEach(function (event, i) {
    formatted[i] = {
      original: event,
      formatted: event.split('.')[0] };
  });
  return formatted;
};