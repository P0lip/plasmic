"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var isHTMLElement = exports.isHTMLElement = function isHTMLElement(element) {
  return element instanceof Element;
};

var isNode = exports.isNode = function isNode(node) {
  return node instanceof Node;
};