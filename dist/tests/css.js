"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isCSSFeatureSupported = isCSSFeatureSupported;
var dimensions = exports.dimensions = {
  height: function height(elem, includingMargin) {
    if (!elem || !elem.nodeType) return;
    var height = elem.offsetHeight;

    if (includingMargin) {
      var _window$getComputedSt = window.getComputedStyle(elem);

      var marginTop = _window$getComputedSt.marginTop;
      var marginBottom = _window$getComputedSt.marginBottom;

      height += parseInt(marginTop) + parseInt(marginBottom);
    }
    return height;
  },
  width: function width(elem, withoutPadding) {
    if (!elem || !elem.nodeType) return;
    var width = elem.offsetWidth;

    if (withoutPadding) {
      var _window$getComputedSt2 = window.getComputedStyle(elem);

      var paddingLeft = _window$getComputedSt2.paddingLeft;
      var paddingRight = _window$getComputedSt2.paddingRight;

      width -= parseInt(paddingLeft) + parseInt(paddingRight);
    }
    return width;
  }
};

function isCSSFeatureSupported(rule, value) {
  if (window.CSS && window.CSS.supports) {
    return window.CSS.supports(rule, value);
  } else {
    var elem = document.body.appendChild(document.createElement("div"));
    elem.style[rule] = value;
    var supported = elem.style[rule] && elem.style[rule] !== "" && elem.style[rule] !== null;
    document.body.removeChild(elem);
    return supported;
  }
}

var cssProps = exports.cssProps = {
  transform: isCSSFeatureSupported("transform", "translate3d(0,0,0)") ? "transform" : "-webkit-transform",
  transition: "transition"
};