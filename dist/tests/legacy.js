'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.supportSlick = supportSlick;
/**
 * Module contains helpers to provide some backwards compatibility with Slick.
 * @module ./legacy.es6
 */
var newPrefix = /plasmic/g;
var legacyPrefix = 'slick';
var whitespaces = /\s+/;

/**
 * supportSlick() - transform a given string (a CSS class), by adding 'slick' prefixes in case there are 'plasmic' prefixes found.
 *
 * @param  {String}   str    - string to be transformed
 * @param  {Boolean}  legacy - indicated whether the transformation should be performed
 * @return {String}   transformed string
 */
function supportSlick(str, legacy) {
  if (!legacy || typeof str !== 'string') return str;

  return str.split(whitespaces).map(function (str) {
    return str + ' ' + (newPrefix.test(str) ? str.replace(newPrefix, legacyPrefix) : '');
  }).join(' ');
}

/**
 * hasjQuery() - detects whether jQuery is available
 *
 * @return {Boolean}
 */
var hasjQuery = exports.hasjQuery = function hasjQuery() {
  return typeof jQuery === 'function' && jQuery().jquery && _typeof(jQuery.fn) === 'object';
};