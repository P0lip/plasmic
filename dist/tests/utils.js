'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.assign = assign;
exports.tryCatch = tryCatch;
exports.extendPrototype = extendPrototype;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Utils module containing methods used across the entire project.
 * @module ./utils.es6
 * @description
 * Functions defined in this module are written at my own, thus there is a test suite, where you can test whether they cause troubles in a given browser.
 */

/**
 * assign() - for docs, refer to https://developer.mozilla.org/pl/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 * This function uses a built-in method Object.assign, but when there is is lacking, it falls back to a similar method.
 * The mentioned method works quite similarly, yet it doesn't follow the specification, so there might be some unexpected behaviors.
 */
function assign(target) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) {
    if (_typeof(arguments[i]) === 'object' && arguments[i] !== null) args.push(arguments[i]);
  }if (Object.assign) {
    Object.assign.apply(Object, [target].concat(args));
  } else {
    args.forEach(function (arg) {
      Object.keys(arg).forEach(function (key) {
        target[key] = arg[key];
      });
    });
  }

  return target;
}

/**
 * tryCatch() - a small wrapper for try catch used by other functions.
 * The reason to use this method is that V8 (as of now) doesn't support try catch pattern, therefore causing each function containg try catch to be deopt (not able to be optimized).
 *
 * @param  {Function} func   -  function used in a try statement
 * @param  {Function} [_catch]  - optional function called when an exception is thrown
 */
function tryCatch(func, _catch) {
  try {
    func();
  } catch (ex) {
    if (typeof _catch === 'function') _catch(ex);
  }
}

/**
 * extendPrototype() - copies properties from one prototype to the target prototype.
 * It does copy all enumerable properties, as it uses Object.getOwnPropertyNames to list props.
 * The function omits copying properties that already exist in the target prototype.
 * Each getter and setter, when found, is preserved.
 *
 * @param  {Object|Function} target  - target function (or object)
 * @param  {Object} target.prototype - a prototype of given target
 * @param  {Object|Function} source  - source function whose prototype will be copied to the target function
 * @param  {Object} source.prototype - a prototype of given source
 * @param  {Object} traps
 *
 */
function extendPrototype(_ref, _ref2, _ref3) {
  var targetProto = _ref.prototype;
  var sourceProto = _ref2.prototype;
  var creation = _ref3.creation;
  var finish = _ref3.finish;
  var getTrap = _ref3.get;
  var setTrap = _ref3.set;
  var valueTrap = _ref3.value;
  var type = _ref3.type;


  var isPropAvailable = function isPropAvailable(prop, obj) {
    return prop in obj;
  };

  function handleTrap(func, prop, trap) {
    for (var _len = arguments.length, args = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
      args[_key - 3] = arguments[_key];
    }

    var results = [];
    var _func = function _func() {
      if (isPropAvailable(prop, this)) {
        var result = func.call.apply(func, [this].concat(args));
        if (typeof finish === 'function') results.push(result);
        return typeof trap === 'function' ? trap(result) : result;
      }
    };

    if (type === 'array') {
      this.forEach(function (elem) {
        _func.call(elem);
      });
      if (typeof finish === 'function') return finish.call(this, results);
    } else {
      return _func.call(this);
    }
  }

  Object.getOwnPropertyNames(sourceProto).forEach(function (prop) {
    if (typeof creation === 'function' && !creation(prop)) return;

    var _Object$getOwnPropert = Object.getOwnPropertyDescriptor(sourceProto, prop);

    var _get = _Object$getOwnPropert.get;
    var _set = _Object$getOwnPropert.set;
    var _value = _Object$getOwnPropert.value;

    if (prop in targetProto) return;
    if (_get) Object.defineProperty(targetProto, prop, {
      enumerable: true,
      get: function get() {
        if (_get) {
          return handleTrap.call(this, _get, prop, getTrap);
        }
      },
      set: function set(value) {
        if (_set) {
          return handleTrap.call(this, _set, prop, setTrap, value);
        }
      }
    });else if (_value) Object.defineProperty(targetProto, prop, {
      writable: true,
      value: function value() {
        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return handleTrap.call.apply(handleTrap, [this, _value, prop, valueTrap].concat(args));
      }
    });
  });
}

/**
 * A shim for WeakMap. It doesn't support any iterator methods, as well as it isn't spec-compliant.
 * It was primarily designed to use for events handler and connecting Plasmic instances with corresponding elements existing in DOM.
 */
var WeakStorage = exports.WeakStorage = WeakMap || function () {
  function _class() {
    _classCallCheck(this, _class);

    this.knownIds = [];
    this.currentId = '';
    this.ensureUniq();
    this.data = function (data) {
      this.data = data;
    };
  }

  /**
   * WeakStorage.ensureUniq() - ensures the uniqnuess of given key.
   *
   * @param {Object|Function} key
   * @return {String} currentId
   */


  _createClass(_class, [{
    key: 'ensureUniq',
    value: function ensureUniq(key) {
      var id = Math.random().toString(32).slice(2) + Math.random().toString(32).slice(2);
      if (!key || this.currentId in key && !(key[this.currentId] instanceof this.data.constructor)) {
        this.currentId = id;
        this.knownIds.push(id);
      }

      return this.currentId;
    }

    /**
     * WealStorage.ensureKey - checkes whether given key is valid.
     * A valid key is either an object or a function.
     *
     * @param {*} key
     */

  }, {
    key: 'ensureKey',
    value: function ensureKey(key) {
      if ((typeof key === 'undefined' ? 'undefined' : _typeof(key)) !== 'object' && typeof key !== 'function' || key === null) throw new TypeError('Key is invalid');
    }
  }, {
    key: 'has',
    value: function has(key, returnId) {
      var _this = this;

      this.ensureKey(key);
      var foundId = void 0;
      this.knownIds.every(function (id) {
        return id in key && key[id] instanceof _this.data ? (foundId = id, false) : true;
      });
      return returnId ? foundId : foundId !== undefined;
    }
  }, {
    key: 'set',
    value: function set(key, value) {
      this.ensureKey(key);
      var id = this.ensureUniq(key);
      Object.defineProperty(key, id, {
        value: Object.freeze(new this.data(value))
      });
    }
  }, {
    key: 'get',
    value: function get(key) {
      this.ensureKey(key);
      var foundKey = this.has(key, true);
      if (foundKey) return key[foundKey].data;
    }
  }, {
    key: 'delete',
    value: function _delete(key) {
      this.ensureKey(key);
      var foundKey = this.has(key, true);
      if (foundKey) {
        delete key[foundKey];
        return true;
      }

      return false;
    }
  }]);

  return _class;
}();