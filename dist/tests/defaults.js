'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Object defined here store default setup values.
 * @module ./defaults.es6
 */
exports.default = {
  accessibility: true,
  adaptiveHeight: false,
  arrows: true,
  asNavFor: null,
  prevArrow: '<button type="button" data-role="none" class="plasmic-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
  nextArrow: '<button type="button" data-role="none" class="plasmic-next" aria-label="Next" tabindex="0" role="button">Next</button>',
  autoplay: false,
  autoplaySpeed: 3000,
  centerMode: false,
  centerPadding: '50px',
  cssEase: 'ease',
  customPaging: function customPaging(slider, i) {
    return this.createElement("button", {
      type: 'button',
      'data-role': 'none',
      role: 'button',
      tabindex: '0',
      textContent: i + 1
    });
  },


  dots: false,
  dotsClass: 'plasmic-dots',
  draggable: true,
  domEvents: false,
  easing: 'linear',
  edgeFriction: 0.35,
  exposeEvents: false,
  fill: true,
  focusOnSelect: false,
  infinite: true,
  initialSlide: 0,
  live: false,
  lazyLoad: 'ondemand',
  mobileFirst: false,
  pauseOnHover: true,
  pauseOnFocus: true,
  pauseOnDotsHover: false,
  respondTo: 'window',
  responsive: null,
  rows: 1,
  rtl: false,
  slickCompatible: false,
  slide: '',
  slidesPerRow: 1,
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  swipe: true,
  swipeToSlide: false,
  touchMove: true,
  touchThreshold: 5,
  variableWidth: false,
  vertical: false,
  verticalSwiping: false,
  waitForAnimate: true,
  zIndex: 1000
};

/**
 * Events dispatched on the root of each slider. You can listen to them via the native addEventListener() method.
 */

var exposedEvents = exports.exposedEvents = ['beforeChange', 'afterChange', 'breakpoint'];