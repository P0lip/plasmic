'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; }; /**
                                                                                                                                                                                                                                                   *
                                                                                                                                                                                                                                                   */


exports.DOMWrapper = DOMWrapper;

var _utils = require('./utils.es6');

var _css = require('./css.es6');

var _events5 = require('./events.es6');

var _events6 = _interopRequireDefault(_events5);

var _domhelpers = require('./domhelpers.es6');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var storage = new _utils.WeakStorage();
var whitespace = /\s+/;

var helpers = {
  callHelper: function callHelper(func, str, val) {
    if (typeof func !== 'function') throw new TypeError('Func wasn\'t specified');

    (function () {
      var _this2 = this;

      if (typeof str === 'string' && typeof val === 'string') func.call(this, str, val);else if (typeof str === 'string' && typeof val === 'undefined') str.split(whitespace).forEach(function (val) {
        func.call(_this2, val);
      });else if (Array.isArray(str)) str.forEach(function (val) {
        func.call(_this2, val);
      });else if ((typeof str === 'undefined' ? 'undefined' : _typeof(str)) === 'object') Object.keys(str).forEach(function (key) {
        func.call(_this2, key, str[key]);
      });
    }).call(this === helpers ? null : this);
  }
};

function DOMWrapper(elem) {
  //if (!new.target) return new DOMWrapper(elem); // FIXME: add support for babel
  if (!(this instanceof DOMWrapper)) return new DOMWrapper(elem);
  this._length = 0;
  this.prev = this;
  this.push(elem);
  this.events = (0, _events6.default)();
  this.observed = false;
}

DOMWrapper.toArray = function (elems) {
  var filteredElems = [];
  if (!elems) return filteredElems;

  if (typeof elems === 'string') filteredElems.push.apply(filteredElems, _toConsumableArray(document.querySelectorAll(elems)));else if ((typeof elems === 'undefined' ? 'undefined' : _typeof(elems)) === 'object' && 'length' in elems) [].forEach.call(elems, function (elem) {
    if (elem instanceof DOMWrapper) filteredElems.push.apply(filteredElems, _toConsumableArray(elem));else if (elem) filteredElems.push(elem);
  });else if (elems instanceof DOMWrapper) filteredElems.push.apply(filteredElems, _toConsumableArray(DOMWrapper));else if (elems) filteredElems.push(elems);
  return filteredElems.filter(_domhelpers.isNode);
};

DOMWrapper.createWrapper = function (elems) {
  var newWrapper = new this.constructor(elems);
  newWrapper.prev = this;
  if (storage.has(this)) storage.get(this).forEach(function (listener, i) {
    listener('new', newWrapper, i);
  });

  return newWrapper;
};

DOMWrapper.createElement = function (tag) {
  var _arguments = arguments;

  var _ref = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  var style = _ref.style;
  var parent = _ref.parent;

  var elem = document.createElement(tag);
  Object.keys(arguments[1]).forEach(function (prop) {
    if (prop in elem) elem[prop] = _arguments[1][prop];else (0, _utils.tryCatch)(function () {
      elem.setAttribute(prop, _arguments[1][prop]);
    });
  });
  if (style) helpers.callHelper(elem.style.setProperty.bind(elem.style), style);
  if (parent) parent.appendChild(elem);
  return elem;
};

/* called once wrapper changes */
DOMWrapper.observeWrapper = function (wrapper, listener) {
  (Array.isArray ? wrapper : [wrapper]).forEach(function (wrapper) {
    if (wrapper instanceof DOMWrapper) {
      wrapper.observed = true;
      var listeners = storage.get(wrapper) || (storage.set(wrapper, []), storage.get(wrapper));
      listeners.push(listener);
    }
  });
  return true;
};

DOMWrapper.prototype = Object.create(Array.prototype, {
  classList: {
    get: function () {
      var toArray = function toArray() {
        for (var _len = arguments.length, classes = Array(_len), _key = 0; _key < _len; _key++) {
          classes[_key] = arguments[_key];
        }

        var _classes = [];
        classes.forEach(function (_class) {
          _classes.push.apply(_classes, _toConsumableArray(typeof _class === 'string' ? _class.split(whitespace) : _class));
        });
        return _classes.filter(function (_class) {
          return _class.length;
        });
      };

      return function () {
        var _this3 = this;

        var _this = this;
        return {
          add: function add() {
            var serializedClasses = toArray.apply(null, arguments);
            _this.forEach(function (elem) {
              var _elem$classList;

              if (elem.classList) (_elem$classList = elem.classList).add.apply(_elem$classList, _toConsumableArray(serializedClasses));
            });
            return this;
          },


          contains: function contains() {
            var _$classList;

            return !_this3.isEmpty && _this3[0].classList && (_$classList = _this3[0].classList).contains.apply(_$classList, arguments);
          },

          remove: function remove() {
            var serializedClasses = toArray.apply(null, arguments);
            _this.forEach(function (elem) {
              var _elem$classList2;

              if (elem.classList) (_elem$classList2 = elem.classList).remove.apply(_elem$classList2, _toConsumableArray(serializedClasses));
            });
            return this;
          },


          return: function _return() {
            return _this3;
          },

          toggle: function toggle() {
            var serializedClasses = toArray.apply(null, arguments);
            if (typeof arguments[arguments.length - 1] === 'boolean') var flag = arguments[arguments.length - 1];
            _this.forEach(function (elem) {
              if (elem.classList) serializedClasses.forEach(function (_class) {
                if (typeof flag === 'boolean') elem.classList.toggle(_class, flag);else elem.classList.toggle(_class);
              });
            });
            return this;
          },


          toArray: toArray
        };
      };
    }()
  },

  isEmpty: {
    get: function get() {
      return !this.length;
    }
  },

  length: {
    get: function get() {
      return this._length;
    },
    set: function set(value) {
      var _this4 = this;

      this._length = value;
      if (this.observed && storage.has(this)) storage.get(this).forEach(function (listener, i) {
        listener('length', _this4, i);
      });

      return value;
    }
  },

  off: {
    get: function get() {
      return this.removeEventListener;
    }
  },

  once: {
    get: function get() {
      return this.addEventListenerOnce;
    }
  },

  on: {
    get: function get() {
      return this.addEventListener;
    }
  },

  push: {
    value: function value() {
      var args = [];
      for (var i = 0; i < arguments.length; i++) {
        args.push.apply(args, _toConsumableArray(DOMWrapper.toArray(arguments[i])));
      }[].push.apply(this, args);
      return this.length;
    }
  },

  style: function () {
    // NOTE: it's a kind of odd stuff for backwards compatibility
    var cache = new _utils.WeakStorage();
    return {
      get: function get() {
        var _this5 = this;

        if (this.isEmpty) return {};

        if (cache.has(this[0])) return cache.get(this[0]);

        var _this = this;
        var setProperty = function setProperty(rule, value) {
          _this.forEach(function (elem) {
            if (elem.style) helpers.callHelper.call(elem.style, elem.style.setProperty, rule, value);
          });
          return this;
        };

        var removeProperty = function removeProperty(rule) {
          _this.forEach(function (elem) {
            if (elem.style) helpers.callHelper.call(elem.style, elem.style.removeProperty, rule);
          });
          return this;
        };

        var styleObj = window.Proxy ? new Proxy({}, {
          get: function get(target, prop) {
            switch (prop) {
              case 'setProperty':
                return setProperty.bind(this);
              case 'removeProperty':
                return removeProperty.bind(this);
              default:
                return _this.isEmpty ? null : _this[0].style[prop];
            }
          },
          set: function set(target, prop, value) {
            _this.forEach(function (elem) {
              elem.style[prop] = value;
            });
            return true;
          }
        }) : function () {
          var props = {};
          Object.getOwnPropertyNames(_this5[0].style).forEach(function (prop) {
            if (!(prop in props)) {
              Object.defineProperty(props, prop, {
                enumerable: true,
                get: function get() {
                  return _this5.isEmpty ? null : _this5[0].style[prop];
                },

                set: function set(value) {
                  _this5.forEach(function (elem) {
                    elem.style[prop] = value;
                  });
                  return true;
                }
              });
            }
          });
          props.setProperty = function () {
            setProperty.apply(this, arguments);
            return this;
          };

          props.removeProperty = function () {
            removeProperty.apply(this, arguments);
            return this;
          };

          return props;
        }();

        cache.set(this[0], styleObj);
        return styleObj;
      }
    };
  }()
});

var DOMWrapperPrototype = DOMWrapper.prototype;


DOMWrapperPrototype.includes || (DOMWrapperPrototype.includes = function (searchElement /*, fromIndex*/) {
  // source: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Array/includes
  var O = Object(this);
  var len = parseInt(O.length) || 0;
  if (len === 0) {
    return false;
  }

  var n = parseInt(arguments[1]) || 0;
  var k;
  if (n >= 0) {
    k = n;
  } else {
    k = len + n;
    if (k < 0) {
      k = 0;
    }
  }

  var currentElement;
  while (k < len) {
    currentElement = O[k];
    if (searchElement === currentElement || searchElement !== searchElement && currentElement !== currentElement) {
      // NaN !== NaN
      return true;
    }

    k++;
  }

  return false;
});

function detectReturn(result) {
  var elems = [];
  var ret = void 0;
  var elemsFound = result.every(function (result) {
    if ((typeof result === 'undefined' ? 'undefined' : _typeof(result)) === 'object') {
      elems.push.apply(elems, _toConsumableArray(DOMWrapper.toArray(result)));
      return true;
    } else {
      ret = result;
      return false;
    }
  });

  return elemsFound ? DOMWrapper.createWrapper.call(this, elems) : ret;
}

(0, _utils.extendPrototype)(DOMWrapper, HTMLElement, {
  creation: function creation(prop) {
    return !(prop in DOMWrapperPrototype);
  },

  finish: function finish(result) {
    return detectReturn.call(this, result);
  },


  type: 'array'
});

(0, _utils.extendPrototype)(DOMWrapper, Element, {
  creation: function creation(prop) {
    return !(prop in DOMWrapperPrototype);
  },

  finish: function finish(result) {
    return detectReturn.call(this, result);
  },


  type: 'array'
});

DOMWrapperPrototype.constructor = DOMWrapper;

DOMWrapperPrototype.appendChild = function (elems) {
  var _this6 = this;

  if (!this.isEmpty) {
    if (Array.isArray(elems) || elems instanceof this.constructor) {
      elems.forEach(function (elem) {
        _this6[0].appendChild(elem);
      });
    } else {
      this[0].appendChild(elems);
    }
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.cloneNode = function (deep) {
  var elems = [];
  this.forEach(function (elem) {
    elems.push(elem.cloneNode(!!deep));
  });
  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.empty = function (newElems) {
  while (this.pop()) {}
  if (newElems) this.push.apply(this, DOMWrapper.toArray(newElems));
  return this;
};

DOMWrapperPrototype.first = function () {
  return this.eq(0);
};

DOMWrapperPrototype.last = function () {
  return this.eq(this.length - 1);
};

DOMWrapperPrototype.eq = function (n) {
  return DOMWrapper.createWrapper.call(this, this[n]);
};

DOMWrapperPrototype.insertBefore = function (elems) {
  var _this7 = this;

  var before = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

  var _ref2 = _slicedToArray(this, 1);

  var first = _ref2[0];

  if (first) {
    var insert = function insert(elem) {
      if (Array.isArray(before) || before instanceof _this7.constructor) before.forEach(function (before) {
        first.insertBefore(elem, before);
      });else first.insertBefore(elem, before);
    };

    if (Array.isArray(elems) || elems instanceof this.constructor) elems.forEach(insert);else insert(elems);
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.remove = function () {
  return this.each(function (elem) {
    elem.remove ? elem.remove() : elem.parentElement && elem.parentElement.removeChild(elem);
  });
};

DOMWrapperPrototype.matches || (DOMWrapperPrototype.matches = function (selector) {
  var matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector;

  return !this.isEmpty && matches && matches.call(this[0], selector);
});

DOMWrapperPrototype.closest || (DOMWrapperPrototype.closest = function (selector) {
  var _this8 = this;

  var elems = [];
  this.forEach(function (elem) {
    while (elem && _this8.matches.call({
      0: elem = elem.parentElement,
      isEmpty: false
    }, selector)) {}
    elems.push(elem);
  });
  return DOMWrapper.createWrapper.call(this, elems);
});

DOMWrapperPrototype.width = function (excludePaddings) {
  return this.isEmpty ? null : _css.dimensions.width(this[0], excludePaddings);
};

DOMWrapperPrototype.height = function (includeMargins) {
  return this.isEmpty ? null : _css.dimensions.height(this[0], includeMargins);
};

DOMWrapperPrototype.each = function (callback) {
  this.forEach(callback);
  return this;
};

// FIXME, should expose native dom events
DOMWrapperPrototype.removeEventListener = function () {
  var _this9 = this;

  for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    args[_key2] = arguments[_key2];
  }

  return this.each(function (elem) {
    var _events;

    (_events = _this9.events(elem)).off.apply(_events, args);
  });
};

DOMWrapperPrototype.addEventListener = function () {
  var _this10 = this;

  for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    args[_key3] = arguments[_key3];
  }

  return this.each(function (elem) {
    var _events2;

    (_events2 = _this10.events(elem)).on.apply(_events2, args);
  });
};

DOMWrapperPrototype.addEventListenerOnce = function () {
  var _this11 = this;

  for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
    args[_key4] = arguments[_key4];
  }

  return this.each(function (elem) {
    var _events3;

    (_events3 = _this11.events(elem)).once.apply(_events3, args);
  });
};

DOMWrapperPrototype.setAttribute = function (prop, value) {
  return this.each(function (elem, i) {
    helpers.callHelper(function (rule, value) {
      elem.setAttribute(rule, String(value).replace('%%i%%', i));
    }, prop, value);
  });
};

DOMWrapperPrototype.removeAttribute = function (rules) {
  return this.each(function (elem) {
    helpers.callHelper(elem.removeAttribute.bind(elem), rules);
  });
};

DOMWrapperPrototype.trigger = function () {
  var _this12 = this;

  for (var _len5 = arguments.length, args = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
    args[_key5] = arguments[_key5];
  }

  return this.each(function (elem) {
    var _events4;

    (_events4 = _this12.events(elem)).trigger.apply(_events4, args);
  });
};