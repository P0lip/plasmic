(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.plasmic = global.plasmic || {})));
}(this, (function (exports) { 'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var slicedToArray = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if (Symbol.iterator in Object(arr)) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

/**
 * Utils module containing methods used across the entire project.
 * Functions defined in this module are written at my own, thus there is a test suite, where you can test whether they cause troubles in a given browser.
 * @module ./utils.es6
 */

/**
 * assign() - for docs, refer to https://developer.mozilla.org/pl/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 * This function uses a built-in method Object.assign, but when there is is lacking, it falls back to a similar method.
 * The mentioned method works quite similarly, yet it doesn't follow the specification, so there might be some unexpected behaviors.
 */
function assign(target) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) {
    if (_typeof(arguments[i]) === 'object' && arguments[i] !== null) args.push(arguments[i]);
  }if (Object.assign) {
    Object.assign.apply(Object, [target].concat(args));
  } else {
    args.forEach(function (arg) {
      Object.keys(arg).forEach(function (key) {
        target[key] = arg[key];
      });
    });
  }

  return target;
}

/**
 * tryCatch() - a small wrapper for try catch used by other functions.
 * The reason to use this method is that V8 (as of now) doesn't support try catch pattern, therefore causing each function containg try catch to be deopt (not able to be optimized).
 *
 * @param  {Function} func   -  function used in a try statement
 * @param  {Function} [_catch]  - optional function called when an exception is thrown
 */
function tryCatch(func, _catch) {
  try {
    func();
  } catch (ex) {
    if (typeof _catch === 'function') _catch(ex);
  }
}

/**
 * extendPrototype() - copies properties from one prototype to the target prototype.
 * It does copy all enumerable properties, as it uses Object.getOwnPropertyNames to list props.
 * The function omits copying properties that already exist in the target prototype.
 * Each getter and setter, when found, is preserved.
 *
 * @param  {Object|Function} target  - target function (or object)
 * @param  {Object} target.prototype - a prototype of given target
 * @param  {Object|Function} source  - source function whose prototype will be copied to the target function
 * @param  {Object} source.prototype - a prototype of given source
 * @param  {Object} traps
 *
 */
function extendPrototype(_ref, _ref2, _ref3) {
  var targetProto = _ref.prototype;
  var sourceProto = _ref2.prototype;
  var creation = _ref3.creation;
  var finish = _ref3.finish;
  var getTrap = _ref3.get;
  var setTrap = _ref3.set;
  var valueTrap = _ref3.value;
  var type = _ref3.type;


  var isPropAvailable = function isPropAvailable(prop, obj) {
    return prop in obj;
  };

  function handleTrap(func, prop, trap) {
    for (var _len = arguments.length, args = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
      args[_key - 3] = arguments[_key];
    }

    var results = [];
    var _func = function _func() {
      if (isPropAvailable(prop, this)) {
        var result = func.call.apply(func, [this].concat(args));
        if (typeof finish === 'function') results.push(result);
        return typeof trap === 'function' ? trap(result) : result;
      }
    };

    if (type === 'array') {
      this.forEach(function (elem) {
        _func.call(elem);
      });
      if (typeof finish === 'function') return finish.call(this, results);
    } else {
      return _func.call(this);
    }
  }

  Object.getOwnPropertyNames(sourceProto).forEach(function (prop) {
    if (typeof creation === 'function' && !creation(prop)) return;

    var _Object$getOwnPropert = Object.getOwnPropertyDescriptor(sourceProto, prop);

    var _get = _Object$getOwnPropert.get;
    var _set = _Object$getOwnPropert.set;
    var _value = _Object$getOwnPropert.value;

    if (prop in targetProto) return;
    if (_get) Object.defineProperty(targetProto, prop, {
      enumerable: true,
      get: function get() {
        if (_get) {
          return handleTrap.call(this, _get, prop, getTrap);
        }
      },
      set: function set(value) {
        if (_set) {
          return handleTrap.call(this, _set, prop, setTrap, value);
        }
      }
    });else if (_value) Object.defineProperty(targetProto, prop, {
      writable: true,
      value: function value() {
        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return handleTrap.call.apply(handleTrap, [this, _value, prop, valueTrap].concat(args));
      }
    });
  });
}

/**
 * A shim for WeakMap. It doesn't support any iterator methods, as well as it isn't spec-compliant.
 * It was primarily designed to use for events handler and connecting Plasmic instances with corresponding elements existing in DOM.
 */
var WeakStorage = WeakMap || function () {
  function _class() {
    classCallCheck(this, _class);

    this.knownIds = [];
    this.currentId = '';
    this.ensureUniq();
    this.data = function (data) {
      this.data = data;
    };
  }

  /**
   * WeakStorage.ensureUniq() - ensures the uniqnuess of given key.
   *
   * @param {Object|Function} key
   * @return {String} currentId
   */


  createClass(_class, [{
    key: 'ensureUniq',
    value: function ensureUniq(key) {
      var id = Math.random().toString(32).slice(2) + Math.random().toString(32).slice(2);
      if (!key || this.currentId in key && !(key[this.currentId] instanceof this.data.constructor)) {
        this.currentId = id;
        this.knownIds.push(id);
      }

      return this.currentId;
    }

    /**
     * WealStorage.ensureKey - checkes whether given key is valid.
     * A valid key is either an object or a function.
     *
     * @param {*} key
     */

  }, {
    key: 'ensureKey',
    value: function ensureKey(key) {
      if ((typeof key === 'undefined' ? 'undefined' : _typeof(key)) !== 'object' && typeof key !== 'function' || key === null) throw new TypeError('Key is invalid');
    }
  }, {
    key: 'has',
    value: function has(key, returnId) {
      var _this = this;

      this.ensureKey(key);
      var foundId = void 0;
      this.knownIds.every(function (id) {
        return id in key && key[id] instanceof _this.data ? (foundId = id, false) : true;
      });
      return returnId ? foundId : foundId !== undefined;
    }
  }, {
    key: 'set',
    value: function set(key, value) {
      this.ensureKey(key);
      var id = this.ensureUniq(key);
      Object.defineProperty(key, id, {
        value: Object.freeze(new this.data(value))
      });
    }
  }, {
    key: 'get',
    value: function get(key) {
      this.ensureKey(key);
      var foundKey = this.has(key, true);
      if (foundKey) return key[foundKey].data;
    }
  }, {
    key: 'delete',
    value: function _delete(key) {
      this.ensureKey(key);
      var foundKey = this.has(key, true);
      if (foundKey) {
        delete key[foundKey];
        return true;
      }

      return false;
    }
  }]);
  return _class;
}();

var dimensions = {
  height: function height(elem, includingMargin) {
    if (!elem || !elem.nodeType) return;
    var height = elem.offsetHeight;

    if (includingMargin) {
      var _window$getComputedSt = window.getComputedStyle(elem);

      var marginTop = _window$getComputedSt.marginTop;
      var marginBottom = _window$getComputedSt.marginBottom;

      height += parseInt(marginTop) + parseInt(marginBottom);
    }
    return height;
  },
  width: function width(elem, withoutPadding) {
    if (!elem || !elem.nodeType) return;
    var width = elem.offsetWidth;

    if (withoutPadding) {
      var _window$getComputedSt2 = window.getComputedStyle(elem);

      var paddingLeft = _window$getComputedSt2.paddingLeft;
      var paddingRight = _window$getComputedSt2.paddingRight;

      width -= parseInt(paddingLeft) + parseInt(paddingRight);
    }
    return width;
  }
};

function isCSSFeatureSupported(rule, value) {
  if (window.CSS && window.CSS.supports) {
    return window.CSS.supports(rule, value);
  } else {
    var elem = document.body.appendChild(document.createElement("div"));
    elem.style[rule] = value;
    var supported = elem.style[rule] && elem.style[rule] !== "" && elem.style[rule] !== null;
    document.body.removeChild(elem);
    return supported;
  }
}

var cssProps = {
  transform: isCSSFeatureSupported("transform", "translate3d(0,0,0)") ? "transform" : "-webkit-transform",
  transition: "transition"
};

var isHTMLElement = function isHTMLElement(element) {
  return element instanceof Element;
};

var isNode = function isNode(node) {
  return node instanceof Node;
};

/**
 * Event handling module. It allows any object or function to communicate by sending events.
 * When possible, a native handling will be used (DOM events).
 * @module ./events.es6
 */
var whitespaces = /\s+/;

function events() {
  var storage = new WeakStorage();

  var Handler = function () {
    /**
     * Handler.constructor();
     *
     * @param {Object|Function|HTMLElement} target    - object event handling will be attached to.
     * @param {Boolean}                     localOnly - indicated whether the native events (DOM events) will be dispatched
     */
    function Handler(target, localOnly) {
      classCallCheck(this, Handler);

      if (!target || typeof target !== 'function' && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) !== 'object') throw new TypeError('Invalid elem passed');

      this.target = target;
      this.isElement = isHTMLElement(target);
      this.listeners = {};
      this.localStorage = new WeakStorage();
      this.localOnly = localOnly && this.isElement;
    }

    /**
     * Handler.on() - adds an event listener.
     * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
     * @type {Function} listener - function called once an event was triggered
     * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
     * @return {Handler}
     */


    createClass(Handler, [{
      key: 'on',
      value: function on(event, listener) {
        var _this = this;

        var data = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

        events.parseEvents(event).forEach(function (_ref) {
          var original = _ref.original;
          var formatted = _ref.formatted;

          var matchedListeners = _this.localStorage.get(listener);
          var boundListener = function boundListener() {
            var e = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

            e.data = data;
            listener.call(this, e);
          };

          if (!matchedListeners) _this.localStorage.set(listener, []);
          _this.localStorage.get(listener).push(boundListener);
          (_this.listeners[original] || (_this.listeners[original] = [])).push(boundListener);
          if (_this.isElement && !_this.localOnly) _this.target.addEventListener(formatted, boundListener);
        });

        return this;
      }

      /**
       * Handler.once() - adds an event listener that will be removed once it is called for a first time
       * It takes the same arguments as the Handler.on() method.
       *
       * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {Function} listener - function called once an event was triggered
       * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
       * @return {Handler}
       */

    }, {
      key: 'once',
      value: function once(event, listener, data) {
        var _this2 = this;

        var callback = function callback() {
          listener();
          _this2.off(event, callback);
        };

        return this.on(event, callback, data);
      }

      /**
       * Handler.off() - removes a previously attached listener or listeners
       *
       * @type {String}   event     - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {Function} [listener] - when defined, function will try to detach only that linked listener. Otherwise it removes all connected listeners.
       * @return {Handler}
       */

    }, {
      key: 'off',
      value: function off(event, listener) {
        var _this3 = this;

        events.parseEvents(event).forEach(function (_ref2) {
          var original = _ref2.original;
          var formatted = _ref2.formatted;

          var listeners = _this3.listeners[original];
          if (!listeners) return;

          if (!listener) {
            while (listener = listeners.shift()) {
              if (!_this3.localOnly) _this3.target.removeEventListener(formatted, listener);
            }
          } else {
            var matchedListeners = _this3.localStorage.get(listener);
            if (matchedListeners) {
              matchedListeners.forEach(function (listener) {
                var index = listeners.indexOf(listener);
                if (!_this3.localOnly) _this3.target.removeEventListener(formatted, listener);
                if (index !== -1) listeners.splice(index, 1);
              });
            }
          }
        });
        return this;
      }

      /**
       * Handler.trigger() - triggers each listener connected with a specified event.
       * It worth to mention that it **does not** fire the DOM event, so any listeners attached not using the built-in Handler's methods will not be executed.
       *
       * @type {String}   event      - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @type {*} [args] - additional arguments passed to the function
       * @return {Handler}
       */

    }, {
      key: 'trigger',
      value: function trigger(event, args) {
        var _this4 = this;

        events.parseEvents(event).forEach(function (_ref3) {
          var original = _ref3.original;

          (_this4.listeners[original] || []).forEach(function (listener) {
            listener.apply(_this4.target, args);
          });
        });
        return this;
      }

      /**
       * Handler.trigger() - triggers each listener connected with a specified event using dispatchEvent.
       * Unlike Handler.trigger() any listeners attached using addEventListener() will be executed.
       * Currently there is no support for optional arguments.
       *
       * @type {String}   event      - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
       * @return {Handler}
       */

    }, {
      key: 'dispatchEvent',
      value: function dispatchEvent(event) {
        var _this5 = this;

        if (!this.localOnly) events.parseEvents(event).forEach(function (_ref4) {
          var formatted = _ref4.formatted;

          _this5.target.dispatchEvent(new Event(formatted));
        });
        return this;
      }
    }]);
    return Handler;
  }();

  /**
   * This function constructs Handler, so the meaning of arguments its takes is defined in the Handler's constructor.
   * @return {Handler}
   */


  return function (obj, localOnly) {
    // We don't construct Handler each time this metod is executed,
    // instead we get it from a WeakStorage object, when found.
    if (storage.has(obj)) {
      return storage.get(obj);
    } else {
      // We store the Handler in the WeakStorage instance.
      var handler = new Handler(obj, localOnly);
      storage.set(obj, handler);
      return handler;
    }
  };
}

/**
 * events.parseEvents() - parses given string and returns an array with formatted events' names.
 *
 * @param  {String} events - string containing an event (or events) name. May inlcude a bunch of events seperated by any whitespace character, as well as namespaces preceded by a dot.
 * @return {Array}  formattedEvents - array containing object literal with parsed event.
 */
events.parseEvents = function (events) {
  if (typeof events !== 'string') return [];

  var splitted = events.split(whitespaces).filter(function (event) {
    return event.length;
  }); // filter out empty strings
  var formatted = new Array(splitted.length);
  splitted.forEach(function (event, i) {
    formatted[i] = {
      original: event,
      formatted: event.split('.')[0] };
  });
  return formatted;
};

/**
 *
 */
var storage = new WeakStorage();
var whitespace = /\s+/;

var helpers = {
  callHelper: function callHelper(func, str, val) {
    if (typeof func !== 'function') throw new TypeError('Func wasn\'t specified');

    (function () {
      var _this2 = this;

      if (typeof str === 'string' && typeof val === 'string') func.call(this, str, val);else if (typeof str === 'string' && typeof val === 'undefined') str.split(whitespace).forEach(function (val) {
        func.call(_this2, val);
      });else if (Array.isArray(str)) str.forEach(function (val) {
        func.call(_this2, val);
      });else if ((typeof str === 'undefined' ? 'undefined' : _typeof(str)) === 'object') Object.keys(str).forEach(function (key) {
        func.call(_this2, key, str[key]);
      });
    }).call(this === helpers ? null : this);
  }
};

function DOMWrapper(elem) {
  //if (!new.target) return new DOMWrapper(elem); // FIXME: add support for babel
  if (!(this instanceof DOMWrapper)) return new DOMWrapper(elem);
  this._length = 0;
  this.prev = this;
  this.push(elem);
  this.events = events();
  this.observed = false;
}

DOMWrapper.toArray = function (elems) {
  var filteredElems = [];
  if (!elems) return filteredElems;

  if (typeof elems === 'string') filteredElems.push.apply(filteredElems, toConsumableArray(document.querySelectorAll(elems)));else if ((typeof elems === 'undefined' ? 'undefined' : _typeof(elems)) === 'object' && 'length' in elems) [].forEach.call(elems, function (elem) {
    if (elem instanceof DOMWrapper) filteredElems.push.apply(filteredElems, toConsumableArray(elem));else if (elem) filteredElems.push(elem);
  });else if (elems instanceof DOMWrapper) filteredElems.push.apply(filteredElems, toConsumableArray(DOMWrapper));else if (elems) filteredElems.push(elems);
  return filteredElems.filter(isNode);
};

DOMWrapper.createWrapper = function (elems) {
  var newWrapper = new this.constructor(elems);
  newWrapper.prev = this;
  if (storage.has(this)) storage.get(this).forEach(function (listener, i) {
    listener('new', newWrapper, i);
  });

  return newWrapper;
};

DOMWrapper.createElement = function (tag) {
  var _arguments = arguments;

  var _ref = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  var style = _ref.style;
  var parent = _ref.parent;

  var elem = document.createElement(tag);
  Object.keys(arguments[1]).forEach(function (prop) {
    if (prop in elem) elem[prop] = _arguments[1][prop];else tryCatch(function () {
      elem.setAttribute(prop, _arguments[1][prop]);
    });
  });
  if (style) helpers.callHelper(elem.style.setProperty.bind(elem.style), style);
  if (parent) parent.appendChild(elem);
  return elem;
};

/* called once wrapper changes */
DOMWrapper.observeWrapper = function (wrapper, listener) {
  (Array.isArray ? wrapper : [wrapper]).forEach(function (wrapper) {
    if (wrapper instanceof DOMWrapper) {
      wrapper.observed = true;
      var listeners = storage.get(wrapper) || (storage.set(wrapper, []), storage.get(wrapper));
      listeners.push(listener);
    }
  });
  return true;
};

DOMWrapper.prototype = Object.create(Array.prototype, {
  classList: {
    get: function () {
      var toArray = function toArray() {
        for (var _len = arguments.length, classes = Array(_len), _key = 0; _key < _len; _key++) {
          classes[_key] = arguments[_key];
        }

        var _classes = [];
        classes.forEach(function (_class) {
          _classes.push.apply(_classes, toConsumableArray(typeof _class === 'string' ? _class.split(whitespace) : _class));
        });
        return _classes.filter(function (_class) {
          return _class.length;
        });
      };

      return function () {
        var _this3 = this;

        var _this = this;
        return {
          add: function add() {
            var serializedClasses = toArray.apply(null, arguments);
            _this.forEach(function (elem) {
              var _elem$classList;

              if (elem.classList) (_elem$classList = elem.classList).add.apply(_elem$classList, toConsumableArray(serializedClasses));
            });
            return this;
          },


          contains: function contains() {
            var _$classList;

            return !_this3.isEmpty && _this3[0].classList && (_$classList = _this3[0].classList).contains.apply(_$classList, arguments);
          },

          remove: function remove() {
            var serializedClasses = toArray.apply(null, arguments);
            _this.forEach(function (elem) {
              var _elem$classList2;

              if (elem.classList) (_elem$classList2 = elem.classList).remove.apply(_elem$classList2, toConsumableArray(serializedClasses));
            });
            return this;
          },


          return: function _return() {
            return _this3;
          },

          toggle: function toggle() {
            var serializedClasses = toArray.apply(null, arguments);
            if (typeof arguments[arguments.length - 1] === 'boolean') var flag = arguments[arguments.length - 1];
            _this.forEach(function (elem) {
              if (elem.classList) serializedClasses.forEach(function (_class) {
                if (typeof flag === 'boolean') elem.classList.toggle(_class, flag);else elem.classList.toggle(_class);
              });
            });
            return this;
          },


          toArray: toArray
        };
      };
    }()
  },

  isEmpty: {
    get: function get() {
      return !this.length;
    }
  },

  length: {
    get: function get() {
      return this._length;
    },
    set: function set(value) {
      var _this4 = this;

      this._length = value;
      if (this.observed && storage.has(this)) storage.get(this).forEach(function (listener, i) {
        listener('length', _this4, i);
      });

      return value;
    }
  },

  off: {
    get: function get() {
      return this.removeEventListener;
    }
  },

  once: {
    get: function get() {
      return this.addEventListenerOnce;
    }
  },

  on: {
    get: function get() {
      return this.addEventListener;
    }
  },

  push: {
    value: function value() {
      var args = [];
      for (var i = 0; i < arguments.length; i++) {
        args.push.apply(args, toConsumableArray(DOMWrapper.toArray(arguments[i])));
      }[].push.apply(this, args);
      return this.length;
    }
  },

  style: function () {
    // NOTE: it's a kind of odd stuff for backwards compatibility
    var cache = new WeakStorage();
    return {
      get: function get() {
        var _this5 = this;

        if (this.isEmpty) return {};

        if (cache.has(this[0])) return cache.get(this[0]);

        var _this = this;
        var setProperty = function setProperty(rule, value) {
          _this.forEach(function (elem) {
            if (elem.style) helpers.callHelper.call(elem.style, elem.style.setProperty, rule, value);
          });
          return this;
        };

        var removeProperty = function removeProperty(rule) {
          _this.forEach(function (elem) {
            if (elem.style) helpers.callHelper.call(elem.style, elem.style.removeProperty, rule);
          });
          return this;
        };

        var styleObj = window.Proxy ? new Proxy({}, {
          get: function get(target, prop) {
            switch (prop) {
              case 'setProperty':
                return setProperty.bind(this);
              case 'removeProperty':
                return removeProperty.bind(this);
              default:
                return _this.isEmpty ? null : _this[0].style[prop];
            }
          },
          set: function set(target, prop, value) {
            _this.forEach(function (elem) {
              elem.style[prop] = value;
            });
            return true;
          }
        }) : function () {
          var props = {};
          Object.getOwnPropertyNames(_this5[0].style).forEach(function (prop) {
            if (!(prop in props)) {
              Object.defineProperty(props, prop, {
                enumerable: true,
                get: function get() {
                  return _this5.isEmpty ? null : _this5[0].style[prop];
                },

                set: function set(value) {
                  _this5.forEach(function (elem) {
                    elem.style[prop] = value;
                  });
                  return true;
                }
              });
            }
          });
          props.setProperty = function () {
            setProperty.apply(this, arguments);
            return this;
          };

          props.removeProperty = function () {
            removeProperty.apply(this, arguments);
            return this;
          };

          return props;
        }();

        cache.set(this[0], styleObj);
        return styleObj;
      }
    };
  }()
});

var DOMWrapperPrototype = DOMWrapper.prototype;


DOMWrapperPrototype.includes || (DOMWrapperPrototype.includes = function (searchElement /*, fromIndex*/) {
  // source: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Array/includes
  var O = Object(this);
  var len = parseInt(O.length) || 0;
  if (len === 0) {
    return false;
  }

  var n = parseInt(arguments[1]) || 0;
  var k;
  if (n >= 0) {
    k = n;
  } else {
    k = len + n;
    if (k < 0) {
      k = 0;
    }
  }

  var currentElement;
  while (k < len) {
    currentElement = O[k];
    if (searchElement === currentElement || searchElement !== searchElement && currentElement !== currentElement) {
      // NaN !== NaN
      return true;
    }

    k++;
  }

  return false;
});

function detectReturn(result) {
  var elems = [];
  var ret = void 0;
  var elemsFound = result.every(function (result) {
    if ((typeof result === 'undefined' ? 'undefined' : _typeof(result)) === 'object') {
      elems.push.apply(elems, toConsumableArray(DOMWrapper.toArray(result)));
      return true;
    } else {
      ret = result;
      return false;
    }
  });

  return elemsFound ? DOMWrapper.createWrapper.call(this, elems) : ret;
}

extendPrototype(DOMWrapper, HTMLElement, {
  creation: function creation(prop) {
    return !(prop in DOMWrapperPrototype);
  },

  finish: function finish(result) {
    return detectReturn.call(this, result);
  },


  type: 'array'
});

extendPrototype(DOMWrapper, Element, {
  creation: function creation(prop) {
    return !(prop in DOMWrapperPrototype);
  },

  finish: function finish(result) {
    return detectReturn.call(this, result);
  },


  type: 'array'
});

DOMWrapperPrototype.constructor = DOMWrapper;

DOMWrapperPrototype.appendChild = function (elems) {
  var _this6 = this;

  if (!this.isEmpty) {
    if (Array.isArray(elems) || elems instanceof this.constructor) {
      elems.forEach(function (elem) {
        _this6[0].appendChild(elem);
      });
    } else {
      this[0].appendChild(elems);
    }
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.cloneNode = function (deep) {
  var elems = [];
  this.forEach(function (elem) {
    elems.push(elem.cloneNode(!!deep));
  });
  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.empty = function (newElems) {
  while (this.pop()) {}
  if (newElems) this.push.apply(this, DOMWrapper.toArray(newElems));
  return this;
};

DOMWrapperPrototype.first = function () {
  return this.eq(0);
};

DOMWrapperPrototype.last = function () {
  return this.eq(this.length - 1);
};

DOMWrapperPrototype.eq = function (n) {
  return DOMWrapper.createWrapper.call(this, this[n]);
};

DOMWrapperPrototype.insertBefore = function (elems) {
  var _this7 = this;

  var before = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

  var _ref2 = slicedToArray(this, 1);

  var first = _ref2[0];

  if (first) {
    var insert = function insert(elem) {
      if (Array.isArray(before) || before instanceof _this7.constructor) before.forEach(function (before) {
        first.insertBefore(elem, before);
      });else first.insertBefore(elem, before);
    };

    if (Array.isArray(elems) || elems instanceof this.constructor) elems.forEach(insert);else insert(elems);
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.remove = function () {
  return this.each(function (elem) {
    elem.remove ? elem.remove() : elem.parentElement && elem.parentElement.removeChild(elem);
  });
};

DOMWrapperPrototype.matches || (DOMWrapperPrototype.matches = function (selector) {
  var matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector;

  return !this.isEmpty && matches && matches.call(this[0], selector);
});

DOMWrapperPrototype.closest || (DOMWrapperPrototype.closest = function (selector) {
  var _this8 = this;

  var elems = [];
  this.forEach(function (elem) {
    while (elem && _this8.matches.call({
      0: elem = elem.parentElement,
      isEmpty: false
    }, selector)) {}
    elems.push(elem);
  });
  return DOMWrapper.createWrapper.call(this, elems);
});

DOMWrapperPrototype.width = function (excludePaddings) {
  return this.isEmpty ? null : dimensions.width(this[0], excludePaddings);
};

DOMWrapperPrototype.height = function (includeMargins) {
  return this.isEmpty ? null : dimensions.height(this[0], includeMargins);
};

DOMWrapperPrototype.each = function (callback) {
  this.forEach(callback);
  return this;
};

// FIXME, should expose native dom events
DOMWrapperPrototype.removeEventListener = function () {
  var _this9 = this;

  for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    args[_key2] = arguments[_key2];
  }

  return this.each(function (elem) {
    var _events;

    (_events = _this9.events(elem)).off.apply(_events, args);
  });
};

DOMWrapperPrototype.addEventListener = function () {
  var _this10 = this;

  for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    args[_key3] = arguments[_key3];
  }

  return this.each(function (elem) {
    var _events2;

    (_events2 = _this10.events(elem)).on.apply(_events2, args);
  });
};

DOMWrapperPrototype.addEventListenerOnce = function () {
  var _this11 = this;

  for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
    args[_key4] = arguments[_key4];
  }

  return this.each(function (elem) {
    var _events3;

    (_events3 = _this11.events(elem)).once.apply(_events3, args);
  });
};

DOMWrapperPrototype.setAttribute = function (prop, value) {
  return this.each(function (elem, i) {
    helpers.callHelper(function (rule, value) {
      elem.setAttribute(rule, String(value).replace('%%i%%', i));
    }, prop, value);
  });
};

DOMWrapperPrototype.removeAttribute = function (rules) {
  return this.each(function (elem) {
    helpers.callHelper(elem.removeAttribute.bind(elem), rules);
  });
};

DOMWrapperPrototype.trigger = function () {
  var _this12 = this;

  for (var _len5 = arguments.length, args = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
    args[_key5] = arguments[_key5];
  }

  return this.each(function (elem) {
    var _events4;

    (_events4 = _this12.events(elem)).trigger.apply(_events4, args);
  });
};

/**
 * Module contains helpers to provide some backwards compatibility with Slick.
 * @module ./legacy.es6
 */
var newPrefix = /plasmic/g;
var legacyPrefix = 'slick';
var whitespaces$1 = /\s+/;

/**
 * supportSlick() - transform a given string (a CSS class), by adding 'slick' prefixes in case there are 'plasmic' prefixes found.
 *
 * @param  {String}   str    - string to be transformed
 * @param  {Boolean}  legacy - indicated whether the transformation should be performed
 * @return {String}   transformed string
 */
function supportSlick(str, legacy) {
  if (!legacy || typeof str !== 'string') return str;

  return str.split(whitespaces$1).map(function (str) {
    return str + ' ' + (newPrefix.test(str) ? str.replace(newPrefix, legacyPrefix) : '');
  }).join(' ');
}

/**
 * hasjQuery() - detects whether jQuery is available
 *
 * @return {Boolean}
 */
var hasjQuery = function hasjQuery() {
  return typeof jQuery === 'function' && jQuery().jquery && _typeof(jQuery.fn) === 'object';
};

/**
 * Object defined here store default setup values.
 * @module ./defaults.es6
 */
var defaults$1 = {
  accessibility: true,
  adaptiveHeight: false,
  arrows: true,
  asNavFor: null,
  prevArrow: '<button type="button" data-role="none" class="plasmic-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
  nextArrow: '<button type="button" data-role="none" class="plasmic-next" aria-label="Next" tabindex="0" role="button">Next</button>',
  autoplay: false,
  autoplaySpeed: 3000,
  centerMode: false,
  centerPadding: '50px',
  cssEase: 'ease',
  customPaging: function customPaging(slider, i) {
    return this.createElement("button", {
      type: 'button',
      'data-role': 'none',
      role: 'button',
      tabindex: '0',
      textContent: i + 1
    });
  },


  dots: false,
  dotsClass: 'plasmic-dots',
  draggable: true,
  domEvents: false,
  easing: 'linear',
  edgeFriction: 0.35,
  exposeEvents: false,
  fill: true,
  focusOnSelect: false,
  infinite: true,
  initialSlide: 0,
  live: false,
  lazyLoad: 'ondemand',
  mobileFirst: false,
  pauseOnHover: true,
  pauseOnFocus: true,
  pauseOnDotsHover: false,
  respondTo: 'window',
  responsive: null,
  rows: 1,
  rtl: false,
  slickCompatible: false,
  slide: '',
  slidesPerRow: 1,
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  swipe: true,
  swipeToSlide: false,
  touchMove: true,
  touchThreshold: 5,
  variableWidth: false,
  vertical: false,
  verticalSwiping: false,
  waitForAnimate: true,
  zIndex: 1000
};

/**
 * Events dispatched on the root of each slider. You can listen to them via the native addEventListener() method.
 */
var exposedEvents = ['beforeChange', 'afterChange', 'breakpoint'];

/*
  TODO:
  2) expose event handling (on, off, once) to make afterChange etc work
  3) live change (as you slide) when nav
  4) MutationObserver (adding a new node)
  TABINDEX to allow arrows
 */
var instanceUid = 0;

var globalStorage = new WeakStorage(); // storage linking each element with Plasmic instance

function SliderDOMWrapper() {
  DOMWrapper.apply(this, arguments);
}

SliderDOMWrapper.prototype = Object.create(DOMWrapper.prototype, {
  classList: {
    get: function () {
      var _Object$getOwnPropert = Object.getOwnPropertyDescriptor(DOMWrapper.prototype, 'classList');

      var originalObj = _Object$getOwnPropert.get;

      var propsToModify = ['add', 'contains', 'remove', 'toggle'];
      var insertSlick = function insertSlick(originalObj) {
        for (var _len = arguments.length, classes = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          classes[_key - 1] = arguments[_key];
        }

        return supportSlick(originalObj.toArray.apply(originalObj, classes).join(' '), true).split(' ');
      };
      return function () {
        var classList = originalObj.call(this);
        Object.keys(classList).forEach(function (key) {
          if (propsToModify.indexOf(key) !== -1) {
            (function () {
              var originalFunc = classList[key];
              classList[key] = function () {
                for (var _len2 = arguments.length, classes = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                  classes[_key2] = arguments[_key2];
                }

                return originalFunc.call(classList, insertSlick.apply(undefined, [classList].concat(classes)));
              };
            })();
          }
        });
        return classList;
      };
    }()
  }
});

SliderDOMWrapper.prototype.constructor = SliderDOMWrapper;

var Plasmic = function () {
  /**
   * @param  {HTMLElement}       element  - element where Plasmic will be attached to
   * @param  {Object} [settings] settings - for the list of available options, refer to ./defaults.es6
   */
  function Plasmic(element, settings) {
    var _this = this;

    classCallCheck(this, Plasmic);

    //validate(settings)
    globalStorage.set(element, this);
    this.options = {};
    assign(this.options, defaults$1, settings);
    if (this.options.slickCompatible) element.slick = this;
    this.originalSettings = this.options;

    this.events = this.options.slickCompatible && hasjQuery() ? function () {
      var parseEvents = events.parseEvents;
      var _events = events();
      var trigger = _events({}).trigger;
      return function (elem) {
        var events = _events(elem);
        events.trigger = function (event, args) {
          trigger.call(this, event, args);
          parseEvents(event).forEach(function (_ref) {
            var original = _ref.original;

            window.jQuery(elem).trigger(original, args);
          });
        };
        return events;
      };
    }() : events();
    this.stylings = new WeakStorage();

    /* elems */
    this.DOMWrapper = this.options.slickCompatible ? SliderDOMWrapper : DOMWrapper;
    this.currentSlideElem = new this.DOMWrapper();
    this.document = this.events(document);
    this.window = this.events(window);
    this.slider = new this.DOMWrapper(element);
    this.list = new this.DOMWrapper();
    this.slideTrack = new this.DOMWrapper();
    this.slides = new this.DOMWrapper();
    this.clonedSlides = new this.DOMWrapper();
    this.emptySlides = new this.DOMWrapper(); // use with conjuncation of fill option
    this.prevArrow = new this.DOMWrapper();
    this.nextArrow = new this.DOMWrapper();
    this.dotsContainer = new this.DOMWrapper();
    this.dots = new this.DOMWrapper();
    this.lazyImages = this.slider.querySelectorAll("img[data-lazy]:not([src])");
    this.allSlides = new this.DOMWrapper();
    DOMWrapper.observeWrapper([this.clonedSlides, this.slides], function () {
      _this.allSlides.empty([].concat(toConsumableArray(_this.clonedSlides), toConsumableArray(_this.slides)));
    });

    if (this.options.domEvents) exposedEvents.forEach(function (event) {
      _this.slider.on(event, function () {
        _this.slider.dispatchEvent(event);
      });
    });

    this.animating = false;
    this.dragging = false;
    this.autoPlayTimer = null;
    this.currentSlide = this.options.initialSlide;
    this.currentDirection = 0;
    this.currentLeft = null;
    this._currentSlide = 0;
    this.direction = 1;
    this.listWidth = -1;
    this.listHeight = -1;
    this.loadIndex = 0;
    this.slideWidth = -1;

    this.sliding = false;
    this.slideOffset = 0;
    this.swipeLeft = null;
    this.touchObject = {};
    this.unplasmiced = false;

    this.activeBreakpoint = null;
    this.breakpoints = [];
    this.breakpointSettings = [];
    this.focussed = false;
    this.interrupted = false;
    this.hidden = this.supportSlick(("hidden" in document ? "hidden" : "webkitHidden") + ".plasmic.plasmic-" + instanceUid);
    this.paused = true;
    this.positionProp = null;
    this.respondTo = null;
    this.rowCount = 1;
    this.shouldClick = true;
    this.slidesCache = null;
    this.visibilityChange = this.supportSlick(("hidden" in document ? "visibilitychange" : "webkitvisibilitychange") + ".plasmic.plasmic-" + instanceUid);
    this.windowWidth = 0;
    this.windowTimer = null;

    //this.navTarget

    this.instanceUid = instanceUid++;

    this.registerBreakpoints();
    this.init(true);
  }

  createClass(Plasmic, [{
    key: "activateADA",
    value: function activateADA() {
      this.currentSlideElem.setAttribute("aria-hidden", "false").querySelectorAll("a, input, button, select").setAttribute("tabindex", "0");
    }
  }, {
    key: "animateHeight",
    value: function animateHeight() {
      var _this2 = this;

      if (this.options.slidesToShow === 1 && this.options.adaptiveHeight && !this.options.vertical) {
        var _list$once$style$setP;

        var height = this.currentSlideElem.height(true);
        this.list.once("transitionend webkitTransitionEnd", function () {
          _this2.list.style.removeProperty(cssProps.transition);
        }).style.setProperty((_list$once$style$setP = {}, defineProperty(_list$once$style$setP, cssProps.transition, "height " + this.options.cssEase + " " + this.options.speed / 1000 + "s"), defineProperty(_list$once$style$setP, "height", height + "px"), _list$once$style$setP));
      }
    }
  }, {
    key: "animateSlide",
    value: function animateSlide(targetLeft, callback) {
      var _this3 = this;

      this.animateHeight();

      if (this.options.rtl && !this.options.vertical) targetLeft = -targetLeft;
      this.applyTransition();
      targetLeft = Math.ceil(targetLeft);

      this.slideTrack.style.setProperty(cssProps.transform, this.options.vertical ? "translate3d(0," + targetLeft + "px,0)" : "translate3d(" + targetLeft + "px,0,0)");

      if (typeof callback === "function") setTimeout(function () {
        _this3.disableTransition();
        callback();
      }, this.options.speed);
    }
  }, {
    key: "applyTransition",
    value: function applyTransition() {
      this.slideTrack.style.setProperty(cssProps.transition, cssProps.transform + " " + this.options.speed + "ms " + this.options.cssEase);
    }
  }, {
    key: "asNavFor",
    value: function asNavFor(index) {
      var asNavFor = this.getNavTarget();

      asNavFor.forEach(function (plasmic) {
        var target = globalStorage.get(plasmic);
        if (!target.unplasmiced) target.slideHandler(index, true);
      });
    }
  }, {
    key: "autoPlayIterator",
    value: function autoPlayIterator() {
      if (!this.paused && !this.interrupted && !this.focussed) {
        var slideTo = this.currentSlide + this.options.slidesToScroll;
        if (!this.options.infinite) {
          if (this.direction === 1 && this.currentSlide + 1 === this.slideCount - 1) {
            this.direction = 0;
          } else if (this.direction === 0) {
            slideTo = this.currentSlide - this.options.slidesToScroll;
            if (this.currentSlide - 1 === 0) {
              this.direction = 1;
            }
          }
        }

        this.slideHandler(slideTo);
      }
    }
  }, {
    key: "build",
    value: function build() {
      this.list.empty(document.createElement("div"));
      this.slideTrack.empty(this.list.appendChild(document.createElement("div")));
    }
  }, {
    key: "buildArrows",
    value: function buildArrows() {
      var _this4 = this;

      if (this.options.arrows) {
        (function () {
          var regex = /^(?:\s*(<[\w\W]+>)[^>]*)$/; // taken from jQuery source code.
          var createArrow = function createArrow(type) {
            var button = document.createElement("button");
            var option = _this4.options[type];
            if (!option) return button;

            if (typeof option === "string" && regex.test(option)) {
              var root = document.createElement("div");
              root.insertAdjacentHTML("afterbegin", _this4.options[type]);
              var _button = root.firstElementChild;

              if (_this4.options.slickCompatible) _button.className = _button.className.replace(_button.className, _this4.supportSlick(_button.className));
              return _button;
            }

            if (option instanceof Element) return _this4.options[type];

            return button;
          };
          _this4.prevArrow.empty(createArrow("prevArrow"));
          _this4.nextArrow.empty(createArrow("nextArrow"));
          var arrows = new _this4.DOMWrapper([_this4.prevArrow, _this4.nextArrow]);

          arrows.classList.add("plasmic-arrow");

          if (_this4.slideCount > _this4.options.slidesToShow) {
            arrows.removeAttribute("aria-hidden tabindex").classList.remove("plasmic-hidden");

            _this4.slider.insertBefore(_this4.prevArrow, _this4.slider.firstElementChild).prev.appendChild(_this4.nextArrow);
          }
        })();
      }
    }
  }, {
    key: "buildDots",
    value: function buildDots() {
      if (this.options.dots && this.slideCount > this.options.slidesToShow) {
        this.slider.classList.add("plasmic-dotted");

        this.dotsContainer.push(DOMWrapper.createElement("ul", {
          className: this.options.dotsClass
        }));

        var container = this.dotsContainer;

        for (var i = 0, dotsCount = this.getDotCount(); i <= dotsCount; i++) {
          var dot = container.appendChild(document.createElement("li"));
          this.dots.push(dot);
          dot.appendChild(this.options.customPaging.call(DOMWrapper, i));
        }

        this.dots.first().setAttribute("aria-hidden", "false").classList.add("plasmic-active");

        this.slider.appendChild(container);
      }
    }
  }, {
    key: "buildOut",
    value: function buildOut() {
      var _this5 = this;

      this.slider.children.filter(function (child) {
        return !child.classList.contains("plasmic-cloned");
      }).forEach(function (slide) {
        if (!_this5.slides.includes(slide)) _this5.slides.push(slide);
      });

      this.slider.classList.add("plasmic-slider").return().appendChild(this.list);
      this.list.setAttribute("aria-live", "polite").classList.add("plasmic-list");

      this.slides.classList.add("plasmic-slide").return().setAttribute("data-plasmic-index", "%%i%%").each(function (slide) {
        _this5.stylings.set(slide, slide.getAttribute("style") || "");
      });

      this.slideTrack.appendChild(this.slides).prev.classList.add("plasmic-track");

      if (this.options.centerMode || this.options.swipeToSlide) this.options.slidesToScroll = 1;

      this.lazyImages.classList.add("plasmic-loading");

      this.setupInfinite();
      this.buildArrows();
      this.buildDots();
      this.updateDots();

      this.setSlideClasses(typeof this.currentSlide === "number" ? this.currentSlide : 0);

      if (this.options.draggable) this.list.classList.add("draggable");
    }
  }, {
    key: "buildRows",
    value: function buildRows() {
      var newSlides = document.createDocumentFragment();

      if (this.options.rows > 1) {
        var originalSlides = this.slides,
            slidesPerSection = this.options.slidesPerRow * this.options.rows,
            numOfSlides = Math.ceil(this.slideCount / slidesPerSection);

        for (var a = 0; a < numOfSlides; a++) {
          var slide = document.createElement("div");
          for (var b = 0; b < this.options.rows; b++) {
            var row = document.createElement("div");
            for (var c = 0; c < this.options.slidesPerRow; c++) {
              var target = a * slidesPerSection + (b * this.options.slidesPerRow + c);
              if (originalSlides[target]) var originalSlide = row.appendChild(originalSlides[target]);
              if (typeof originalSlide !== "undefined") {
                originalSlide.style.width = 100 / this.options.slidesPerRow + "%";
                originalSlide.style.display = "inline-block";
              }
              slide.appendChild(row);
            }
            newSlides.appendChild(slide);
          }
        }

        this.slider.children.remove();

        this.slider.appendChild(newSlides);
      }
    }
  }, {
    key: "changeSlide",
    value: function changeSlide(event, dontAnimate) {
      var _this6 = this;

      var target = event.currentTarget;
      var _event$data = event.data;
      var message = _event$data.message;
      var index = _event$data.index;


      var wrappedTarget = new this.DOMWrapper(target);

      // If target is a link, prevent default action.
      if (wrappedTarget.tagName === "A" && event.preventDefault) event.preventDefault();

      // If target is not the <li> element (ie: a child), find the <li>.
      if (wrappedTarget.tagName !== "LI") wrappedTarget.closest("li");

      var unevenOffset = this.slideCount % this.options.slidesToScroll !== 0,
          indexOffset = unevenOffset ? 0 : (this.slideCount - this.currentSlide) % this.options.slidesToScroll;

      switch (message) {
        case "previous":
          {
            var slideOffset = indexOffset === 0 ? this.options.slidesToScroll : this.options.slidesToShow - indexOffset;
            if (this.slideCount > this.options.slidesToShow) this.slideHandler(this.currentSlide - slideOffset, false, dontAnimate);
          }
          break;

        case "next":
          {
            var _slideOffset = indexOffset === 0 ? this.options.slidesToScroll : indexOffset;
            if (this.slideCount > this.options.slidesToShow) this.slideHandler(this.currentSlide + _slideOffset, false, dontAnimate);
          }
          break;

        case "index":
          var _index = index === 0 ? 0 : index || [].indexOf.call(target.parentElement.children, target) * this.options.slidesToScroll;

          this.slideHandler(this.checkNavigable(_index), false, dontAnimate);
          wrappedTarget.children.forEach(function (child) {
            _this6.events(child).trigger("focus");
          });
          break;
      }
    }
  }, {
    key: "checkNavigable",
    value: function checkNavigable(index) {
      var navigables = this.getNavigableIndexes();
      var prevNavigable = 0;

      if (index > navigables[navigables.length - 1]) {
        index = navigables[navigables.length - 1];
      } else {
        for (var n in navigables) {
          if (index < navigables[n]) {
            index = prevNavigable;
            break;
          }
          prevNavigable = navigables[n];
        }
      }

      return index;
    }
  }, {
    key: "checkResponsive",
    value: function checkResponsive(initial, forceUpdate) {
      var _this7 = this;

      var _window = window;
      var windowWidth = _window.innerWidth;
      var sliderWidth = this.slider.width();

      var respondToWidth = void 0;
      if (this.respondTo === "window") respondToWidth = windowWidth;else if (this.respondTo === "slider") respondToWidth = sliderWidth;else if (this.respondTo === "min") respondToWidth = Math.min(windowWidth, sliderWidth);

      if (this.options.responsive && this.options.responsive.length && this.options.responsive !== null) {
        var targetBreakpoint = null;
        Object.keys(this.breakpoints).forEach(function (breakpoint) {
          if (!_this7.originalSettings.mobileFirst) {
            if (respondToWidth < _this7.breakpoints[breakpoint]) targetBreakpoint = _this7.breakpoints[breakpoint];
          } else if (respondToWidth > _this7.breakpoints[breakpoint]) {
            targetBreakpoint = _this7.breakpoints[breakpoint];
          }
        });

        /* jshint -W004 */
        if (targetBreakpoint !== null) {
          if (this.activeBreakpoint !== null) {
            if (targetBreakpoint !== this.activeBreakpoint || forceUpdate) {
              this.activeBreakpoint = targetBreakpoint;
              if (this.breakpointSettings[targetBreakpoint] === "unplasmic") {
                this.unplasmic(targetBreakpoint);
              } else {
                assign(this.options, this.originalSettings, this.breakpointSettings[targetBreakpoint]);
                if (initial) this.currentSlide = this.options.initialSlide;
                this.refresh(initial);
              }
              var triggerBreakpoint = targetBreakpoint;
            }
          } else {
            this.activeBreakpoint = targetBreakpoint;
            if (this.breakpointSettings[targetBreakpoint] === "unplasmic") {
              this.unplasmic(targetBreakpoint);
            } else {
              assign(this.options, this.originalSettings, this.breakpointSettings[targetBreakpoint]);
              if (initial) this.currentSlide = this.options.initialSlide;
              this.refresh(initial);
              var triggerBreakpoint = targetBreakpoint;
            }
          }
        } else {
          if (this.activeBreakpoint !== null) {
            this.activeBreakpoint = null;
            this.options = this.originalSettings;
            if (initial) this.currentSlide = this.options.initialSlide;
            this.refresh(initial);
            var triggerBreakpoint = targetBreakpoint;
          }
        }
        /* jshint +W004 */

        // only trigger breakpoints during an actual break. not on initialize.
        if (!initial && triggerBreakpoint) this.slider.trigger("breakpoint", [this, triggerBreakpoint]);
      }
    }
  }, {
    key: "cleanUpEvents",
    value: function cleanUpEvents() {
      var _this8 = this;

      if (this.options.dots) this.dots.off("click.plasmic", this.changeSlide).off("mouseenter.plasmic.bound mouseleave.plasmic.bound");

      this.slider.off("focus.plasmic").off("blur.plasmic");

      this.prevArrow.off("click.plasmic");
      this.nextArrow.off("click.plasmic");

      this.list.off("touchstart.plasmic touchmove.plasmic touchend.plasmic touchcancel.plasmic").off("mousedown.plasmic mousemove.plasmic moseup.plasmic mouseleave.plasmic");

      this.document.off(this.visibilityChange, this.visibility);

      this.cleanUpSlideEvents();

      if (this.options.accessibility) this.list.off("keydown.plasmic.bound");

      if (this.options.focusOnSelect) this.allSlides.off("click.plasmic.bound");

      this.window.off("orientationchange.plasmic.bound").off("resize.plasmic.bound").off("load.plasmic.bound");

      this.slideTrack.querySelectorAll(":not([draggable=true])").forEach(function (node) {
        _this8.events(node).off("dragstart", _this8.preventDefault);
      });

      this.document.off("DOMContentLoaded.plasmic");
    }
  }, {
    key: "cleanUpSlideEvents",
    value: function cleanUpSlideEvents() {
      this.list.off("mouseenter.plasmic mouseleave.plasmic");
    }
  }, {
    key: "cleanUpRows",
    value: function cleanUpRows() {
      if (this.options.rows > 1) {
        var originalSlides = this.slides.children.children;
        originalSlides.removeAttribute("style");
        this.slider.children.remove();
        this.slider.appendChild(originalSlides);
      }
    }
  }, {
    key: "clickHandler",
    value: function clickHandler(event) {
      if (!this.shouldClick) {
        event.stopImmediatePropagation();
        event.stopPropagation();
        event.preventDefault();
      }
    }
  }, {
    key: "destroy",
    value: function destroy(refresh) {
      var _this9 = this;

      this.autoPlay = false;
      this.touchObject = {};
      this.cleanUpEvents();

      this.clonedSlides.remove().empty();

      this.dotsContainer.remove().empty();

      this.prevArrow.remove();
      this.nextArrow.remove();

      this.slider.appendChild(this.slides.classList.remove("plasmic-slide plasmic-active plasmic-center plasmic-visible plasmic-current").return().removeAttribute("aria-hidden data-plasmic-index").each(function (slide) {
        slide.setAttribute("style", _this9.stylings.get(slide));
      }));

      this.list.remove();
      this.slider.classList.remove("plasmic-slider plasmic-initialized plasmic-dotted");
      this.cleanUpRows();

      this.unplasmiced = true;

      if (!refresh) {
        this.slider.trigger("destroy", [this]);
        globalStorage.delete(this.slider);
      }
    }
  }, {
    key: "disableTransition",
    value: function disableTransition() {
      this.slideTrack.style.removeProperty(cssProps.transition);
    }
  }, {
    key: "focusHandler",
    value: function focusHandler() {
      this.slider.off("focus.focus-handler blur.focus-handler").on("focus.focus-handler blur.focus-handler", function (e) {
        e.stopImmediatePropagation();
        setTimeout(function () {
          if (this.options.pauseOnFocus) {
            this.focussed = document.activeElement === e.target;
            this.autoPlay = true;
          }
        }, 0);
      });
    }
  }, {
    key: "getDotCount",
    value: function getDotCount() {
      var pagerQty = 0,
          breakPoint = 0,
          counter = 0;

      if (this.options.infinite) {
        while (breakPoint < this.slideCount) {
          ++pagerQty;
          breakPoint = counter + this.options.slidesToScroll;
          counter += this.options.slidesToScroll <= this.options.slidesToShow ? this.options.slidesToScroll : this.options.slidesToShow;
        }
      } else if (this.options.centerMode) {
        pagerQty = this.slideCount;
      } else if (!this.options.asNavFor) {
        pagerQty = 1 + Math.ceil((this.slideCount - this.options.slidesToShow) / this.options.slidesToScroll);
      } else {
        while (breakPoint < this.slideCount) {
          ++pagerQty;
          breakPoint = counter + this.options.slidesToScroll;
          counter += this.options.slidesToScroll <= this.options.slidesToShow ? this.options.slidesToScroll : this.options.slidesToShow;
        }
      }

      return pagerQty - 1;
    }
  }, {
    key: "getLeft",
    value: function getLeft(slideIndex) {
      var targetLeft = void 0;
      if (!this.slides.length) return targetLeft;
      this.slideOffset = 0;
      var verticalHeight = this.slides.first().height(true);
      var verticalOffset = 0;

      if (this.options.infinite) {
        if (this.slideCount > this.options.slidesToShow) {
          this.slideOffset = this.slideWidth * this.options.slidesToShow * -1;
          verticalOffset = verticalHeight * this.options.slidesToShow * -1;
        }

        if (this.slideCount % this.options.slidesToScroll !== 0) {
          if (slideIndex + this.options.slidesToScroll > this.slideCount && this.slideCount > this.options.slidesToShow) {
            if (slideIndex > this.slideCount) {
              if (!this.options.fill) {
                var diff = this.slideCount / this.options.slidesToShow,
                    idealSize = (Math.floor(diff) + (diff > this.options.slidesToShow)) * this.options.slidesToShow;
                verticalOffset = this.slideOffset = (this.options.slidesToShow - (slideIndex - idealSize)) * this.slideWidth * -1;
              } else {
                verticalOffset = this.slideOffset = (this.options.slidesToShow - (slideIndex - this.slideCount)) * this.slideWidth * -1;
              }
            } else {
              verticalOffset = this.slideOffset = (this.slideCount - slideIndex + (this.options.slidesToShow - this.slideCount + slideIndex)) * this.slideWidth * -1;
            }
          }
        }
      } else {
        if (slideIndex + this.options.slidesToShow > this.slideCount) {
          this.slideOffset = (slideIndex + this.options.slidesToShow - this.slideCount) * this.slideWidth;
          verticalOffset = (slideIndex + this.options.slidesToShow - this.slideCount) * verticalHeight;
        }
      }

      if (this.slideCount <= this.options.slidesToShow) {
        this.slideOffset = 0;
        verticalOffset = 0;
      }

      if (this.options.centerMode && this.options.infinite) this.slideOffset += this.slideWidth * Math.floor(this.options.slidesToShow / 2) - this.slideWidth;else if (this.options.centerMode) this.slideOffset = this.slideWidth * Math.floor(this.options.slidesToShow / 2);

      if (!this.options.vertical) targetLeft = slideIndex * this.slideWidth * -1 + this.slideOffset;else targetLeft = slideIndex * verticalHeight * -1 + verticalOffset;

      if (this.options.variableWidth) {
        /* jshint -W004 */
        if (this.slideCount <= this.options.slidesToShow || !this.options.infinite) var targetSlide = this.allSlides[slideIndex];else var targetSlide = this.allSlides[slideIndex + this.options.slidesToShow];
        /* jshint +W004 */
        if (this.options.rtl) {
          if (targetSlide) targetLeft = (this.slideTrack.offsetWidth - targetSlide.offsetLeft - targetSlide.offsetWidth) * -1;else targetLeft = 0;
        } else {
          targetLeft = targetSlide ? targetSlide.offsetLeft * -1 : 0;
        }

        if (this.options.centerMode) {
          if (this.slideCount <= this.options.slidesToShow || !this.options.infinite) targetSlide = this.allSlides[slideIndex];else targetSlide = this.allSlides[slideIndex + this.options.slidesToShow + 1];

          if (this.options.rtl) {
            if (targetSlide) targetLeft = (this.slideTrack.offsetWidth - targetSlide.offsetLeft - targetSlide.offsetWidth) * -1;else targetLeft = 0;
          } else {
            targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
          }

          targetLeft += (this.list.offsetWidth - targetSlide.offsetWidth) / 2;
        }
      }

      return targetLeft;
    }
  }, {
    key: "getNavigableIndexes",
    value: function getNavigableIndexes() {
      var indexes = [];
      var counter = 0,
          breakpoint = 0;

      if (!this.options.infinite) {
        var max = this.slideCount;
      } else {
        breakpoint = this.options.slidesToScroll * -1;
        counter = this.options.slidesToScroll * -1;
        var max = this.slideCount * 2;
      }

      while (breakpoint < max) {
        indexes.push(breakpoint);
        breakpoint = counter + this.options.slidesToScroll;
        counter += this.options.slidesToScroll <= this.options.slidesToShow ? this.options.slidesToScroll : this.options.slidesToShow;
      }

      return indexes;
    }
  }, {
    key: "getNavTarget",
    value: function getNavTarget() {
      var _this10 = this;

      if (this.navTarget) return this.navTarget;
      var asNavFor = this.options.asNavFor;

      var elems = [];

      if (asNavFor) if (isHTMLElement(asNavFor) && asNavFor !== this.slider[0]) elems.push(asNavFor);else if (typeof asNavFor === "string") elems.push.apply(elems, toConsumableArray([].filter.call(document.querySelectorAll(asNavFor), function (elem) {
        return elem !== _this10.slider;
      })));

      return elems;
    }
  }, {
    key: "getSlideCount",
    value: function getSlideCount() {
      var _this11 = this;

      if (this.options.swipeToSlide) {
        var _ret3 = function () {
          var centerOffset = _this11.options.centerMode ? _this11.slideWidth * Math.floor(_this11.options.slidesToShow / 2) : 0;
          var swipedSlide = void 0;
          _this11.slides.every(function (slide) {
            if (slide.offsetLeft - centerOffset + slide.offsetWidth / 2 > _this11.swipeLeft * -1) {
              swipedSlide = slide;
              return false;
            }
            return true;
          });

          return {
            v: Math.abs(swipedSlide.getAttribute("data-plasmic-index") - _this11.currentSlide) || 1
          };
        }();

        if ((typeof _ret3 === "undefined" ? "undefined" : _typeof(_ret3)) === "object") return _ret3.v;
      } else {
        return this.options.slidesToScroll;
      }
    }
  }, {
    key: "goTo",
    value: function goTo(slide, dontAnimate) {
      this.changeSlide({
        data: {
          message: "index",
          index: parseInt(slide)
        }
      }, dontAnimate);
    }
  }, {
    key: "init",
    value: function init(creation) {
      if (!this.slider.classList.contains("plasmic-initialized")) {
        this.slider.classList.add("plasmic-initialized");

        this.build();
        this.buildRows();
        this.buildOut();
        this.setProps();
        this.startLoad();
        this.loadSlider();
        this.initializeEvents();
        this.updateArrows();
        this.updateDots();
        this.checkResponsive(true);
        this.focusHandler();
      }

      this.unplasmiced = false;

      if (creation) this.slider.trigger("init");

      if (this.options.accessibility) this.initADA();

      if (this.options.autoplay) {
        this.paused = false;
        this.autoPlay = true;
      }
    }
  }, {
    key: "initADA",
    value: function initADA() {
      var _this12 = this;

      this.clonedSlides.setAttribute({
        "aria-hidden": "true",
        "tabindex": "-1"
      }).querySelectorAll("a, input, button, select").setAttribute("tabindex", "-1");

      this.slideTrack.setAttribute("role", "listbox");

      this.slides.forEach(function (slide, i) {
        slide.setAttribute("role", "option");
        if (_this12.options.dots) {
          var describedBySlideId = _this12.options.centerMode ? i : Math.floor(i / _this12.options.slidesToShow);
          //Evenly distribute aria-describedby tags through available dots.
          slide.setAttribute("aria-describedby", "plasmic-slide" + _this12.instanceUid + describedBySlideId);
        }
      });

      if (this.dotsContainer) {
        this.dotsContainer.setAttribute("role", "tablist");
        if (!this.dots.isEmpty) {
          this.dots.setAttribute({
            role: "presentation",
            "aria-selected": "false",
            "aria-controls": "navigation" + this.instanceUid + "%%i%%",
            id: "plasmic-slide" + this.instanceUid + "%%i%%"
          }).first().setAttribute("aria-selected", "true");

          this.dotsContainer.querySelectorAll("button").setAttribute("role", "button");

          this.slider.setAttribute("role", "toolbar");
        }
      }

      this.activateADA();
    }
  }, {
    key: "initArrowEvents",
    value: function initArrowEvents() {
      if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
        this.prevArrow.off("click.plasmic").on("click.plasmic", this.changeSlide.bind(this), {
          message: "previous"
        });
        this.nextArrow.off("click.plasmic").on("click.plasmic", this.changeSlide.bind(this), {
          message: "next"
        });
      }
    }
  }, {
    key: "initDotEvents",
    value: function initDotEvents() {
      if (this.options.dots && this.slideCount > this.options.slidesToShow) this.dots.on("click.plasmic", this.changeSlide.bind(this), {
        message: "index"
      });

      if (this.options.dots && this.options.pauseOnDotsHover) this.dots.on("mouseenter.plasmic mouseleave.plasmic", this.changeSlide.bind(this));
    }
  }, {
    key: "initializeEvents",
    value: function initializeEvents() {
      var _this13 = this;

      this.initArrowEvents();
      this.initDotEvents();
      this.initSlideEvents();

      this.list.on("touchstart.plasmic mousedown.plasmic", this.swipeHandler.bind(this), {
        action: "start"
      }).on("touchmove.plasmic mousemove.plasmic", this.swipeHandler.bind(this), {
        action: "move"
      }).on("touchend.plasmic mouseup.plasmic touchcancel.plasmic mouseleave.plasmic", this.swipeHandler.bind(this), {
        action: "end"
      }).on("click.plasmic", this.clickHandler.bind(this));

      if (this.options.accessibility) this.list.on("keydown.plasmic", this.keyHandler.bind(this));

      if (this.options.focusOnSelect) this.allSlides.forEach(function (slide) {
        _this13.events(slide).on("click.plasmic.bound", _this13.selectHandler.bind(_this13));
      });

      this.window.on("orientationchange.plasmic.bound.plasmic-" + this.instanceUid, this.orientationChange.bind(this)).on("resize.plasmic.bound.plasmic-" + this.instanceUid, this.resize.bind(this)).on("load.plasmic.bound.plasmic-" + this.instanceUid, this.setPosition.bind(this));

      [].forEach.call(this.slideTrack.querySelectorAll(":not([draggable=true])"), function (node) {
        _this13.events(node).on("dragstart", function (e) {
          e.preventDefault();
        });
      });

      this.document.on(this.visibilityChange, this.visibility.bind(this)).on("DOMContentLoaded.plasmic", this.setPosition.bind(this));
    }
  }, {
    key: "initUI",
    value: function initUI() {
      if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
        this.prevArrow.style.display = "";
        this.nextArrow.style.display = "";
      }

      if (this.options.dots && this.slideCount > this.options.slidesToShow) this.dotsContainer.style.display = "";
    }
  }, {
    key: "initSlideEvents",
    value: function initSlideEvents() {
      if (this.options.pauseOnHover) this.list.on("mouseenter.plasmic", this.interrupt.bind(this, true)).on("mouseleave.plasmic", this.interrupt.bind(this, false));
    }
  }, {
    key: "interrupt",
    value: function interrupt(toggle) {
      if (!toggle) this.autoPlay = true;

      this.interrupted = toggle;
      return toggle;
    }
  }, {
    key: "keyHandler",
    value: function keyHandler(_ref2) {
      var target = _ref2.target;
      var keyCode = _ref2.keyCode;

      //Dont slide if the cursor is inside the form fields and arrow keys are pressed
      if (!target.tagName.match("TEXTAREA|INPUT|SELECT")) {
        if (keyCode === 37 && this.options.accessibility) {
          this.changeSlide({
            data: {
              message: this.options.rtl ? "next" : "previous"
            }
          });
        } else if (event.keyCode === 39 && this.options.accessibility) {
          this.changeSlide({
            data: {
              message: this.options.rtl ? "previous" : "next"
            }
          });
        }
      }
    }
  }, {
    key: "lazyLoad",
    value: function lazyLoad() {
      var _this14 = this;

      var loadImages = function loadImages() {
        _this14.lazyImages.forEach(function (img) {
          var source = img.dataset.lazy;
          var tempImg = DOMWrapper.createElement("img", {
            onerror: function onerror() {
              new _this14.DOMWrapper(img).removeAttribute("data-lazy").classList.remove("plasmic-loading").add("plasmic-lazyload-error");

              _this14.slider.trigger("lazyLoadError", [_this14, img, source]);
            },

            onload: function onload() {
              new _this14.DOMWrapper(img).setAttribute("src", source).removeAttribute("data-lazy").classList.remove("plasmic-loading");
              if (_this14.options.adaptiveHeight) _this14.setPosition();
              _this14.slider.trigger("lazyLoaded", [_this14, img, source]);
            }
          });
          tempImg.src = source;
        });
      };

      var rangeStart = void 0,
          rangeEnd = void 0;
      if (this.options.centerMode) {
        if (this.options.infinite) {
          rangeStart = this.currentSlide + (this.options.slidesToShow / 2 + 1);
          rangeEnd = rangeStart + this.options.slidesToShow + 2;
        } else {
          rangeStart = Math.max(0, this.currentSlide - (this.options.slidesToShow / 2 + 1));
          rangeEnd = 2 + (this.options.slidesToShow / 2 + 1) + this.currentSlide;
        }
      } else {
        rangeStart = this.options.infinite ? this.options.slidesToShow + this.currentSlide : this.currentSlide;
        rangeEnd = Math.ceil(rangeStart + this.options.slidesToShow);
      }

      loadImages(this.allSlides.slice(rangeStart, rangeEnd));

      if (this.slideCount <= this.options.slidesToShow) loadImages(this.allSlides);else if (this.currentSlide >= this.slideCount - this.options.slidesToShow) loadImages(this.clonedSlides.slice(0, this.options.slidesToShow));else if (this.currentSlide === 0) loadImages(this.clonedSlides.slice(this.options.slidesToShow * -1));
    }
  }, {
    key: "loadSlider",
    value: function loadSlider() {
      this.setPosition();
      this.slideTrack.style.opacity = 1;
      this.slider.classList.remove("plasmic-loading");
      this.initUI();

      if (this.options.lazyLoad === "progressive") this.progressiveLazyLoad();
    }
  }, {
    key: "orientationChange",
    value: function orientationChange() {
      this.checkResponsive();
      this.setPosition();
    }
  }, {
    key: "pause",
    value: function pause() {
      this.autoPlay = false;
      this.paused = true;
    }
  }, {
    key: "play",
    value: function play() {
      this.autoPlay = true;
      this.options.autoPlay = true;
      this.paused = false;
      this.focussed = false;
      this.interrupted = false;
    }
  }, {
    key: "postSlide",
    value: function postSlide(index) {
      if (!this.unplasmiced) {
        this.slider.trigger("afterChange", [this, index]);

        this.animating = false;
        this.setPosition();
        this.swipeLeft = null;

        if (this.options.autoplay) this.autoPlay = true;

        if (this.options.accessibility) this.initADA();
      }
    }
  }, {
    key: "refresh",
    value: function refresh(initializing) {
      var lastVisibleIndex = this.slideCount - this.options.slidesToShow;

      // in non-infinite sliders, we don't want to go past the
      // last visible index.
      if (!this.options.infinite && this.currentSlide > lastVisibleIndex) this.currentSlide = lastVisibleIndex;

      // if less slides than to show, go to start.
      if (this.slideCount <= this.options.slidesToShow) this.currentSlide = 0;

      var currentSlide = this.currentSlide;


      this.destroy(true);
      this.animating = false, this.dragging = false;
      this.sliding = false;
      this.slideOffset = 0;
      this.swipeLeft = null;
      this.currentLeft = 0;
      this.currentSlide = currentSlide;
      this.currentDirection = 0;
      this.direction = 1;
      this.listWidth = -1;
      this.listHeight = -1;
      this.slideWidth = -1;
      this.loadIndex = 0;
      this.unplasmiced = false;
      this.touchObject = {};

      this.dots.empty();
      this.nextArrow.empty();
      this.prevArrow.empty();
      this.slideTrack.empty();
      this.list.empty();

      this.init();

      if (!initializing) this.changeSlide({
        data: {
          message: "index",
          index: currentSlide
        }
      }, false);
    }
  }, {
    key: "reinit",
    value: function reinit() {
      var _this15 = this;

      this.slides.classList.add("plasmic-slide");

      if (this.currentSlide >= this.slideCount && this.currentSlide !== 0) this.currentSlide = this.currentSlide - this.options.slidesToScroll;

      if (this.slideCount <= this.options.slidesToShow) this.currentSlide = 0;

      this.registerBreakpoints();

      this.setProps();
      this.setupInfinite();
      this.buildArrows();
      this.updateArrows();
      this.initArrowEvents();
      this.buildDots();
      this.updateDots();
      this.initDotEvents();
      this.cleanUpSlideEvents();
      this.initSlideEvents();

      this.checkResponsive(false, true);

      if (this.options.focusOnSelect) this.allSlides.forEach(function (slide) {
        _this15.events(slide).on("click.plasmic", _this15.selectHandle.bind(_this15));
      });

      this.setSlideClasses(typeof this.currentSlide === "number" ? this.currentSlide : 0);

      this.setPosition();
      this.focusHandler();

      this.paused = !this.options.autoplay;
      this.autoPlay = this.options.autoplay;

      this.events(this.slide).trigger("reInit", [this]);
    }
  }, {
    key: "registerBreakpoints",
    value: function registerBreakpoints() {
      var _this16 = this;

      var responsiveSettings = this.options.responsive || [];

      var dedupe = function dedupe(arr, prop) {
        arr.slice().forEach(function (elem) {
          if (elem.breakpoint === prop) arr.splice(arr.indexOf(elem), 1);
        });
      };

      if (Array.isArray(responsiveSettings) && responsiveSettings.length) {
        this.respondTo = this.options.respondTo || "window";
        responsiveSettings.forEach(function (breakpoint) {
          var currentBreakpoint = breakpoint.breakpoint;
          var settings = breakpoint.settings;

          dedupe(_this16.breakpoints, currentBreakpoint);
          _this16.breakpoints.push(typeof currentBreakpoint === "string" && _this16.options.slickCompatible ? "unslick" : currentBreakpoint); // TODO: test
          _this16.breakpointSettings[currentBreakpoint] = settings;
        });
        this.breakpoints.sort(function (a, b) {
          return _this16.options.mobileFirst ? a - b : b - a;
        });
      }
    }
  }, {
    key: "resize",
    value: function resize() {
      var _this17 = this;

      if (window.innerWidth !== this.windowWidth) {
        // TODO: add prefixes for webkit/blink browsers
        window.cancelAnimationFrame(this.windowDelay);
        this.windowDelay = window.requestAnimationFrame(function () {
          _this17.windowWidth = window.innerWidth;
          _this17.checkResponsive();
          if (!_this17.unplasmiced) _this17.setPosition();
        });
      }
    }
  }, {
    key: "selectHandler",
    value: function selectHandler(_ref3) {
      var target = _ref3.target;

      var targetElement = target.classList.contains("plasmic-slide") ? target : function () {
        var parent = target;
        while (parent && !(parent = parent.parentElement).classList.contains("plasmic-slide")) {}
        return parent;
      }();

      var index = 0;
      if (targetElement) index = parseInt(targetElement.dataset.plasmicIndex);

      if (this.slideCount <= this.options.slidesToShow) {
        this.setSlideClasses(index);
        this.asNavFor(index);
        return;
      }

      this.slideHandler(index);
    }
  }, {
    key: "plasmicPrev",
    value: function plasmicPrev() {
      this.changeSlide({
        data: {
          message: "previous"
        }
      });
    }
  }, {
    key: "plasmicSetOption",
    value: function plasmicSetOption() {
      var _this18 = this;

      /**
       * accepts arguments in format of:
       *
       *  - for changing a single option's value:
       *     .plasmic("setOption", option, value, refresh )
       *
       *  - for changing a set of responsive options:
       *     .plasmic("setOption", 'responsive', [{}, ...], refresh )
       *
       *  - for updating multiple values at once (not responsive)
       *     .plasmic("setOption", { 'option': value, ... }, refresh )
       */

      /* jshint -W004 */
      if (_typeof(arguments[0]) === "object" && arguments[0] !== null) var option = arguments[0],
          refresh = arguments[1],
          type = "multiple";else if (typeof arguments[0] === "string") var option = arguments[0],
          value = arguments[1],
          refresh = arguments[2],
          type = arguments[0] === "responsive" && Array.isArray(arguments[1]) ? "responsive" : _typeof(arguments[1]) ? "single" : "";
      /* jshint +W004 */
      switch (type) {
        case "single":
          this.options[option] = value;
          break;
        case "multiple":
          Object.keys(option).forEach(function (key) {
            _this18.options[key] = option[key];
          });
          break;
        case "responsive":
          arguments[1].forEach(function (item) {
            if (!Array.isArray(_this18.options.responsive)) _this18.options.responsive = [];
            _this18.options.responsive.push(item);
            _this18.registerBreakpoints(); // dedupe
          });
      }

      if (refresh) {
        this.unload();
        this.reinit();
      }
    }
  }, {
    key: "setCSS",
    value: function setCSS(position) {
      if (this.options.rtl) position = -position;

      var x = this.positionProp === "left" ? Math.ceil(position) + "px" : "0",
          y = this.positionProp === "top" ? Math.ceil(position) + "px" : "0";

      this.slideTrack.style.setProperty(cssProps.transform, "translate3d(" + x + "," + y + ",0)");
    }
  }, {
    key: "setDimensions",
    value: function setDimensions() {
      if (!this.options.vertical) {
        if (this.options.centerMode) this.list.style.padding = "0 " + this.options.centerPadding;
      } else {
        this.list.style.height = this.slides.height(true) * this.options.slidesToShow + "px";
        if (this.options.centerMode) this.list.style.padding = this.options.centerPadding + " 0";
      }

      this.listWidth = this.list.width(true);
      this.listHeight = this.list.height();
      if (!this.options.vertical && !this.options.variableWidth) {
        this.slideWidth = Math.ceil(this.listWidth / this.options.slidesToShow);
        this.slideTrack.style.width = Math.ceil(this.slideWidth * this.allSlides.length) + "px";
      } else if (this.options.variableWidth) {
        this.slideTrack.style.width = 5000 * this.slideCount + "px";
      } else {
        this.slideWidth = Math.ceil(this.listWidth);
        this.slideTrack.style.height = Math.ceil(this.slides.height(true) * this.allSlides.length) + "px";
      }

      if (!this.options.variableWidth && this.slides.length) {
        var _window$getComputedSt = window.getComputedStyle(this.slides[0]);

        var marginLeft = _window$getComputedSt.marginLeft;
        var marginRight = _window$getComputedSt.marginRight;

        var offset = parseInt(marginLeft) + parseInt(marginRight);
        this.allSlides.style.width = this.slideWidth - offset + "px";
      }
    }
  }, {
    key: "setHeight",
    value: function setHeight() {
      if (this.options.slidesToShow === 1 && this.options.adaptiveHeight && !this.options.vertical) this.list.style.height = dimensions.height(this.slides[this.currentSlide]) + "px";
    }
  }, {
    key: "setPosition",
    value: function setPosition() {
      this.setDimensions();
      this.setHeight();
      this.setCSS(this.getLeft(this.currentSlide));
      this.slider.trigger("setPosition", [this]);
    }
  }, {
    key: "setProps",
    value: function setProps() {
      this.positionProp = this.options.vertical ? "top" : "left";

      if (this.positionProp === "top") this.slider.classList.add("plasmic-vertical");else this.slider.classList.remove("plasmic-vertical");
    }
  }, {
    key: "setSlideClasses",
    value: function setSlideClasses(index) {
      var _this19 = this;

      this.slides.setAttribute("aria-hidden", "true").classList.remove("plasmic-active plasmic-center plasmic-current");

      var each = function each() {
        var start = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
        var length = arguments.length <= 1 || arguments[1] === undefined ? _this19.slides.length : arguments[1];

        _this19.slides.slice(start, length).forEach(function (slide) {
          slide.classList.add("plasmic-active");
          slide.setAttribute("aria-hidden", "false");
        });
      };

      if (this.slides[index]) this.slides[index].classList.add("plasmic-current");
      if (this.options.centerMode) {
        var centerOffset = Math.floor(this.options.slidesToShow / 2);
        if (this.options.infinite) {
          if (index >= centerOffset && index <= this.slideCount - 1 - centerOffset) {
            each(index - centerOffset, index + centerOffset + 1);
          } else {
            var indexOffset = this.options.slidesToShow + index;
            each(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2);
          }

          if (index === 0) {
            var slide = this.slides[this.slides.length - 1 - this.options.slidesToShow];
            if (slide) slide.classList.add("plasmic-center");
          } else if (index === this.slideCount - 1) {
            var _slide = this.slides[this.options.slidesToShow];
            if (_slide) _slide.classList.add("plasmic-center");
          }

          if (this.slides[index]) this.slides[index].classList.add("plasmic-center");
        }
      } else {
        if (index >= 0 && index <= this.slideCount - this.options.slidesToShow) {
          each(index, index + this.options.slidesToShow);
        } else if (this.slides.length <= this.options.slidesToShow) {
          each();
        } else if (!this.options.fill) {
          each(index);
        } else {
          var remainder = this.slideCount % this.options.slidesToShow,
              _indexOffset = this.options.infinite === true ? this.options.slidesToShow + index : index;
          if (this.options.slidesToShow == this.options.slidesToScroll && this.slideCount - index < this.options.slidesToShow) each(_indexOffset - (this.options.slidesToShow - remainder), _indexOffset + remainder);else each(_indexOffset, _indexOffset + this.options.slidesToShow);
        }
      }

      if (this.options.lazyLoad === "ondemand") this.lazyLoad();
    }
  }, {
    key: "setupInfinite",
    value: function setupInfinite() {
      var _this20 = this;

      if (this.options.infinite && this.slideCount > this.options.slidesToShow) {
        var infiniteCount = this.options.slidesToShow + !!this.options.centerMode;
        var uniqueClone = function uniqueClone(elem, index) {
          var empty = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];
          return new _this20.DOMWrapper(elem).cloneNode(true).removeAttribute("id").classList.add("plasmic-cloned plasmic-slide").return().setAttribute({
            "data-plasmic-index": index,
            "data-plasmic-empty": empty
          })[0];
        };

        if (this.options.fill) {
          for (var i = this.slideCount; i > this.slideCount - infiniteCount; i -= 1) {
            var slideIndex = i - 1;
            this.clonedSlides.push(this.slideTrack.insertBefore(uniqueClone(this.slides[slideIndex], slideIndex - this.slideCount), this.slideTrack.firstElementChild));
          }

          for (var _i = 0; _i < infiniteCount; _i += 1) {
            var _slideIndex = _i;
            this.clonedSlides.push(this.slideTrack.appendChild(uniqueClone(this.slides[_slideIndex], _slideIndex + this.slideCount)));
          }
        } else {
          for (var _i2 = 0; _i2 < this.options.slidesToShow; _i2++) {
            var _slideIndex2 = this.slideCount - this.options.slidesToShow + _i2,
                shouldBeEmpty = this.slides[_slideIndex2] && _i2 >= this.options.slidesToShow - this.slideCount % this.options.slidesToShow;

            var slide = this.slideTrack.insertBefore(uniqueClone(!shouldBeEmpty ? document.createElement("div") : this.slides[_slideIndex2], _slideIndex2, !shouldBeEmpty), this.slideTrack.firstElementChild);

            this.clonedSlides.push(slide);
            if (!shouldBeEmpty) this.emptySlides.push(slide);
          }

          this.clonedSlides.filter(function (elem) {
            return !JSON.parse(elem.dataset.plasmicEmpty);
          }).sort(function (elemA, elemB) {
            return elemA.dataset.plasmicIndex - elemB.dataset.plasmicIndex;
          }).forEach(function (slide, index) {
            _this20.slideTrack.insertBefore(slide, _this20.slideTrack.children[index]);
          });

          for (var _slideIndex3 = 0; _slideIndex3 < this.options.slidesToShow - this.slideCount % infiniteCount; _slideIndex3++) {
            var _slide2 = this.slideTrack.appendChild(uniqueClone(document.createElement("div"), _slideIndex3 - this.slideCount - 1, true));
            this.clonedSlides.push(_slide2);
            this.emptySlides.push(_slide2);
          }

          for (var _slideIndex4 = 0; _slideIndex4 < infiniteCount; _slideIndex4++) {
            this.clonedSlides.push(this.slideTrack.appendChild(uniqueClone(this.slides[_slideIndex4], _slideIndex4 + this.slideCount)));
          }
        }

        this.clonedSlides.removeAttribute("id");
      }
    }
  }, {
    key: "slideHandler",
    value: function slideHandler(index, sync, dontAnimate) {
      var _this21 = this;

      sync = sync || false;

      if (this.animating && this.options.waitForAnimate || this.options.fade && this.currentSlide === index || this.slideCount <= this.options.slidesToShow) return;

      if (!sync) this.asNavFor(index);

      var targetSlide = index;
      var targetLeft = this.getLeft(targetSlide);
      var slideLeft = this.getLeft(this.currentSlide);
      this.currentLeft = this.swipeLeft === null ? slideLeft : this.swipeLeft;

      if (!this.options.infinite && !this.options.centerMode && (index < 0 || index > this.getDotCount() * this.options.slidesToScroll)) {
        targetSlide = this.currentSlide;
        if (!dontAnimate) this.animateSlide(slideLeft, function () {
          _this21.postSlide(targetSlide);
        });
        return;
      } else if (!this.options.infinite && this.options.centerMode && (index < 0 || index > this.slideCount - this.options.slidesToScroll)) {
        targetSlide = this.currentSlide;
        if (!dontAnimate) this.animateSlide(slideLeft, function () {
          _this21.postSlide(targetSlide);
        });
        return;
      }

      if (this.options.autoplay) this.autoPlay = false;

      /* jshint -W004 */
      if (targetSlide < 0) {
        if (this.slideCount % this.options.slidesToScroll !== 0) var animSlide = this.slideCount - this.slideCount % this.options.slidesToScroll;else var animSlide = this.slideCount + targetSlide;
      } else if (targetSlide >= this.slideCount) {
        if (this.slideCount % this.options.slidesToScroll !== 0) var animSlide = 0;else var animSlide = targetSlide - this.slideCount;
      } else {
        var animSlide = targetSlide;
      }
      /* jshint +W004 */

      this.animating = true;

      this.events(this.slider).trigger("beforeChange", [this, this.currentSlide, animSlide]);

      this.currentSlide = animSlide;

      this.setSlideClasses(this.currentSlide);

      if (this.options.asNavFor) {
        var navTarget = this.getNavTarget();
        navTarget.forEach(function (nav) {
          var plasmic = globalStorage.get(nav);
          if (plasmic.slideCount <= plasmic.options.slidesToShow) plasmic.setSlideClasses(_this21.currentSlide);
        });
      }

      this.updateDots();
      this.updateArrows();

      if (!dontAnimate) this.animateSlide(targetLeft, function () {
        _this21.postSlide(animSlide);
      });else this.postSlide(animSlide);
    }
  }, {
    key: "supportSlick",
    value: function supportSlick$$(src) {
      return supportSlick(src, this.options.slickCompatible);
    }
  }, {
    key: "plasmicNext",
    value: function plasmicNext() {
      this.changeSlide({
        data: {
          message: "next"
        }
      });
    }
  }, {
    key: "progressiveLazyLoad",
    value: function progressiveLazyLoad() {
      var _this22 = this;

      var tryCount = arguments.length <= 0 || arguments[0] === undefined ? 1 : arguments[0];

      if (this.lazyImages.length) this.lazyImages.forEach(function (img) {
        var source = img.dataset.lazy;
        var tempImg = DOMWrapper.createElement("img", {
          onerror: function onerror() {
            if (tryCount < 3) {
              /**
                * try to load the image 3 times,
                * leave a slight delay so we don't get
                * servers blocking the request.
                */
              setTimeout(function () {
                this.progressiveLazyLoad(++tryCount);
              }, 500);
            } else {
              new _this22.DOMWrapper(img).removeAttribute("data-lazy").classList.remove("plasmic-loading").add("plasmic-lazyload-error");

              _this22.slider.trigger("lazyLoadError", [_this22, img, source]);
              _this22.progressiveLazyLoad();
            }
          },

          onload: function onload() {
            new _this22.DOMWrapper(img).setAttribute("src", source).removeAttribute("data-lazy").classList.remove("plasmic-loading");
            if (_this22.options.adaptiveHeight) _this22.setPosition();
            _this22.slider.trigger("lazyLoaded", [_this22, img, source]);
            _this22.progressiveLazyLoad();
          }
        });
        tempImg.src = source;
      });else this.slider.trigger("allImagesLoaded", [this]);
    }
  }, {
    key: "swipeDirection",
    value: function swipeDirection() {
      var rtl = !this.options.rtl,
          r = Math.atan2(this.touchObject.startY - this.touchObject.curY, this.touchObject.startX - this.touchObject.curX);
      var swipeAngle = Math.round(r * 180 / Math.PI);
      if (swipeAngle < 0) swipeAngle = 360 - Math.abs(swipeAngle);

      if (swipeAngle <= 45 && swipeAngle >= 0 || swipeAngle <= 360 && swipeAngle >= 315) return rtl ? "left" : "right";

      if (swipeAngle >= 135 && swipeAngle <= 225) return rtl ? "right" : "left";

      if (this.options.verticalSwiping) return swipeAngle >= 35 && swipeAngle <= 135 ? "down" : "up";

      return "vertical";
    }
  }, {
    key: "swipeEnd",
    value: function swipeEnd() {
      this.dragging = false;
      this.interrupted = false;
      this.shouldClick = this.touchObject.swipeLength < 10;

      if (this.touchObject.curX === undefined) return false;

      if (this.touchObject.edgeHit) this.slider.trigger("edge", [this, this.swipeDirection()]);

      if (this.touchObject.swipeLength >= this.touchObject.minSwipe) {
        var direction = this.swipeDirection();
        var slideCount = void 0;
        switch (direction) {
          case "left":
          case "down":
            slideCount = this.options.swipeToSlide ? this.checkNavigable(this.currentSlide + this.getSlideCount()) : this.currentSlide + this.getSlideCount();
            this.currentDirection = 0;
            break;

          case "right":
          case "up":
            slideCount = this.options.swipeToSlide ? this.checkNavigable(this.currentSlide - this.getSlideCount()) : this.currentSlide - this.getSlideCount();
            this.currentDirection = 1;
            break;
        }

        if (direction !== "vertical") {
          this.slideHandler(slideCount);
          this.touchObject = {};
          this.slider.trigger("swipe", [this, direction]);
        }
      } else {
        if (this.touchObject.startX !== this.touchObject.curX) {
          this.slideHandler(this.currentSlide);
          this.touchObject = {};
        }
      }
    }
  }, {
    key: "swipeHandler",
    value: function swipeHandler(_ref4) {
      var type = _ref4.type;
      var _ref4$originalEvent = _ref4.originalEvent;
      var source = _ref4$originalEvent === undefined ? {} : _ref4$originalEvent;
      var data = _ref4.data;

      if (!this.options.swipe || "ontouchend" in document && !this.options.swipe) return;

      if (!this.options.draggable && type.indexOf("mouse") !== -1) return;

      var event = arguments[0];

      this.touchObject.fingerCount = source.touches !== undefined ? source.touches.length : 1;
      this.touchObject.minSwipe = this.listWidth / this.options.touchThreshold;

      if (this.options.verticalSwiping) this.touchObject.minSwipe = this.listHeight / this.options.touchThreshold;

      switch (data.action) {
        case "start":
          this.swipeStart(event);
          break;

        case "move":
          this.swipeMove(event);
          break;

        case "end":
          this.swipeEnd(event);
          break;
      }
    }
  }, {
    key: "swipeMove",
    value: function swipeMove(_ref5) {
      var clientX = _ref5.clientX;
      var clientY = _ref5.clientY;
      var originalEvent = _ref5.originalEvent;
      var event = arguments[0];

      var _ref6 = originalEvent || {};

      var touches = _ref6.touches;

      var curLeft = void 0,
          swipeDirection = void 0,
          swipeLength = void 0,
          positionOffset = void 0;

      if (!this.dragging || touches && touches.length !== 1) return false;

      curLeft = this.getLeft(this.currentSlide);
      this.touchObject.curX = touches !== undefined ? touches[0].pageX : clientX;
      this.touchObject.curY = touches !== undefined ? touches[0].pageY : clientY;
      this.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(this.touchObject.curX - this.touchObject.startX, 2)));

      if (this.options.verticalSwiping === true) {
        this.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(this.touchObject.curY - this.touchObject.startY, 2)));
      }

      swipeDirection = this.swipeDirection();
      if (swipeDirection === "vertical") return;

      if (originalEvent !== undefined && this.touchObject.swipeLength > 4) {
        event.preventDefault();
      }

      positionOffset = (this.options.rtl === false ? 1 : -1) * (this.touchObject.curX > this.touchObject.startX ? 1 : -1);
      if (this.options.verticalSwiping === true) {
        positionOffset = this.touchObject.curY > this.touchObject.startY ? 1 : -1;
      }

      swipeLength = this.touchObject.swipeLength;

      this.touchObject.edgeHit = false;

      if (this.options.infinite === false) {
        if (this.currentSlide === 0 && swipeDirection === 'right' || this.currentSlide >= this.getDotCount() && swipeDirection === 'left') {
          swipeLength = this.touchObject.swipeLength * this.options.edgeFriction;
          this.touchObject.edgeHit = true;
        }
      }

      if (this.options.vertical === false) {
        this.swipeLeft = curLeft + swipeLength * positionOffset;
      } else {
        this.swipeLeft = curLeft + swipeLength * (this.$list.height() / this.listWidth) * positionOffset;
      }
      if (this.options.verticalSwiping === true) {
        this.swipeLeft = curLeft + swipeLength * positionOffset;
      }

      if (this.options.fade === true || this.options.touchMove === false) {
        return false;
      }

      if (this.animating === true) {
        this.swipeLeft = null;
        return false;
      }

      this.setCSS(this.swipeLeft);
    }
  }, {
    key: "swipeStart",
    value: function swipeStart(event) {
      var touches = void 0;

      this.interrupted = true;

      if (this.touchObject.fingerCount !== 1 || this.slideCount <= this.options.slidesToShow) {
        this.touchObject = {};
        return false;
      }

      if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) touches = event.originalEvent.touches[0];

      this.touchObject.startX = this.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
      this.touchObject.startY = this.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

      this.dragging = true;
    }
  }, {
    key: "startLoad",
    value: function startLoad() {
      if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
        this.prevArrow.style.display = "none";
        this.nextArrow.style.display = "none";
      }

      if (this.options.dots && this.slideCount > this.options.slidesToShow) this.dotsContainer.style.display = "none";

      this.slider.classList.add("plasmic-loading");
    }
  }, {
    key: "updateArrows",
    value: function updateArrows() {
      var _this23 = this;

      if (this.options.arrows && this.slideCount > this.options.slidesToShow && !this.options.infinite) {
        var setAttrs = function setAttrs(_ref7, _ref8) {
          var _ref10 = slicedToArray(_ref7, 2);

          var prevClass = _ref10[0];
          var prevAria = _ref10[1];

          var _ref9 = slicedToArray(_ref8, 2);

          var nextClass = _ref9[0];
          var nextAria = _ref9[1];

          _this23.prevArrow.setAttribute("aria-disabled", "" + prevAria).classList[prevClass ? "add" : "remove"]("plasmic-disabled");
          _this23.nextArrow.setAttribute("aria-disabled", "" + nextAria).classList[nextClass ? "add" : "remove"]("plasmic-disabled");
        };
        setAttrs([false, false], [false, false]);

        if (this.currentSlide === 0) setAttrs([true, true], [false, false]);else if (this.currentSlide >= this.slideCount - this.options.slidesToShow && !this.options.centerMode) setAttrs([false, false], [true, true]);else if (this.currentSlide >= this.slideCount - 1 && this.options.centerMode) setAttrs([false, false], [true, true]);
      }
    }
  }, {
    key: "updateDots",
    value: function updateDots() {
      var _this24 = this;

      if (this.dots.length) {
        this.dots.forEach(function (dot, n) {
          if (Math.floor(_this24.currentSlide / _this24.options.slidesToScroll) === n) {
            dot.classList.add("plasmic-active");
            dot.setAttribute("aria-hidden", "false");
          } else {
            dot.classList.remove("plasmic-active");
            dot.setAttribute("aria-hidden", "true");
          }
        });
      }
    }
  }, {
    key: "unload",
    value: function unload() {
      this.clonedSlides.remove();
      this.dotsContainer.remove();
      this.prevArrow.remove();
      this.nextArrow.remove();

      this.slides.setAttribute("aria-hidden", "true").classList.remove("plasmic-slide plasmic-active plasmic-visible plasmic-current").return().style.width = "";
    }
  }, {
    key: "visibility",
    value: function visibility() {
      if (this.options.autoplay) this.interrupted = document[this.hidden];
    }
  }, {
    key: "autoPlay",
    get: function get() {
      return !!this.autoPlayTimer;
    },
    set: function set(value) {
      clearInterval(this.autoPlayTimer);
      if (value && this.slideCount > this.options.slidesToShow) this.autoPlayTimer = setInterval(this.autoPlayIterator.bind(this), this.options.autoplaySpeed);
      return value;
    }
  }, {
    key: "currentSlide",
    get: function get() {
      return this._currentSlide;
    },
    set: function set(value) {
      this._currentSlide = value;
      this.currentSlideElem.empty(this.slides[value]);
      return true;
    }
  }, {
    key: "next",
    get: function get() {
      return this.plasmicNext;
    }
  }, {
    key: "prev",
    get: function get() {
      return this.plasmicPrev;
    }
  }, {
    key: "setOption",
    get: function get() {
      return this.plasmicSetOption;
    }
  }, {
    key: "slideCount",
    get: function get() {
      return this.slides.length;
    }
  }]);
  return Plasmic;
}();

if (hasjQuery()) {
  var _window = window;
  var jQuery$1 = _window.jQuery;

  jQuery$1.fn.plasmic = function (opt) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    for (var i = 0; i < this.length; i++) {
      if ((typeof opt === 'undefined' ? 'undefined' : _typeof(opt)) === 'object' && opt !== null || typeof opt === 'undefined') {
        this[i].plasmic = new Plasmic(this[i], opt);
      } else if (typeof opt === 'string') {
        var _i$plasmic;

        if (opt.indexOf('slick') !== -1) {
          opt = opt.replace('slick', '');
          opt = opt[0].toLowerCase() + opt.slice(1);
        }

        return (_i$plasmic = this[i].plasmic)[opt].apply(_i$plasmic, args);
      }
    }

    return this;
  };

  if (!jQuery$1.fn.slick) jQuery$1.fn.slick = function (opt) {
    if ((typeof opt === 'undefined' ? 'undefined' : _typeof(opt)) === 'object' && opt !== null) opt.slickCompatible = true;

    for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    return this.plasmic.apply(this, [opt].concat(args));
  };
}

exports['default'] = Plasmic;
exports.DOMWrapper = DOMWrapper;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=plasmic.js.map
