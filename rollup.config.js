import babel from 'rollup-plugin-babel';

export default {
  dest: 'dist/plasmic.js',
  entry: 'src/index.es6',
  format: 'umd',
  moduleName: 'plasmic',
  plugins: [babel({
    presets: ['es2015-rollup'],
    babelrc: false,
  })],
  sourceMap: true,
};