import Plasmic from './slider.es6';
import { hasjQuery } from './legacy.es6';

export default Plasmic;

if (hasjQuery()) {
  const { jQuery } = window;
  jQuery.fn.plasmic = function (opt, ...args) {
    for (let i = 0; i < this.length; i += 1) {
      if (typeof opt === 'object' && opt !== null || typeof opt === 'undefined') {
        this[i].plasmic = new Plasmic(this[i], opt);
      } else if (typeof opt === 'string') {
        if (opt.indexOf('slick') !== -1) {
          opt = opt.replace('slick', '');
          opt = opt[0].toLowerCase() + opt.slice(1);
        }

        return this[i].plasmic[opt](...args);
      }
    }

    return this;
  };

  if (!jQuery.fn.slick) {
    jQuery.fn.slick = function (opt, ...args) {
      if (typeof opt === 'object' && opt !== null) opt.slickCompatible = true;
      return this.plasmic(opt, ...args);
    };
  }
}
