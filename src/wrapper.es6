/**
 *
 */
import { extendPrototype, WeakStorage, tryCatch } from './utils.es6';
import { dimensions, isNode } from './helpers.es6';
import events from './events.es6';

const storage = new WeakStorage();
const whitespace = /\s+/;

const helpers = {
  callHelper(func, str, val) {
    if (typeof func !== 'function') throw new TypeError('Func wasn\'t specified');

    (function () {
      if (typeof str === 'string' && typeof val === 'string') {
        func.call(this, str, val);
      } else if (typeof str === 'string' && typeof val === 'undefined') {
        str.split(whitespace).forEach(part => {
          func.call(this, part);
        });
      } else if (Array.isArray(str)) {
        str.forEach(part => {
          func.call(this, part);
        });
      } else if (typeof str === 'object') {
        Object.keys(str).forEach(key => {
          func.call(this, key, str[key]);
        });
      }
    }).call(this === helpers ? null : this);
  },
};

export default function DOMWrapper(elem) {
  // if (!new.target) return new DOMWrapper(elem); // FIXME: add support for babel
  if (!(this instanceof DOMWrapper)) return new DOMWrapper(elem);
  this._length = 0;
  this.prev = this;
  this.push(elem);
  this.events = events();
  this.observed = false;
}

DOMWrapper.toArray = function (elems) {
  const filteredElems = [];
  if (!elems) return filteredElems;

  if (typeof elems === 'string') {
    filteredElems.push(...document.querySelectorAll(elems));
  } else if (typeof elems === 'object' && 'length' in elems) {
    [].forEach.call(elems, elem => {
      if (elem instanceof DOMWrapper) {
        filteredElems.push(...elem);
      } else if (elem) {
        filteredElems.push(elem);
      }
    });
  } else if (elems instanceof DOMWrapper) {
    filteredElems.push(...DOMWrapper);
  } else if (elems) {
    filteredElems.push(elems);
  }
  return filteredElems.filter(isNode);
};

DOMWrapper.createWrapper = function (elems) {
  const newWrapper = new this.constructor(elems);
  newWrapper.prev = this;
  if (storage.has(this)) {
    storage.get(this).forEach((listener, i) => {
      listener('new', newWrapper, i);
    });
  }

  return newWrapper;
};

DOMWrapper.createElement = function (tag, { style, parent } = {}) {
  const elem = document.createElement(tag);
  Object.keys(arguments[1]).forEach(prop => {
    if (prop in elem) {
      elem[prop] = arguments[1][prop];
    } else {
      tryCatch(() => {
        elem.setAttribute(prop, arguments[1][prop]);
      });
    }
  });
  if (style) helpers.callHelper(elem.style.setProperty.bind(elem.style), style);
  if (parent) parent.appendChild(elem);
  return elem;
};

/* called once wrapper changes */
DOMWrapper.observeWrapper = function (wrapper, listener) {
  (Array.isArray ? wrapper : [wrapper]).forEach(wrapper => {
    if (wrapper instanceof DOMWrapper) {
      wrapper.observed = true;
      const listeners = storage.get(wrapper) || (storage.set(wrapper, []), storage.get(wrapper));
      listeners.push(listener);
    }
  });
  return true;
};

DOMWrapper.prototype = Object.create(Array.prototype, {
  classList: {
    get: (function () {
      const toArray = (...classes) => {
        const _classes = [];
        classes.forEach(_class => {
          _classes.push(...(typeof _class === 'string' ? _class.split(whitespace) : _class));
        });
        return _classes.filter(_class => _class.length);
      };

      return function () {
        const _this = this;
        return {
          add(...args) {
            const serializedClasses = toArray(...args);
            _this.forEach(elem => {
              if (elem.classList) elem.classList.add(...serializedClasses);
            });
            return this;
          },

          contains: (...classes) => !this.isEmpty && this[0].classList &&
                                    this[0].classList.contains(...classes),

          remove(...args) {
            const serializedClasses = toArray(...args);
            _this.forEach(elem => {
              if (elem.classList) elem.classList.remove(...serializedClasses);
            });
            return this;
          },

          return: () => this,

          toggle(...args) {
            const serializedClasses = toArray(...args);
            let flag;
            if (typeof args[args.length - 1] === 'boolean') flag = args[args.length - 1];

            _this.forEach(elem => {
              if (elem.classList) {
                serializedClasses.forEach(_class => {
                  if (typeof flag === 'boolean') {
                    elem.classList.toggle(_class, flag);
                  } else {
                    elem.classList.toggle(_class);
                  }
                });
              }
            });
            return this;
          },

          toArray,
        };
      };
    })(),
  },

  isEmpty: {
    get() {
      return !this.length;
    },
  },

  length: {
    get() {
      return this._length;
    },

    set(value) {
      this._length = value;
      if (this.observed && storage.has(this)) {
        storage.get(this).forEach((listener, i) => {
          listener('length', this, i);
        });
      }

      return value;
    },
  },

  off: {
    get() {
      return this.removeEventListener;
    },
  },

  once: {
    get() {
      return this.addEventListenerOnce;
    },
  },

  on: {
    get() {
      return this.addEventListener;
    },
  },

  push: {
    value(...args) {
      const serializedArgs = [];
      args.forEach(arg => {
        args.push(...DOMWrapper.toArray(arg));
      });
      [].push.apply(this, serializedArgs);
      return this.length;
    },
  },

  style: (function () { // NOTE: it's a kind of odd stuff for backwards compatibility
    const cache = new WeakStorage();
    return {
      get() {
        if (this.isEmpty) return {};

        if (cache.has(this[0])) return cache.get(this[0]);

        const _this = this;
        const setProperty = function (rule, value) {
          _this.forEach(elem => {
            if (elem.style) {
              helpers.callHelper.call(elem.style, elem.style.setProperty, rule, value);
            }
          });
          return this;
        };

        const removeProperty = function (rule) {
          _this.forEach(elem => {
            if (elem.style) {
              helpers.callHelper.call(elem.style, elem.style.removeProperty, rule);
            }
          });
          return this;
        };

        const styleObj = window.Proxy ? new Proxy({}, {
          get(target, prop) {
            switch (prop) {
              case 'setProperty':
                return setProperty.bind(this);
              case 'removeProperty':
                return removeProperty.bind(this);
              default:
                return _this.isEmpty ? null : _this[0].style[prop];
            }
          },

          set(target, prop, value) {
            _this.forEach(elem => {
              elem.style[prop] = value;
            });
            return true;
          },
        }) : (() => {
          const props = {};
          Object.getOwnPropertyNames(this[0].style).forEach(prop => {
            if (!(prop in props)) {
              Object.defineProperty(props, prop, {
                enumerable: true,
                get: () => this.isEmpty ? null : this[0].style[prop],

                set: value => {
                  this.forEach(elem => {
                    elem.style[prop] = value;
                  });
                  return true;
                },
              });
            }
          });
          props.setProperty = function () {
            setProperty.apply(this, arguments);
            return this;
          };

          props.removeProperty = function () {
            removeProperty.apply(this, arguments);
            return this;
          };

          return props;
        })();

        cache.set(this[0], styleObj);
        return styleObj;
      },
    };
  })(),
});

const { prototype: DOMWrapperPrototype } = DOMWrapper;

DOMWrapperPrototype.includes ||
(DOMWrapperPrototype.includes = function (searchElement /*, fromIndex*/) {
  /* eslint-disable */
  // source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
  var O = Object(this);
  var len = parseInt(O.length) || 0;
  if (len === 0) {
    return false;
  }

  var n = parseInt(arguments[1]) || 0;
  var k;
  if (n >= 0) {
    k = n;
  } else {
    k = len + n;
    if (k < 0) {k = 0;}
  }

  var currentElement;
  while (k < len) {
    currentElement = O[k];
    if (searchElement === currentElement ||
       (searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
      return true;
    }

    k++;
  }
  /* eslint-enable */
  return false;
});

function detectReturn(returnData) {
  const elems = [];
  let ret;
  const elemsFound = returnData.every(result => {
    if (typeof result === 'object') {
      elems.push(...DOMWrapper.toArray(result));
      return true;
    }
    ret = result;
    return false;
  });

  return elemsFound ? DOMWrapper.createWrapper.call(this, elems) : ret;
}

extendPrototype(DOMWrapper, HTMLElement, {
  creation: prop => !(prop in DOMWrapperPrototype),

  finish(result) {
    return detectReturn.call(this, result);
  },

  type: 'array',
});

extendPrototype(DOMWrapper, Element, {
  creation: prop => !(prop in DOMWrapperPrototype),

  finish(result) {
    return detectReturn.call(this, result);
  },

  type: 'array',
});

DOMWrapperPrototype.constructor = DOMWrapper;

DOMWrapperPrototype.appendChild = function (elems) {
  if (!this.isEmpty) {
    if (Array.isArray(elems) || elems instanceof this.constructor) {
      elems.forEach(elem => {
        this[0].appendChild(elem);
      });
    } else {
      this[0].appendChild(elems);
    }
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.cloneNode = function (deep) {
  const elems = [];
  this.forEach(elem => {
    elems.push(elem.cloneNode(!!deep));
  });
  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.empty = function (newElems) {
  while (this.pop());
  if (newElems) this.push(...DOMWrapper.toArray(newElems));
  return this;
};

DOMWrapperPrototype.first = function () {
  return this.eq(0);
};

DOMWrapperPrototype.last = function () {
  return this.eq(this.length - 1);
};

DOMWrapperPrototype.eq = function (n) {
  return DOMWrapper.createWrapper.call(this, this[n]);
};

DOMWrapperPrototype.insertBefore = function (elems, before = null) {
  const [first] = this;
  if (first) {
    const insert = elem => {
      if (Array.isArray(before) || before instanceof this.constructor) {
        before.forEach(before => {
          first.insertBefore(elem, before);
        });
      } else {
        first.insertBefore(elem, before);
      }
    };

    if (Array.isArray(elems) || elems instanceof this.constructor) {
      elems.forEach(insert);
    } else {
      insert(elems);
    }
  }

  return DOMWrapper.createWrapper.call(this, elems);
};

DOMWrapperPrototype.remove || (DOMWrapperPrototype.remove = function () {
  return this.each(elem => {
    if (elem.parentElement) elem.parentElement.removeChild(elem);
  });
});

DOMWrapperPrototype.matches || (DOMWrapperPrototype.matches = function (selector) {
  const matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector ||
                  Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector ||
                  Element.prototype.webkitMatchesSelector;

  return !this.isEmpty && matches && matches.call(this[0], selector);
});

DOMWrapperPrototype.closest || (DOMWrapperPrototype.closest = function (selector) {
  const elems = [];
  this.forEach(elem => {
    while (elem && this.matches.call({
      0: elem = elem.parentElement,
      isEmpty: false,
    }, selector));
    elems.push(elem);
  });
  return DOMWrapper.createWrapper.call(this, elems);
});

DOMWrapperPrototype.width = function (excludePaddings) {
  return this.isEmpty ? null : dimensions.width(this[0], excludePaddings);
};

DOMWrapperPrototype.height = function (includeMargins) {
  return this.isEmpty ? null : dimensions.height(this[0], includeMargins);
};

DOMWrapperPrototype.each = function (callback) {
  this.forEach(callback);
  return this;
};

// FIXME, should expose native dom events
DOMWrapperPrototype.removeEventListener = function (...args) {
  return this.each(elem => {
    this.events(elem).off(...args);
  });
};

DOMWrapperPrototype.addEventListener = function (...args) {
  return this.each(elem => {
    this.events(elem).on(...args);
  });
};

DOMWrapperPrototype.addEventListenerOnce = function (...args) {
  return this.each(elem => {
    this.events(elem).once(...args);
  });
};

DOMWrapperPrototype.setAttribute = function (prop, value) {
  return this.each((elem, i) => {
    helpers.callHelper((rule, value) => {
      elem.setAttribute(rule, String(value).replace('%%i%%', i));
    }, prop, value);
  });
};

DOMWrapperPrototype.removeAttribute = function (rules) {
  return this.each(elem => {
    helpers.callHelper(elem.removeAttribute.bind(elem), rules);
  });
};

DOMWrapperPrototype.trigger = function (...args) {
  return this.each(elem => {
    this.events(elem).trigger(...args);
  });
};
