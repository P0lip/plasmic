/*
  TODO:
  2) expose event handling (on, off, once) to make afterChange etc work
  3) live change (as you slide) when nav
  4) MutationObserver (adding a new node)
  TABINDEX to allow arrows
 */
import { assign, WeakStorage } from './utils.es6';
import { cssProps, dimensions, isHTMLElement } from './helpers.es6';
import events from './events.es6';
import DOMWrapper from './wrapper.es6';
import { hasjQuery, supportSlick } from './legacy.es6';
import defaults, { exposedEvents } from './defaults.es6';

let instanceUid = 0;

const globalStorage = new WeakStorage(); // storage linking each element with Plasmic instance

function SliderDOMWrapper(...args) {
  DOMWrapper.apply(this, ...args);
}

SliderDOMWrapper.prototype = Object.create(DOMWrapper.prototype, {
  classList: {
    get: (function () {
      const { get: original } = Object.getOwnPropertyDescriptor(DOMWrapper.prototype, 'classList');
      const propsToModify = ['add', 'contains', 'remove', 'toggle'];
      const insertSlick = (obj, ...classes) => supportSlick(
              obj
                .toArray(...classes)
                .join(' '),
              true)
              .split(' ');
      return function () {
        const classList = original.call(this);
        Object.keys(classList).forEach(key => {
          if (propsToModify.indexOf(key) !== -1) {
            const originalFunc = classList[key];
            classList[key] = function (...classes) {
              return originalFunc.call(classList, insertSlick(classList, ...classes));
            };
          }
        });
        return classList;
      };
    })(),
  },
});

SliderDOMWrapper.prototype.constructor = SliderDOMWrapper;

export default class Plasmic {
  /**
   * @param  {HTMLElement}       element  - element where Plasmic will be attached to
   * @param  {Object} [settings] settings - for the list of available options, refer to ./defaults.es6
   */
  constructor(element, settings) {
    globalStorage.set(element, this);
    this.options = {};
    assign(this.options, defaults, settings);
    if (this.options.slickCompatible) element.slick = this;
    this.originalSettings = this.options;

    this.events = this.options.slickCompatible && hasjQuery() ? (function () {
      const { parseEvents } = events;
      const _events = events();
      const trigger = _events({}).trigger;
      return function (elem) {
        const events = _events(elem);
        events.trigger = function (event, args) {
          trigger.call(this, event, args);
          parseEvents(event).forEach(({ original }) => {
            window.jQuery(elem).trigger(original, args);
          });
        };
        return events;
      };
    })() : events();
    this.stylings = new WeakStorage();

    this.DOMWrapper = this.options.slickCompatible ? SliderDOMWrapper : DOMWrapper;
    this.currentSlideElem = new this.DOMWrapper();
    this.document = this.events(document);
    this.window = this.events(window);
    this.slider = new this.DOMWrapper(element);
    this.list = new this.DOMWrapper();
    this.slideTrack = new this.DOMWrapper();
    this.slides = new this.DOMWrapper();
    this.clonedSlides = new this.DOMWrapper();
    this.emptySlides = new this.DOMWrapper(); // use with conjunction of fill option
    this.prevArrow = new this.DOMWrapper();
    this.nextArrow = new this.DOMWrapper();
    this.dotsContainer = new this.DOMWrapper();
    this.dots = new this.DOMWrapper();
    this.lazyImages = this.slider.querySelectorAll('img[data-lazy]:not([src])');
    this.allSlides = new this.DOMWrapper();
    DOMWrapper.observeWrapper([this.clonedSlides, this.slides], () => {
      this.allSlides.empty([...this.clonedSlides, ...this.slides]);
    });

    if (this.options.domEvents) {
      exposedEvents.forEach(event => {
        this.slider.on(event, () => {
          this.slider.dispatchEvent(event);
        });
      });
    }

    this.animating = false;
    this.dragging = false;
    this.autoPlayTimer = null;
    this.currentSlide = this.options.initialSlide;
    this.currentDirection = 0;
    this.currentLeft = null;
    this._currentSlide = 0;
    this.direction = 1;
    this.listWidth = -1;
    this.listHeight = -1;
    this.loadIndex = 0;
    this.slideWidth = -1;

    this.sliding = false;
    this.slideOffset = 0;
    this.swipeLeft = null;
    this.touchObject = {};
    this.unplasmiced = false;

    this.activeBreakpoint = null;
    this.breakpoints = [];
    this.breakpointSettings = [];
    this.focussed = false;
    this.interrupted = false;
    this.hidden = this.supportSlick(`${'hidden' in document ? 'hidden' : 'webkitHidden'}.plasmic.plasmic-${instanceUid}`);
    this.paused = true;
    this.positionProp = null;
    this.respondTo = null;
    this.rowCount = 1;
    this.shouldClick = true;
    this.slidesCache = null;
    this.visibilityChange = this.supportSlick(`${'hidden' in document ? 'visibilitychange' : 'webkitvisibilitychange'}.plasmic.plasmic-${instanceUid}`);
    this.windowWidth = 0;
    this.windowTimer = null;

    //this.navTarget

    this.instanceUid = instanceUid += 1;

    this.registerBreakpoints();
    this.init(true);
  }

  activateADA() {
    this.currentSlideElem
      .setAttribute('aria-hidden', 'false')
      .querySelectorAll('a, input, button, select')
      .setAttribute('tabindex', '0');
  }

  animateHeight() {
    if (this.options.slidesToShow === 1 && this.options.adaptiveHeight && !this.options.vertical) {
      const height = this.currentSlideElem.height(true);
      this.list
        .once('transitionend webkitTransitionEnd', () => {
          this.list.style.removeProperty(cssProps.transition);
        })
        .style
          .setProperty({
            [cssProps.transition]: `height ${this.options.cssEase} ${this.options.speed / 1000}s`,
            height: `${height}px`,
          });
    }
  }

  animateSlide(targetLeft, callback) {
    this.animateHeight();

    if (this.options.rtl && !this.options.vertical) targetLeft = -targetLeft;
    this.applyTransition();
    targetLeft = Math.ceil(targetLeft);

    this.slideTrack
      .style
      .setProperty(
        cssProps.transform,
        this.options.vertical ? `translate3d(0,${targetLeft}px,0)` : `translate3d(${targetLeft}px,0,0)`
      );

    if (typeof callback === 'function') {
      setTimeout(() => {
        this.disableTransition();
        callback();
      }, this.options.speed);
    }
  }

  applyTransition() {
    this.slideTrack
        .style
        .setProperty(
          cssProps.transition,
          `${cssProps.transform} ${this.options.speed}ms ${this.options.cssEase}`
        );
  }

  asNavFor(index) {
    const asNavFor = this.getNavTarget();

    asNavFor.forEach(plasmic => {
      const target = globalStorage.get(plasmic);
      if (!target.unplasmiced)
        target.slideHandler(index, true);
    });
  }

  get autoPlay() {
    return !!this.autoPlayTimer;
  }

  set autoPlay(value) {
    clearInterval(this.autoPlayTimer);
    if (value && this.slideCount > this.options.slidesToShow) {
      this.autoPlayTimer = setInterval(this.autoPlayIterator.bind(this),
                                      this.options.autoplaySpeed);
    }
    return value;
  }

  autoPlayIterator() {
    if (!this.paused && !this.interrupted && !this.focussed) {
      let slideTo = this.currentSlide + this.options.slidesToScroll;
      if (!this.options.infinite) {
        if (this.direction === 1 && (this.currentSlide + 1) === (this.slideCount - 1)) {
          this.direction = 0;
        } else if (this.direction === 0) {
          slideTo = this.currentSlide - this.options.slidesToScroll;
          if (this.currentSlide - 1 === 0) {
            this.direction = 1;
          }
        }
      }

      this.slideHandler(slideTo);
    }
  }

  build() {
    this.list.empty(document.createElement('div'));
    this.slideTrack.empty(this.list.appendChild(document.createElement('div')));
  }

  buildArrows() {
    if (this.options.arrows) {
      const regex = /^(?:\s*(<[\w\W]+>)[^>]*)$/; // taken from jQuery source code.
      const createArrow = type => {
        const button = document.createElement('button');
        const option = this.options[type];
        if (!option) return button;

        if (typeof option === 'string' && regex.test(option)) {
          const root = document.createElement('div');
          root.insertAdjacentHTML('afterbegin', this.options[type]);
          const { firstElementChild: button } = root;
          if (this.options.slickCompatible) {
            button.className = button.className.replace(button.className,
                                                        this.supportSlick(button.className));
          }
          return button;
        }

        if (option instanceof Element) return this.options[type];

        return button;
      };
      this.prevArrow.empty(createArrow('prevArrow'));
      this.nextArrow.empty(createArrow('nextArrow'));
      const arrows = new this.DOMWrapper([this.prevArrow, this.nextArrow]);

      arrows.classList.add('plasmic-arrow');

      if (this.slideCount > this.options.slidesToShow) {
        arrows
          .removeAttribute('aria-hidden tabindex')
          .classList
            .remove('plasmic-hidden');

        this.slider
          .insertBefore(this.prevArrow, this.slider.firstElementChild)
          .prev
          .appendChild(this.nextArrow);
      }
    }
  }

  buildDots() {
    if (this.options.dots && this.slideCount > this.options.slidesToShow) {
      this.slider.classList.add('plasmic-dotted');

      this.dotsContainer.push(DOMWrapper.createElement('ul', {
        className: this.options.dotsClass,
      }));

      const container = this.dotsContainer;

      for (let i = 0, dotsCount = this.getDotCount(); i <= dotsCount; i += 1) {
        const dot = container.appendChild(document.createElement('li'));
        this.dots.push(dot);
        dot.appendChild(this.options.customPaging.call(DOMWrapper, i));
      }

      this.dots
        .first()
        .setAttribute('aria-hidden', 'false')
        .classList.add('plasmic-active');

      this.slider.appendChild(container);
    }
  }

  buildOut() {
    this.slides.push(...this.slider
      .children
      .filter(child => !child.classList.contains('plasmic-cloned') && !this.slides.includes(child))
    );

    this.slider
      .classList
        .add('plasmic-slider')
        .return()
      .appendChild(this.list);
    this.list
      .setAttribute('aria-live', 'polite')
      .classList.add('plasmic-list');

    this.slides
      .classList
        .add('plasmic-slide')
        .return()
      .setAttribute('data-plasmic-index', '%%i%%')
      .each(slide => {
        this.stylings.set(slide, slide.getAttribute('style') || '');
      });

    this.slideTrack
      .appendChild(this.slides)
      .prev
        .classList
        .add('plasmic-track');

    if (this.options.centerMode || this.options.swipeToSlide) this.options.slidesToScroll = 1;

    this.lazyImages.classList.add('plasmic-loading');

    this.setupInfinite();
    this.buildArrows();
    this.buildDots();
    this.updateDots();

    this.setSlideClasses(typeof this.currentSlide === 'number' ? this.currentSlide : 0);

    if (this.options.draggable) this.list.classList.add('draggable');
  }

  buildRows() {
    const newSlides = document.createDocumentFragment();

    if (this.options.rows > 1) {
      const originalSlides = this.slides;
      const slidesPerSection = this.options.slidesPerRow * this.options.rows;
      const numOfSlides = Math.ceil(this.slideCount / slidesPerSection);

      for (let a = 0; a < numOfSlides; a += 1) {
        const slide = document.createElement('div');
        for (let b = 0; b < this.options.rows; b += 1) {
          const row = document.createElement('div');
          for (let c = 0; c < this.options.slidesPerRow; c += 1) {
            const target = (a * slidesPerSection + ((b * this.options.slidesPerRow) + c));
            let originalSlide;
            if (originalSlides[target]) {
              originalSlide = row.appendChild(originalSlides[target]);
            }
            if (typeof originalSlide !== 'undefined') {
              originalSlide.style.width = `${100 / this.options.slidesPerRow}%`;
              originalSlide.style.display = 'inline-block';
            }
            slide.appendChild(row);
          }
          newSlides.appendChild(slide);
        }
      }

      this.slider.children.remove();

      this.slider.appendChild(newSlides);
    }
  }

  changeSlide(event, dontAnimate) {
    const { currentTarget: target, data: { message, index } } = events;
    const wrappedTarget = new this.DOMWrapper(target);

    // If target is a link, prevent default action.
    if (wrappedTarget.tagName === 'A' && event.preventDefault) event.preventDefault();

      // If target is not the <li> element (ie: a child), find the <li>.
    if (wrappedTarget.tagName !== 'LI') wrappedTarget.closest('li');

    const unevenOffset = this.slideCount % this.options.slidesToScroll !== 0;
    const indexOffset = unevenOffset ? 0 : (this.slideCount - this.currentSlide) % this.options.slidesToScroll;

    switch (message) {
      case 'previous':
        {
          const slideOffset = indexOffset === 0 ? this.options.slidesToScroll : this.options.slidesToShow - indexOffset;
          if (this.slideCount > this.options.slidesToShow) {
            this.slideHandler(this.currentSlide - slideOffset, false, dontAnimate);
          }
        }
        break;

      case 'next':
        {
          const slideOffset = indexOffset === 0 ? this.options.slidesToScroll : indexOffset;
          if (this.slideCount > this.options.slidesToShow) {
            this.slideHandler(this.currentSlide + slideOffset, false, dontAnimate);
          }
        }
        break;

      case 'index':
        {
          const newIndex = index === 0 ? 0 : index || [].indexOf.call(target.parentElement.children, target) * this.options.slidesToScroll;
          this.slideHandler(this.checkNavigable(newIndex), false, dontAnimate);
          wrappedTarget.children.forEach(child => {
            this.events(child).trigger('focus');
          });
        }
        break;
    }
  }

  checkNavigable(index) {
    const navigables = this.getNavigableIndexes();
    let prevNavigable = 0;
    let newIndex = index;

    if (index > navigables[navigables.length - 1]) {
      newIndex = navigables[navigables.length - 1];
    } else {
      Object.keys(navigables).every(n => {
        if (newIndex < navigables[n]) {
          newIndex = prevNavigable;
          return false;
        }
        prevNavigable = navigables[n];
        return true;
      });
    }

    return newIndex;
  }

  checkResponsive(initial, forceUpdate) {
    const { innerWidth: windowWidth } = window;
    const sliderWidth = this.slider.width();

    let respondToWidth;
    let triggerBreakpoint;

    if (this.respondTo === 'window') {
      respondToWidth = windowWidth;
    } else if (this.respondTo === 'slider') {
      respondToWidth = sliderWidth;
    } else if (this.respondTo === 'min') {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }

    if (this.options.responsive && this.options.responsive.length) {
      let targetBreakpoint = null;
      Object.keys(this.breakpoints).forEach(breakpoint => {
        if (!this.originalSettings.mobileFirst) {
          if (respondToWidth < this.breakpoints[breakpoint]) {
            targetBreakpoint = this.breakpoints[breakpoint];
          }
        } else if (respondToWidth > this.breakpoints[breakpoint]) {
          targetBreakpoint = this.breakpoints[breakpoint];
        }
      });

      /* jshint -W004 */
      if (targetBreakpoint !== null) {
        if (this.activeBreakpoint !== null) {
          if (targetBreakpoint !== this.activeBreakpoint || forceUpdate) {
            this.activeBreakpoint = targetBreakpoint;
            if (this.breakpointSettings[targetBreakpoint] === 'unplasmic') {
              this.unplasmic(targetBreakpoint);
            } else {
              assign(this.options, this.originalSettings, this.breakpointSettings[targetBreakpoint]);
              if (initial) this.currentSlide = this.options.initialSlide;
              this.refresh(initial);
            }
            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          this.activeBreakpoint = targetBreakpoint;
          if (this.breakpointSettings[targetBreakpoint] === 'unplasmic') {
            this.unplasmic(targetBreakpoint);
          } else {
            assign(this.options, this.originalSettings, this.breakpointSettings[targetBreakpoint]);
            if (initial) this.currentSlide = this.options.initialSlide;
            this.refresh(initial);
            triggerBreakpoint = targetBreakpoint;
          }
        }
      } else if (this.activeBreakpoint !== null) {
        this.activeBreakpoint = null;
        this.options = this.originalSettings;
        if (initial) this.currentSlide = this.options.initialSlide;
        this.refresh(initial);
        triggerBreakpoint = targetBreakpoint;
      }
      /* jshint +W004 */

      // only trigger breakpoints during an actual break. not on initialize.
      if (!initial && triggerBreakpoint) {
        this.slider.trigger('breakpoint', [this, triggerBreakpoint]);
      }
    }
  }

  cleanUpEvents() {
    if (this.options.dots) {
      this.dots
        .off('click.plasmic', this.changeSlide)
        .off('mouseenter.plasmic.bound mouseleave.plasmic.bound');
    }

    this.slider.off('focus.plasmic blur.plasmic');

    this.prevArrow.off('click.plasmic');
    this.nextArrow.off('click.plasmic');

    this.list
      .off('touchstart.plasmic touchmove.plasmic touchend.plasmic touchcancel.plasmic')
      .off('mousedown.plasmic mousemove.plasmic moseup.plasmic mouseleave.plasmic');

    this.document.off(this.visibilityChange, this.visibility);

    this.cleanUpSlideEvents();

    if (this.options.accessibility) this.list.off('keydown.plasmic.bound');

    if (this.options.focusOnSelect) this.allSlides.off('click.plasmic.bound');

    this.window
      .off('orientationchange.plasmic.bound')
      .off('resize.plasmic.bound')
      .off('load.plasmic.bound');

    this.slideTrack.querySelectorAll(':not([draggable=true])').forEach(node => {
      this.events(node).off('dragstart', this.preventDefault);
    });

    this.document.off('DOMContentLoaded.plasmic');
  }

  cleanUpSlideEvents() {
    this.list.off('mouseenter.plasmic mouseleave.plasmic');
  }

  cleanUpRows() {
    if (this.options.rows > 1) {
      const originalSlides = this.slides.children.children;
      originalSlides.removeAttribute('style');
      this.slider.children.remove();
      this.slider.appendChild(originalSlides);
    }
  }

  clickHandler(event) {
    if (!this.shouldClick) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  }

  get currentSlide() {
    return this._currentSlide;
  }

  set currentSlide(value) {
    this._currentSlide = value;
    this.currentSlideElem.empty(this.slides[value]);
    return true;
  }

  destroy(refresh) {
    this.autoPlay = false;
    this.touchObject = {};
    this.cleanUpEvents();

    this.clonedSlides
      .remove()
      .empty();

    this.dotsContainer
      .remove()
      .empty();

    this.prevArrow.remove();
    this.nextArrow.remove();

    this.slider.appendChild(
      this.slides
        .classList
          .remove('plasmic-slide plasmic-active plasmic-center plasmic-visible plasmic-current')
          .return()
        .removeAttribute('aria-hidden data-plasmic-index')
        .each(slide => {
          slide.setAttribute('style', this.stylings.get(slide));
        })
    );

    this.list.remove();
    this.slider.classList.remove('plasmic-slider plasmic-initialized plasmic-dotted');
    this.cleanUpRows();

    this.unplasmiced = true;

    if (!refresh) {
      this.slider.trigger('destroy', [this]);
      globalStorage.delete(this.slider);
    }
  }

  disableTransition() {
    this.slideTrack.style.removeProperty(cssProps.transition);
  }

  focusHandler() {
    this.slider
      .off('focus.focus-handler blur.focus-handler')
      .on('focus.focus-handler blur.focus-handler', e => {
        e.stopImmediatePropagation();
        setTimeout(function () {
          if (this.options.pauseOnFocus) {
            this.focussed = document.activeElement === e.target;
            this.autoPlay = true;
          }
        }, 0);
      });
  }

  getDotCount() {
    let pagerQty = 0;
    let breakPoint = 0;
    let counter = 0;

    if (this.options.infinite) {
      while (breakPoint < this.slideCount) {
        pagerQty += 1;
        breakPoint = counter + this.options.slidesToScroll;
        if (this.options.slidesToScroll <= this.options.slidesToShow) {
          counter += this.options.slidesToScroll;
        } else {
          counter += this.options.slidesToShow;
        }
      }
    } else if (this.options.centerMode) {
      pagerQty = this.slideCount;
    } else if (!this.options.asNavFor) {
      pagerQty = 1 + Math.ceil((this.slideCount - this.options.slidesToShow) / this.options.slidesToScroll);
    } else {
      while (breakPoint < this.slideCount) {
        pagerQty += 1;
        breakPoint = counter + this.options.slidesToScroll;
        counter += this.options.slidesToScroll <= this.options.slidesToShow ? this.options.slidesToScroll : this.options.slidesToShow;
      }
    }

    return pagerQty - 1;
  }

  getLeft(slideIndex) {
    if (!this.slides.length) return;

    let targetLeft;
    let targetSlide;

    this.slideOffset = 0;
    const verticalHeight = this.slides.first().height(true);
    let verticalOffset = 0;

    if (this.options.infinite) {
      if (this.slideCount > this.options.slidesToShow) {
        this.slideOffset = this.slideWidth * this.options.slidesToShow * -1;
        verticalOffset = (verticalHeight * this.options.slidesToShow) * -1;
      }

      if (this.slideCount % this.options.slidesToScroll !== 0) {
        if (slideIndex + this.options.slidesToScroll > this.slideCount && this.slideCount > this.options.slidesToShow) {
          if (slideIndex > this.slideCount) {
            if (!this.options.fill) {
              const diff = this.slideCount / this.options.slidesToShow;
              const idealSize = (Math.floor(diff) + (diff > this.options.slidesToShow)) * this.options.slidesToShow;
              verticalOffset = this.slideOffset = ((this.options.slidesToShow - (slideIndex - idealSize)) * this.slideWidth) * -1;
            } else {
              verticalOffset = this.slideOffset = ((this.options.slidesToShow - (slideIndex - this.slideCount)) * this.slideWidth) * -1;
            }
          } else {
            verticalOffset = this.slideOffset = (((this.slideCount - slideIndex) + (this.options.slidesToShow - this.slideCount + slideIndex)) * this.slideWidth) * -1;
          }
        }
      }
    } else if (slideIndex + this.options.slidesToShow > this.slideCount) {
      this.slideOffset = ((slideIndex + this.options.slidesToShow) - this.slideCount) * this.slideWidth;
      verticalOffset = ((slideIndex + this.options.slidesToShow) - this.slideCount) * verticalHeight;
    }

    if (this.slideCount <= this.options.slidesToShow) {
      this.slideOffset = 0;
      verticalOffset = 0;
    }

    if (this.options.centerMode && this.options.infinite) {
      this.slideOffset += this.slideWidth * Math.floor(this.options.slidesToShow / 2) - this.slideWidth;
    } else if (this.options.centerMode) {
      this.slideOffset = this.slideWidth * Math.floor(this.options.slidesToShow / 2);
    }

    if (!this.options.vertical) {
      targetLeft = ((slideIndex * this.slideWidth) * -1) + this.slideOffset;
    } else {
      targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
    }

    if (this.options.variableWidth) {
      /* jshint -W004 */
      if (this.slideCount <= this.options.slidesToShow || !this.options.infinite) {
        targetSlide = this.allSlides[slideIndex];
      } else {
        targetSlide = this.allSlides[slideIndex + this.options.slidesToShow];
      }

      if (this.options.rtl) {
        if (targetSlide) {
          targetLeft = (this.slideTrack.offsetWidth - targetSlide.offsetLeft - targetSlide.offsetWidth) * -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide ? targetSlide.offsetLeft * -1 : 0;
      }

      if (this.options.centerMode) {
        if (this.slideCount <= this.options.slidesToShow || !this.options.infinite) {
          targetSlide = this.allSlides[slideIndex];
        } else {
          targetSlide = this.allSlides[slideIndex + this.options.slidesToShow + 1];
        }

        if (this.options.rtl) {
          if (targetSlide) {
            targetLeft = (this.slideTrack.offsetWidth - targetSlide.offsetLeft - targetSlide.offsetWidth) * -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }
        targetLeft += (this.list.offsetWidth - targetSlide.offsetWidth) / 2;
      }
    }

    return targetLeft;
  }

  getNavigableIndexes() {
    const indexes = [];
    let counter = 0;
    let breakpoint = 0;
    let max;

    if (!this.options.infinite) {
      max = this.slideCount;
    } else {
      breakpoint = this.options.slidesToScroll * -1;
      counter = this.options.slidesToScroll * -1;
      max = this.slideCount * 2;
    }

    while (breakpoint < max) {
      indexes.push(breakpoint);
      breakpoint = counter + this.options.slidesToScroll;
      if (this.options.slidesToScroll <= this.options.slidesToShow) {
        counter += this.options.slidesToScroll;
      } else {
        counter += this.options.slidesToShow;
      }
    }

    return indexes;
  }

  getNavTarget() {
    if (this.navTarget) return this.navTarget;
    const { options: { asNavFor } } = this;
    const elems = [];

    if (asNavFor) {
      if (isHTMLElement(asNavFor) && asNavFor !== this.slider[0]) {
        elems.push(asNavFor);
      } else if (typeof asNavFor === 'string') {
        elems.push(...[].filter
                        .call(document.querySelectorAll(asNavFor), elem => elem !== this.slider));
      }
    }

    return elems;
  }

  getSlideCount() {
    if (this.options.swipeToSlide) {
      const centerOffset = this.options.centerMode ? this.slideWidth * Math.floor(this.options.slidesToShow / 2) : 0;
      let swipedSlide;
      this.slides.every(slide => {
        if (slide.offsetLeft - centerOffset + (slide.offsetWidth / 2) > (this.swipeLeft * -1)) {
          swipedSlide = slide;
          return false;
        }
        return true;
      });

      if (!swipedSlide) return 1;

      return Math.abs(swipedSlide.getAttribute('data-plasmic-index') - this.currentSlide);
    }

    return this.options.slidesToScroll;
  }

  goTo(slide, dontAnimate) {
    this.changeSlide({
      data: {
        message: 'index',
        index: parseInt(slide, 10),
      },
    }, dontAnimate);
  }

  init(creation) {
    if (!this.slider.classList.contains('plasmic-initialized')) {
      this.slider.classList.add('plasmic-initialized');

      this.build();
      this.buildRows();
      this.buildOut();
      this.setProps();
      this.startLoad();
      this.loadSlider();
      this.initializeEvents();
      this.updateArrows();
      this.updateDots();
      this.checkResponsive(true);
      this.focusHandler();
    }

    this.unplasmiced = false;

    if (creation) this.slider.trigger('init');

    if (this.options.accessibility) this.initADA();

    if (this.options.autoplay) {
      this.paused = false;
      this.autoPlay = true;
    }
  }

  initADA() {
    this.clonedSlides
      .setAttribute({
        'aria-hidden': 'true',
        tabindex: '-1',
      })
      .querySelectorAll('a, input, button, select')
      .setAttribute('tabindex', '-1');

    this.slideTrack.setAttribute('role', 'listbox');

    this.slides.forEach((slide, i) => {
      slide.setAttribute('role', 'option');
      if (this.options.dots) {
        const describedBySlideId = this.options.centerMode ? i : Math.floor(i / this.options.slidesToShow);
        // Evenly distribute aria-describedby tags through available dots.
        slide.setAttribute('aria-describedby', `plasmic-slide${this.instanceUid}${describedBySlideId}`);
      }
    });

    if (this.dotsContainer) {
      this.dotsContainer.setAttribute('role', 'tablist');
      if (!this.dots.isEmpty) {
        this.dots
          .setAttribute({
            role: 'presentation',
            'aria-selected': 'false',
            'aria-controls': `navigation${this.instanceUid}%%i%%`,
            id: `plasmic-slide${this.instanceUid}%%i%%`,
          })
          .first()
          .setAttribute('aria-selected', 'true');

        this.dotsContainer
          .querySelectorAll('button')
          .setAttribute('role', 'button');

        this.slider.setAttribute('role', 'toolbar');
      }
    }

    this.activateADA();
  }

  initArrowEvents() {
    if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
      this.prevArrow
        .off('click.plasmic')
        .on('click.plasmic', this.changeSlide.bind(this), {
          message: 'previous',
        });
      this.nextArrow
        .off('click.plasmic')
        .on('click.plasmic', this.changeSlide.bind(this), {
          message: 'next',
        });
    }
  }

  initDotEvents() {
    if (this.options.dots && this.slideCount > this.options.slidesToShow) {
      this.dots.on('click.plasmic', this.changeSlide.bind(this), {
        message: 'index',
      });
    }

    if (this.options.dots && this.options.pauseOnDotsHover) {
      this.dots.on('mouseenter.plasmic mouseleave.plasmic', this.changeSlide.bind(this));
    }
  }

  initializeEvents() {
    this.initArrowEvents();
    this.initDotEvents();
    this.initSlideEvents();

    const boundSwipeHandler = this.swipeHandler.bind(this);

    this.list
      .on('touchstart.plasmic mousedown.plasmic', boundSwipeHandler, {
        action: 'start',
      })
      .on('touchmove.plasmic mousemove.plasmic', boundSwipeHandler, {
        action: 'move',
      })
      .on('touchend.plasmic mouseup.plasmic touchcancel.plasmic mouseleave.plasmic',
        boundSwipeHandler, {
          action: 'end',
        }
      )
      .on('click.plasmic', this.clickHandler.bind(this));

    if (this.options.accessibility) {
      this.list.on('keydown.plasmic', this.keyHandler.bind(this));
    }

    if (this.options.focusOnSelect) {
      this.allSlides.forEach(slide => {
        this.events(slide).on('click.plasmic.bound', this.selectHandler.bind(this));
      });
    }

    this.window
      .on(`orientationchange.plasmic.bound.plasmic-${this.instanceUid}`,
        this.orientationChange.bind(this))
      .on(`resize.plasmic.bound.plasmic-${this.instanceUid}`,
        this.resize.bind(this))
      .on(`load.plasmic.bound.plasmic-${this.instanceUid}`,
        this.setPosition.bind(this));

    [].forEach.call(this.slideTrack.querySelectorAll(':not([draggable=true])'), node => {
      this.events(node).on('dragstart', e => {
        e.preventDefault();
      });
    });

    this.document
      .on(this.visibilityChange, this.visibility.bind(this))
      .on('DOMContentLoaded.plasmic', this.setPosition.bind(this));
  }

  initUI() {
    if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
      this.prevArrow.style.display = '';
      this.nextArrow.style.display = '';
    }

    if (this.options.dots && this.slideCount > this.options.slidesToShow) {
      this.dotsContainer.style.display = '';
    }
  }

  initSlideEvents() {
    if (this.options.pauseOnHover) {
      this.list
        .on('mouseenter.plasmic', this.interrupt.bind(this, true))
        .on('mouseleave.plasmic', this.interrupt.bind(this, false));
    }
  }

  interrupt(toggle) {
    if (!toggle) this.autoPlay = true;
    this.interrupted = toggle;
    return toggle;
  }

  keyHandler({ target, keyCode }) {
    // Dont slide if the cursor is inside the form fields and arrow keys are pressed
    if (!/TEXTAREA|INPUT|SELECT/.test(target.tagName)) {
      if (keyCode === 37 && this.options.accessibility) {
        this.changeSlide({
          data: {
            message: this.options.rtl ? 'next' : 'previous',
          },
        });
      } else if (keyCode === 39 && this.options.accessibility) {
        this.changeSlide({
          data: {
            message: this.options.rtl ? 'previous' : 'next',
          },
        });
      }
    }
  }

  lazyLoad() {
    const loadImages = () => {
      this.lazyImages.forEach(img => {
        const source = img.dataset.lazy;
        const tempImg = DOMWrapper.createElement('img', {
          onerror: () => {
            new this.DOMWrapper(img)
              .removeAttribute('data-lazy')
              .classList
                .remove('plasmic-loading')
                .add('plasmic-lazyload-error');

            this.slider.trigger('lazyLoadError', [this, img, source]);
          },

          onload: () => {
            new this.DOMWrapper(img)
              .setAttribute('src', source)
              .removeAttribute('data-lazy')
              .classList
                .remove('plasmic-loading');
            if (this.options.adaptiveHeight) this.setPosition();
            this.slider.trigger('lazyLoaded', [this, img, source]);
          },
        });
        tempImg.src = source;
      });
    };

    let rangeStart;
    let rangeEnd;
    if (this.options.centerMode) {
      if (this.options.infinite) {
        rangeStart = this.currentSlide + (this.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + this.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(0, this.currentSlide - (this.options.slidesToShow / 2 + 1));
        rangeEnd = 2 + (this.options.slidesToShow / 2 + 1) + this.currentSlide;
      }
    } else {
      if (this.options.infinite) {
        rangeStart = this.options.slidesToShow + this.currentSlide;
      } else {
        rangeStart = this.currentSlide;
      }
      rangeEnd = Math.ceil(rangeStart + this.options.slidesToShow);
    }

    loadImages(this.allSlides.slice(rangeStart, rangeEnd));

    if (this.slideCount <= this.options.slidesToShow) {
      loadImages(this.allSlides);
    } else if (this.currentSlide >= this.slideCount - this.options.slidesToShow) {
      loadImages(this.clonedSlides.slice(0, this.options.slidesToShow));
    } else if (this.currentSlide === 0) {
      loadImages(this.clonedSlides.slice(this.options.slidesToShow * -1));
    }
  }

  loadSlider() {
    this.setPosition();
    this.slideTrack.style.opacity = 1;
    this.slider.classList.remove('plasmic-loading');
    this.initUI();

    if (this.options.lazyLoad === 'progressive') this.progressiveLazyLoad();
  }

  get next() {
    return this.plasmicNext;
  }

  off(...args) {
    return this.events(this.slider).off(...args);
  }

  on(...args) {
    return this.events(this.slider).on(...args);
  }

  once(...args) {
    return this.events(this.slider).once(...args);
  }

  orientationChange() {
    this.checkResponsive();
    this.setPosition();
  }

  pause() {
    this.autoPlay = false;
    this.paused = true;
  }

  play() {
    this.autoPlay = true;
    this.options.autoPlay = true;
    this.paused = false;
    this.focussed = false;
    this.interrupted = false;
  }

  postSlide(index) {
    if (!this.unplasmiced) {
      this.slider.trigger('afterChange', [this, index]);

      this.animating = false;
      this.setPosition();
      this.swipeLeft = null;

      if (this.options.autoplay) this.autoPlay = true;

      if (this.options.accessibility) this.initADA();
    }
  }

  get prev() {
    return this.plasmicPrev;
  }

  refresh(initializing) {
    const lastVisibleIndex = this.slideCount - this.options.slidesToShow;

    // in non-infinite sliders, we don't want to go past the
    // last visible index.
    if (!this.options.infinite && (this.currentSlide > lastVisibleIndex)) {
      this.currentSlide = lastVisibleIndex;
    }

    // if less slides than to show, go to start.
    if (this.slideCount <= this.options.slidesToShow) this.currentSlide = 0;

    const { currentSlide } = this;

    this.destroy(true);
    this.animating = false,
    this.dragging = false;
    this.sliding = false;
    this.slideOffset = 0;
    this.swipeLeft = null;
    this.currentLeft = 0;
    this.currentSlide = currentSlide;
    this.currentDirection = 0;
    this.direction = 1;
    this.listWidth = -1;
    this.listHeight = -1;
    this.slideWidth = -1;
    this.loadIndex = 0;
    this.unplasmiced = false;
    this.touchObject = {};

    this.dots.empty();
    this.nextArrow.empty();
    this.prevArrow.empty();
    this.slideTrack.empty();
    this.list.empty();

    this.init();

    if (!initializing) {
      this.changeSlide({
        data: {
          message: 'index',
          index: currentSlide,
        },
      }, false);
    }
  }

  reinit() {
    this.slides.classList.add('plasmic-slide');

    if (this.currentSlide >= this.slideCount && this.currentSlide !== 0) {
      this.currentSlide = this.currentSlide - this.options.slidesToScroll;
    }

    if (this.slideCount <= this.options.slidesToShow) this.currentSlide = 0;

    this.registerBreakpoints();

    this.setProps();
    this.setupInfinite();
    this.buildArrows();
    this.updateArrows();
    this.initArrowEvents();
    this.buildDots();
    this.updateDots();
    this.initDotEvents();
    this.cleanUpSlideEvents();
    this.initSlideEvents();

    this.checkResponsive(false, true);

    if (this.options.focusOnSelect) {
      this.allSlides.forEach(slide => {
        this.events(slide).on('click.plasmic', this.selectHandler.bind(this));
      });
    }

    this.setSlideClasses(typeof this.currentSlide === 'number' ? this.currentSlide : 0);

    this.setPosition();
    this.focusHandler();

    this.paused = !this.options.autoplay;
    this.autoPlay = this.options.autoplay;

    this.events(this.slide).trigger('reInit', [this]);
  }

  registerBreakpoints() {
    const responsiveSettings = this.options.responsive || [];

    const dedupe = (arr, prop) => {
      arr.slice().forEach(elem => {
        if (elem.breakpoint === prop) arr.splice(arr.indexOf(elem), 1);
      });
    };

    if (Array.isArray(responsiveSettings) && responsiveSettings.length) {
      this.respondTo = this.options.respondTo || 'window';
      responsiveSettings.forEach(breakpoint => {
        const { breakpoint: currentBreakpoint, settings } = breakpoint;
        dedupe(this.breakpoints, currentBreakpoint);
        if (typeof currentBreakpoint === 'string' && this.options.slickCompatible) {
          this.breakpoints.push('unslick');
        } else {
          this.breakpoints.push(currentBreakpoint); // TODO: test
        }
        this.breakpointSettings[currentBreakpoint] = settings;
      });
      this.breakpoints.sort((a, b) => this.options.mobileFirst ? a - b : b - a);
    }
  }

  resize() {
    if (window.innerWidth !== this.windowWidth) {
      // TODO: add prefixes for webkit/blink browsers
      window.cancelAnimationFrame(this.windowDelay);
      this.windowDelay = window.requestAnimationFrame(() => {
        this.windowWidth = window.innerWidth;
        this.checkResponsive();
        if (!this.unplasmiced) this.setPosition();
      });
    }
  }

  selectHandler({ target }) {
    const targetElement = target.classList.contains('plasmic-slide') ? target : (function () {
      let parent = target;
      while (parent && !(parent = parent.parentElement).classList.contains('plasmic-slide'));
      return parent;
    })();

    const index = targetElement ? parseInt(targetElement.dataset.plasmicIndex, 10) : 0;

    if (this.slideCount <= this.options.slidesToShow) {
      this.setSlideClasses(index);
      this.asNavFor(index);
      return;
    }

    this.slideHandler(index);
  }

  get setOption() {
    return this.plasmicSetOption;
  }

  plasmicPrev() {
    this.changeSlide({
      data: {
        message: 'previous',
      },
    });
  }

  plasmicSetOption() {
    /**
     * accepts arguments in format of:
     *
     *  - for changing a single option's value:
     *     .plasmic('setOption', option, value, refresh )
     *
     *  - for changing a set of responsive options:
     *     .plasmic('setOption', 'responsive', [{}, ...], refresh )
     *
     *  - for updating multiple values at once (not responsive)
     *     .plasmic('setOption', { 'option': value, ... }, refresh )
     */

    /* jshint -W004 */
    if (typeof arguments[0] === 'object' && arguments[0] !== null)
      var option =  arguments[0],
          refresh = arguments[1],
          type = 'multiple';
    else if (typeof arguments[0] === 'string')
      var option =  arguments[0],
          value = arguments[1],
          refresh = arguments[2],
          type = arguments[0] === 'responsive' && Array.isArray(arguments[1]) ? 'responsive' : typeof arguments[1] ? 'single' : '';
    /* jshint +W004 */
    switch (type) {
      case 'single':
        this.options[option] = value;
        break;
      case 'multiple':
        Object.keys(option).forEach(key => {
          this.options[key] = option[key];
        });
        break;
      case 'responsive':
        arguments[1].forEach(item => {
          if (!Array.isArray(this.options.responsive)) this.options.responsive = [];
          this.options.responsive.push(item);
          this.registerBreakpoints(); // dedupe
        });
    }

    if (refresh) {
      this.unload();
      this.reinit();
    }
  }

  setCSS(position) {
    if (this.options.rtl) position = -position;

    const x = this.positionProp === 'left' ? `${Math.ceil(position)}px` : '0';
    const y = this.positionProp === 'top' ? `${Math.ceil(position)}px` : '0';

    this.slideTrack.style.setProperty(cssProps.transform, `translate3d(${x},${y},0)`);
  }

  setDimensions() {
    if (!this.options.vertical) {
      if (this.options.centerMode) {
        this.list.style.padding = `0 ${this.options.centerPadding}`;
      }
    } else {
      this.list.style.height = `${this.slides.height(true) * this.options.slidesToShow}px`;
      if (this.options.centerMode) {
        this.list.style.padding = `${this.options.centerPadding} 0`;
      }
    }

    this.listWidth = this.list.width(true);
    this.listHeight = this.list.height();
    if (!this.options.vertical && !this.options.variableWidth) {
      this.slideWidth = Math.ceil(this.listWidth / this.options.slidesToShow);
      this.slideTrack.style.width = `${Math.ceil((this.slideWidth * this.allSlides.length))}px`;
    } else if (this.options.variableWidth) {
      this.slideTrack.style.width = 5000 * this.slideCount + 'px';
    } else {
      this.slideWidth = Math.ceil(this.listWidth);
      this.slideTrack
           .style
           .height = `${Math.ceil(this.slides.height(true) * this.allSlides.length)}px`;
    }

    if (!this.options.variableWidth && this.slides.length) {
      const { marginLeft, marginRight } = window.getComputedStyle(this.slides[0]);
      const offset = parseInt(marginLeft, 10) + parseInt(marginRight, 10);
      this.allSlides.style.width = `${this.slideWidth - offset}px`;
    }
  }

  setHeight() {
    if (this.options.slidesToShow === 1 && this.options.adaptiveHeight && !this.options.vertical) {
      this.list.style.height = `${dimensions.height(this.slides[this.currentSlide])}px`;
    }
  }

  setPosition() {
    this.setDimensions();
    this.setHeight();
    this.setCSS(this.getLeft(this.currentSlide));
    this.slider.trigger('setPosition', [this]);
  }

  setProps() {
    this.positionProp = this.options.vertical ? 'top' : 'left';

    if (this.positionProp === 'top') {
      this.slider.classList.add('plasmic-vertical');
    } else {
      this.slider.classList.remove('plasmic-vertical');
    }
  }

  setSlideClasses(index) {
    this.slides
      .setAttribute('aria-hidden', 'true')
      .classList
        .remove('plasmic-active plasmic-center plasmic-current');

    const each = (start = 0, length = this.slides.length) => {
      this.slides.slice(start, length).forEach(slide => {
        slide.classList.add('plasmic-active');
        slide.setAttribute('aria-hidden', 'false');
      });
    };

    if (this.slides[index]) this.slides[index].classList.add('plasmic-current');

    if (this.options.centerMode) {
      const centerOffset = Math.floor(this.options.slidesToShow / 2);
      if (this.options.infinite) {
        if (index >= centerOffset && index <= (this.slideCount - 1) - centerOffset) {
          each(index - centerOffset, index + centerOffset + 1);
        } else {
          const indexOffset = this.options.slidesToShow + index;
          each(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2);
        }

        if (index === 0) {
          const slide = this.slides[this.slides.length - 1 - this.options.slidesToShow];
          if (slide) slide.classList.add('plasmic-center');
        } else if (index === this.slideCount - 1) {
          const slide = this.slides[this.options.slidesToShow];
          if (slide) slide.classList.add('plasmic-center');
        }

        if (this.slides[index]) this.slides[index].classList.add('plasmic-center');
      }
    } else if (index >= 0 && index <= (this.slideCount - this.options.slidesToShow)) {
      each(index, index + this.options.slidesToShow);
    } else if (this.slides.length <= this.options.slidesToShow) {
      each();
    } else if (!this.options.fill) {
      each(index);
    } else {
      const remainder = this.slideCount % this.options.slidesToShow;
      const indexOffset = this.options.infinite ? this.options.slidesToShow + index : index;
      if (this.options.slidesToShow === this.options.slidesToScroll && (this.slideCount - index) < this.options.slidesToShow) {
        each(indexOffset - (this.options.slidesToShow - remainder), indexOffset + remainder);
      } else {
        each(indexOffset, indexOffset + this.options.slidesToShow);
      }
    }

    if (this.options.lazyLoad === 'ondemand') this.lazyLoad();
  }

  setupInfinite() {
    if (this.options.infinite && this.slideCount > this.options.slidesToShow) {
      const infiniteCount = this.options.slidesToShow + !!this.options.centerMode;
      const uniqueClone = (elem, index, empty = false) => new this.DOMWrapper(elem)
        .cloneNode(true)
        .removeAttribute('id')
        .classList
          .add('plasmic-cloned plasmic-slide')
          .return()
        .setAttribute({
          'data-plasmic-index': index,
          'data-plasmic-empty': empty,
        })[0];

      if (this.options.fill) {
        for (let i = this.slideCount; i > (this.slideCount - infiniteCount); i -= 1) {
          const slideIndex = i - 1;
          this.clonedSlides.push(this.slideTrack.insertBefore(uniqueClone(this.slides[slideIndex], slideIndex - this.slideCount), this.slideTrack.firstElementChild));
        }

        for (let i = 0; i < infiniteCount; i += 1) {
          const slideIndex = i;
          this.clonedSlides.push(this.slideTrack.appendChild(uniqueClone(this.slides[slideIndex], slideIndex + this.slideCount)));
        }
      } else {
        for (let i = 0; i < this.options.slidesToShow; i += 1) {
          const slideIndex = this.slideCount - this.options.slidesToShow + i;
          const shouldBeEmpty = this.slides[slideIndex] && i >= this.options.slidesToShow - this.slideCount % this.options.slidesToShow;

          const slide = this.slideTrack.insertBefore(uniqueClone(!shouldBeEmpty ? document.createElement('div') : this.slides[slideIndex], slideIndex, !shouldBeEmpty), this.slideTrack.firstElementChild);

          this.clonedSlides.push(slide);
          if (!shouldBeEmpty) this.emptySlides.push(slide);
        }

        this.clonedSlides
          .filter(elem => !JSON.parse(elem.dataset.plasmicEmpty))
          .sort((elemA, elemB) => elemA.dataset.plasmicIndex - elemB.dataset.plasmicIndex)
          .forEach((slide, index) => {
            this.slideTrack.insertBefore(slide, this.slideTrack.children[index]);
          });

        for (let slideIndex = 0; slideIndex < this.options.slidesToShow - this.slideCount % infiniteCount; slideIndex += 1) {
          const slide = this.slideTrack.appendChild(uniqueClone(document.createElement('div'), slideIndex - this.slideCount - 1, true));
          this.clonedSlides.push(slide);
          this.emptySlides.push(slide);
        }

        for (let slideIndex = 0; slideIndex < infiniteCount; slideIndex += 1) {
          const cloned = uniqueClone(this.slides[slideIndex], slideIndex + this.slideCount);
          this.clonedSlides.push(this.slideTrack.appendChild(cloned));
        }
      }

      this.clonedSlides.removeAttribute('id');
    }
  }

  slideHandler(index, sync, dontAnimate) {
    sync = sync || false;

    if (this.animating && this.options.waitForAnimate ||
        this.options.fade && this.currentSlide === index ||
        this.slideCount <= this.options.slidesToShow) return;

    if (!sync) this.asNavFor(index);

    let targetSlide = index;
    let animSlide;

    const targetLeft = this.getLeft(targetSlide);
    const slideLeft = this.getLeft(this.currentSlide);
    this.currentLeft = this.swipeLeft === null ? slideLeft : this.swipeLeft;

    if (!this.options.infinite && !this.options.centerMode && (index < 0 || index > this.getDotCount() * this.options.slidesToScroll)) {
      targetSlide = this.currentSlide;
      if (!dontAnimate) {
        this.animateSlide(slideLeft, () => {
          this.postSlide(targetSlide);
        });
      }
      return;
    } else if (!this.options.infinite && this.options.centerMode && (index < 0 || index > (this.slideCount - this.options.slidesToScroll))) {
      targetSlide = this.currentSlide;
      if (!dontAnimate) {
        this.animateSlide(slideLeft, () => {
          this.postSlide(targetSlide);
        });
      }
      return;
    }

    if (this.options.autoplay) this.autoPlay = false;

    if (targetSlide < 0) {
      if (this.slideCount % this.options.slidesToScroll !== 0) {
        animSlide = this.slideCount - (this.slideCount % this.options.slidesToScroll);
      } else {
        animSlide = this.slideCount + targetSlide;
      }
    } else if (targetSlide >= this.slideCount) {
      if (this.slideCount % this.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - this.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }

    this.animating = true;

    this.events(this.slider).trigger('beforeChange', [this, this.currentSlide, animSlide]);

    this.currentSlide = animSlide;

    this.setSlideClasses(this.currentSlide);

    if (this.options.asNavFor) {
      const navTarget = this.getNavTarget();
      navTarget.forEach(nav => {
        const plasmic = globalStorage.get(nav);
        if (plasmic.slideCount <= plasmic.options.slidesToShow) {
          plasmic.setSlideClasses(this.currentSlide);
        }
      });
    }

    this.updateDots();
    this.updateArrows();

    if (!dontAnimate) {
      this.animateSlide(targetLeft, () => {
        this.postSlide(animSlide);
      });
    } else {
      this.postSlide(animSlide);
    }
  }

  supportSlick(src) {
    return supportSlick(src, this.options.slickCompatible);
  }

  plasmicNext() {
    this.changeSlide({
      data: {
        message: 'next',
      },
    });
  }

  progressiveLazyLoad(tryCount = 1) {
    if (this.lazyImages.length) {
      this.lazyImages.forEach(img => {
        const source = img.dataset.lazy;
        const tempImg = DOMWrapper.createElement('img', {
          onerror: () => {
            if (tryCount < 3) {
              /**
               * try to load the image 3 times,
               * leave a slight delay so we don't get
               * servers blocking the request.
               */
              setTimeout(() => {
                this.progressiveLazyLoad(tryCount + 1);
              }, 500);
            } else {
              new this.DOMWrapper(img)
                .removeAttribute('data-lazy')
                .classList
                .remove('plasmic-loading')
                .add('plasmic-lazyload-error');

              this.slider.trigger('lazyLoadError', [this, img, source]);
              this.progressiveLazyLoad();
            }
          },

          onload: () => {
            new this.DOMWrapper(img)
              .setAttribute('src', source)
              .removeAttribute('data-lazy')
              .classList
              .remove('plasmic-loading');
            if (this.options.adaptiveHeight) this.setPosition();
            this.slider.trigger('lazyLoaded', [this, img, source]);
            this.progressiveLazyLoad();
          },
        });
        tempImg.src = source;
      });
    } else {
      this.slider.trigger('allImagesLoaded', [this]);
    }
  }

  get slideCount() {
    return this.slides.length;
  }

  swipeDirection() {
    const rtl = !this.options.rtl;
    const r = Math.atan2(
              this.touchObject.startY - this.touchObject.curY,
              this.touchObject.startX - this.touchObject.curX);
    let swipeAngle = Math.round(r * 180 / Math.PI);
    if (swipeAngle < 0) swipeAngle = 360 - Math.abs(swipeAngle);

    if (swipeAngle <= 45 && swipeAngle >= 0 || swipeAngle <= 360 && swipeAngle >= 315) {
      return rtl ? 'left' : 'right';
    }

    if (swipeAngle >= 135 && swipeAngle <= 225) {
      return rtl ? 'right' : 'left';
    }

    if (this.options.verticalSwiping) {
      return swipeAngle >= 35 && swipeAngle <= 135 ? 'down' : 'up';
    }

    return 'vertical';
  }

  swipeEnd() {
    this.dragging = false;
    this.interrupted = false;
    this.shouldClick = this.touchObject.swipeLength < 10;

    if (this.touchObject.curX === undefined) return false;

    if (this.touchObject.edgeHit) this.slider.trigger('edge', [this, this.swipeDirection()]);

    if (this.touchObject.swipeLength >= this.touchObject.minSwipe) {
      const direction = this.swipeDirection();
      let slideCount;
      switch (direction) {
        case 'left':
        case 'down':
          slideCount = this.options.swipeToSlide ?
                        this.checkNavigable(this.currentSlide + this.getSlideCount()) :
                        this.currentSlide + this.getSlideCount();
          this.currentDirection = 0;
          break;

        case 'right':
        case 'up':
          slideCount = this.options.swipeToSlide ?
                        this.checkNavigable(this.currentSlide - this.getSlideCount()) :
                        this.currentSlide - this.getSlideCount();
          this.currentDirection = 1;
          break;
      }

      if (direction !== 'vertical') {
        this.slideHandler(slideCount);
        this.touchObject = {};
        this.slider.trigger('swipe', [this, direction]);
      }
    } else if (this.touchObject.startX !== this.touchObject.curX) {
      this.slideHandler(this.currentSlide);
      this.touchObject = {};
    }

    return true;
  }

  swipeHandler({ type, originalEvent: source = {}, data }) {
    if (!this.options.swipe || 'ontouchend' in document && !this.options.swipe) return;

    if (!this.options.draggable && type.indexOf('mouse') !== -1) return;

    const event = arguments[0];

    this.touchObject.fingerCount = source.touches !== undefined ? source.touches.length : 1;
    this.touchObject.minSwipe = this.listWidth / this.options.touchThreshold;

    if (this.options.verticalSwiping) {
      this.touchObject.minSwipe = this.listHeight / this.options.touchThreshold;
    }

    switch (data.action) {
      case 'start':
        this.swipeStart(event);
        break;

      case 'move':
        this.swipeMove(event);
        break;

      case 'end':
        this.swipeEnd(event);
        break;
    }
  }

  swipeMove(event) {
    const { clientX, clientY, originalEvent } = event;
    const { touches } = originalEvent || {};
    const curLeft = this.getLeft(this.currentSlide);
    const swipeDirection = this.swipeDirection();
    let swipeLength;
    let positionOffset;

    if (!this.dragging || touches && touches.length !== 1) return false;

    this.touchObject.curX = touches !== undefined ? touches[0].pageX : clientX;
    this.touchObject.curY = touches !== undefined ? touches[0].pageY : clientY;
    this.touchObject.swipeLength = Math.round(
      Math.sqrt(Math.pow(this.touchObject.curX - this.touchObject.startX, 2))
    );

    if (this.options.verticalSwiping === true) {
      this.touchObject.swipeLength = Math.round(
        Math.sqrt(Math.pow(this.touchObject.curY - this.touchObject.startY, 2))
      );
    }

    if (swipeDirection === 'vertical') return false;

    if (originalEvent !== undefined && this.touchObject.swipeLength > 4) {
      event.preventDefault();
    }

    positionOffset = (this.options.rtl === false ? 1 : -1) * (this.touchObject.curX > this.touchObject.startX ? 1 : -1);
    if (this.options.verticalSwiping === true) {
      positionOffset = this.touchObject.curY > this.touchObject.startY ? 1 : -1;
    }

    swipeLength = this.touchObject.swipeLength;

    this.touchObject.edgeHit = false;

    if (this.options.infinite === false) {
      if ((this.currentSlide === 0 && swipeDirection === 'right') || (this.currentSlide >= this.getDotCount() && swipeDirection === 'left')) {
        swipeLength = this.touchObject.swipeLength * this.options.edgeFriction;
        this.touchObject.edgeHit = true;
      }
    }

    if (!this.options.vertical) {
      this.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      this.swipeLeft = curLeft + (swipeLength * (this.$list.height() / this.listWidth)) * positionOffset;
    }

    if (this.options.verticalSwiping) {
      this.swipeLeft = curLeft + swipeLength * positionOffset;
    }

    if (this.options.fade || !this.options.touchMove) return false;

    if (this.animating) {
      this.swipeLeft = null;
      return false;
    }

    this.setCSS(this.swipeLeft);
    return true;
  }

  swipeStart({ originalEvent, clientX, clientY }) {
    let pageX;
    let pageY;

    this.interrupted = true;

    if (this.touchObject.fingerCount !== 1 || this.slideCount <= this.options.slidesToShow) {
      this.touchObject = {};
      return false;
    }

    if (originalEvent !== undefined && originalEvent.touches !== undefined) {
      const touches = originalEvent.touches[0];
      pageX = touches.pageX;
      pageY = touches.pageY;
    }

    this.touchObject.startX = this.touchObject.curX = pageX || clientX;
    this.touchObject.startY = this.touchObject.curY = pageY || clientY;

    this.dragging = true;
    return true;
  }

  startLoad() {
    if (this.options.arrows && this.slideCount > this.options.slidesToShow) {
      this.prevArrow.style.display = 'none';
      this.nextArrow.style.display = 'none';
    }

    if (this.options.dots && this.slideCount > this.options.slidesToShow) {
      this.dotsContainer.style.display = 'none';
    }

    this.slider.classList.add('plasmic-loading');
  }

  trigger(...args) {
    return this.events(this.slider).trigger(...args);
  }

  updateArrows() {
    if (this.options.arrows && (this.slideCount > this.options.slidesToShow) && !this.options.infinite) {
      const setAttrs = ([prevClass, prevAria], [nextClass, nextAria]) => {
        this.prevArrow
          .setAttribute('aria-disabled', `${prevAria}`)
          .classList[prevClass ? 'add' : 'remove']('plasmic-disabled');
        this.nextArrow
          .setAttribute('aria-disabled', `${nextAria}`)
          .classList[nextClass ? 'add' : 'remove']('plasmic-disabled');
      };
      setAttrs([false, false], [false, false]);

      if (this.currentSlide === 0) {
        setAttrs([true, true], [false, false]);
      } else if (this.currentSlide >= this.slideCount - this.options.slidesToShow && !this.options.centerMode) {
        setAttrs([false, false], [true, true]);
      } else if (this.currentSlide >= this.slideCount - 1 && this.options.centerMode) {
        setAttrs([false, false], [true, true]);
      }
    }
  }

  updateDots() {
    if (this.dots.length) {
      this.dots.forEach((dot, n) => {
        if (Math.floor(this.currentSlide / this.options.slidesToScroll) === n) {
          dot.classList.add('plasmic-active');
          dot.setAttribute('aria-hidden', 'false');
        } else {
          dot.classList.remove('plasmic-active');
          dot.setAttribute('aria-hidden', 'true');
        }
      });
    }
  }

  unload() {
    this.clonedSlides.remove();
    this.dotsContainer.remove();
    this.prevArrow.remove();
    this.nextArrow.remove();

    this.slides
      .setAttribute('aria-hidden', 'true')
      .classList
        .remove('plasmic-slide plasmic-active plasmic-visible plasmic-current')
        .return()
      .style
        .width = '';
  }

  visibility() {
    if (this.options.autoplay) {
      this.interrupted = document[this.hidden];
    }
  }
}
