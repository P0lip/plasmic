/**
 * Module contains helpers to provide some backwards compatibility with Slick.
 * @module ./legacy.es6
 */
const newPrefix = /plasmic/g;
const legacyPrefix = 'slick';
const whitespaces = /\s+/;

/**
 * supportSlick() - transform a given string (a CSS class), by adding 'slick' prefixes in case there are 'plasmic' prefixes found.
 *
 * @param  {String}   str    - string to be transformed
 * @param  {Boolean}  legacy - indicated whether the transformation should be performed
 * @return {String}   transformed string
 */
export function supportSlick(str, legacy) {
  if (!legacy || typeof str !== 'string') return str;

  return str
    .split(whitespaces)
    .map(part => `${part} ${newPrefix.test(part) ? part.replace(newPrefix, legacyPrefix) : ''}`)
    .join(' ');
}

/**
 * hasjQuery() - detects whether jQuery is available
 *
 * @return {Boolean}
 */
export const hasjQuery = () => typeof jQuery === 'function' &&
                              jQuery().jquery &&
                              typeof jQuery.fn === 'object';