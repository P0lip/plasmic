/**
 * Event handling module. It allows any object or function to communicate by sending events.
 * When possible, a native handling will be used (DOM events).
 * @module ./events.es6
 */
import { WeakStorage, tryCatch } from './utils.es6';
import { isHTMLElement } from './helpers.es6';

const whitespaces = /\s+/;

export class EventsHandler {
  /**
   * Handler.constructor();
   * @constructor
   *
   * @param {Object|Function|HTMLElement} target    - object event handling will be attached to.
   * @param {Boolean}                     localOnly - indicated whether the native events (DOM events) will be dispatched
   */
  constructor(target, localOnly) {
    if (!target || typeof target !== 'function' && typeof target !== 'object') {
      throw new TypeError('Invalid elem passed');
    }

    this.target = target;
    this.isElement = isHTMLElement(target);
    this.listeners = {};
    this.localStorage = new WeakStorage();
    this.localOnly = localOnly && this.isElement;
  }

  /**
   * EventsHandler.on() - adds an event listener.
   * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
   * @type {Function} listener - function called once an event was triggered
   * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
   * @return {Handler}
   */
  on(event, listener, data = {}) {
    EventsHandler.parseEvents(event).forEach(({ original, formatted }) => {
      const _this = this;
      const matchedListeners = this.localStorage.get(listener);
      const boundListener = function (e = {}, ...args) {
        if (!_this.localOnly) e.data = data;
        listener.call(this, e, ...args);
      };

      if (!matchedListeners) this.localStorage.set(listener, []);
      this.localStorage.get(listener).push(boundListener);
      (this.listeners[original] || (this.listeners[original] = [])).push(boundListener);
      if (this.isElement && !this.localOnly) {
        this.target.addEventListener(formatted, boundListener);
      }
    });

    return this;
  }

  /**
   * Handler.once() - adds an event listener that will be removed once it is called for a first time
   * It takes the same arguments as the Handler.on() method.
   *
   * @type {String}   event    - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
   * @type {Function} listener - function called once an event was triggered
   * @type {Object}   [data]   - additional data passed to the listener (as its first argument which is likely to be either an Event object or object literal)
   * @return {Handler}
   */
  once(event, listener, data) {
    const callback = () => {
      listener();
      this.off(event, callback);
    };

    return this.on(event, callback, data);
  }

  /**
   * EventsHandler.off() - removes a previously attached listener or listeners
   *
   * @type {String}   event     - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
   * @type {Function} [listener] - when defined, function will try to detach only that linked listener. Otherwise it removes all connected listeners.
   * @return {Handler}
   */
  off(event, listener) {
    EventsHandler.parseEvents(event).forEach(({ original, formatted }) => {
      const listeners = this.listeners[original];
      if (!listeners) return;

      if (!listener) {
        let listener;
        while (listener = listeners.shift()) {
          if (!this.localOnly) this.target.removeEventListener(formatted, listener);
        }
      } else {
        const matchedListeners = this.localStorage.get(listener);
        if (matchedListeners) {
          matchedListeners.forEach(matchedListener => {
            const index = listeners.indexOf(matchedListener);
            if (!this.localOnly) this.target.removeEventListener(formatted, matchedListener);
            if (index !== -1) listeners.splice(index, 1);
          });
        }
      }
    });
    return this;
  }

  /**
   * EventsHandler.parseEvents() - parses given string and returns an array with formatted events' names.
   *
   * @param  {String} events - string containing an event (or events) name. May inlcude a bunch of events seperated by any whitespace character, as well as namespaces preceded by a dot.
   * @return {Array}  formattedEvents - array containing object literal with parsed event.
   */
  static parseEvents(events) {
    if (typeof events !== 'string') return [];

    const split = events
                      .split(whitespaces)
                      .filter(event => event.length); // filter out empty strings
    const formatted = new Array(split.length);
    split.forEach((event, i) => {
      formatted[i] = {
        original: event,
        formatted: event.split('.')[0], // event without the namespace
      };
    });
    return formatted;
  }

  /**
   * EventsHandler.trigger() - triggers each listener connected with a specified event.
   * It worth to mention that it **does not** fire the DOM event, so any listeners attached not using the built-in Handler's methods will not be executed.
   *
   * @type {String}   event  - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
   * @type {*}        [args] - additional arguments passed to the function
   * @return {Handler}
   */
  trigger(event, args) {
    EventsHandler.parseEvents(event).forEach(({ original }) => {
      (this.listeners[original] || []).forEach(listener => {
        tryCatch(() => {
          listener.apply(this.target, args);
        });
      });
    });
    return this;
  }

  /**
   * EventsHandler.trigger() - triggers each listener connected with a specified event using dispatchEvent.
   * Unlike Handler.trigger() any listeners attached using addEventListener() will be executed.
   * Currently there is no support for optional arguments.
   *
   * @type {String}   event      - string used as an event. Passed string must be parsable by the events.parseEvents() method, so refer to its docs (stated at the bottom) for a proper. usage.
   * @return {Handler}
   */
  dispatchEvent(event) {
    if (!this.localOnly) {
      EventsHandler.parseEvents(event).forEach(({ formatted }) => {
        this.target.dispatchEvent(new Event(formatted)); // FIXME: bail out in IE
      });
    }

    return this;
  }
}

export default function () {
  const storage = new WeakStorage();
  /**
   * This function constructs Handler, so the meaning of arguments its takes is defined in the Handler's constructor.
   * @return {EventsHandler}
   */
  return (obj, localOnly) => {
    if (storage.has(obj)) return storage.get(obj);

    const handler = new EventsHandler(obj, localOnly);
    storage.set(obj, handler);
    return handler;
  };
}
