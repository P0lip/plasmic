export const isHTMLElement = element => element instanceof Element;

export const isNode = node => node instanceof Node;

export const dimensions = {
  height(elem, includingMargin) {
    if (!elem || !elem.nodeType) return -1;
    let { offsetHeight: height } = elem;
    if (includingMargin) {
      const { marginTop, marginBottom } = window.getComputedStyle(elem);
      height += parseInt(marginTop, 10) + parseInt(marginBottom, 10);
    }
    return height;
  },

  width(elem, withoutPadding) {
    if (!elem || !elem.nodeType) return -1;
    let { offsetWidth: width } = elem;
    if (withoutPadding) {
      const { paddingLeft, paddingRight } = window.getComputedStyle(elem);
      width -= parseInt(paddingLeft, 10) + parseInt(paddingRight, 10);
    }
    return width;
  },
};

export function isCSSFeatureSupported(rule, value) {
  if (window.CSS && window.CSS.supports) return window.CSS.supports(rule, value);

  const elem = document.body.appendChild(document.createElement('div'));
  elem.style[rule] = value;
  const supported = elem.style[rule] && elem.style[rule] !== '' && elem.style[rule] !== null;
  document.body.removeChild(elem);
  return supported;
}

export const cssProps = {
  transform: isCSSFeatureSupported(
              'transform',
              'translate3d(0,0,0)') ? 'transform' : '-webkit-transform',
  transition: 'transition',
};
