/**
 * Utils module containing methods used across the entire project.
 * @module ./utils.es6
 * @description
 * Functions defined in this module are written at my own, thus there is a test suite, where you can test whether they cause troubles in a given browser.
 */

/**
 * assign() - for docs, refer to https://developer.mozilla.org/pl/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 * This function uses a built-in method Object.assign, but when there is is lacking, it falls back to a similar method.
 * The mentioned method works quite similarly, yet it doesn't follow the specification, so there might be some unexpected behaviors.
 */
export function assign(target, ...args) {
  const filteredArgs = args.filter(arg => typeof arg === 'object' && arg !== null);
  if (Object.assign) {
    Object.assign(target, ...filteredArgs);
  } else {
    args.forEach(arg => {
      Object.keys(arg).forEach(key => {
        target[key] = filteredArgs[key];
      });
    });
  }

  return target;
}

/**
 * tryCatch() - a small wrapper for try catch used by other functions.
 * The reason to use this method is that V8 (as of now) doesn't support try catch pattern, therefore causing each function containg try catch to be deopt (not able to be optimized).
 *
 * @param  {Function} _try     -  function used in a try statement
 * @param  {Function} [_catch] - optional function called when an exception is thrown
 */
export function tryCatch(_try, _catch) {
  try {
    _try();
  } catch (ex) {
    if (typeof _catch === 'function') _catch(ex);
  }
}

/**
 * extendPrototype() - copies properties from one prototype to the target prototype.
 * It does copy all enumerable properties, as it uses Object.getOwnPropertyNames to list props.
 * The function omits copying properties that already exist in the target prototype.
 * Each getter and setter, when found, is preserved.
 *
 * @param  {Object|Function} target  - target function (or object)
 * @param  {Object} target.prototype - a prototype of given target
 * @param  {Object|Function} source  - source function whose prototype will be copied to the target function
 * @param  {Object} source.prototype - a prototype of given source
 * @param  {Object} traps
 *
 */
export function extendPrototype(
  { prototype: targetProto },
  { prototype: sourceProto },
  { creation, finish, get: getTrap, set: setTrap, value: valueTrap, type }) {

  const isPropAvailable = (prop, obj) => prop in obj;

  function handleTrap(func, prop, trap, ...args) {
    const results = [];
    const _func = function () {
      if (isPropAvailable(prop, this)) {
        const result = func.call(this, ...args);
        if (typeof finish === 'function')
          results.push(result);
        return typeof trap === 'function' ? trap(result) : result;
      }
    };

    if (type === 'array') {
      this.forEach(elem => {
        _func.call(elem);
      });
      if (typeof finish === 'function') return finish.call(this, results);
    }

    return _func.call(this);
  }

  Object.getOwnPropertyNames(sourceProto).forEach(prop => {
    if (typeof creation === 'function' && !creation(prop)) return;

    const { get, set, value } = Object.getOwnPropertyDescriptor(sourceProto, prop);
    if (prop in targetProto) return;
    if (get) {
      Object.defineProperty(targetProto, prop, {
        enumerable: true,
        get() {
          if (get) {
            return handleTrap.call(this, get, prop, getTrap);
          }
        },

        set(value) {
          if (set) {
            return handleTrap.call(this, set, prop, setTrap, value);
          }
        },
      });
    } else if (value) {
      Object.defineProperty(targetProto, prop, {
        writable: true,
        value(...args) {
          return handleTrap.call(this, value, prop, valueTrap, ...args);
        },
      });
    }
  });
}

/**
 * A shim for WeakMap. It doesn't support any iterator methods, as well as it isn't spec-compliant.
 * It was primarily designed to use for events handler and connecting Plasmic instances with corresponding elements existing in DOM.
 */
export const WeakStorage = WeakMap || class {
  constructor() {
    this.knownIds = [];
    this.currentId = '';
    this.ensureUniq();
    this.Data = function (data) {
      this.data = data;
    };
  }

  /**
   * WeakStorage.ensureUniq() - ensures the uniqnuess of given key.
   *
   * @param {Object|Function} key
   * @return {String} currentId
   */
  ensureUniq(key) {
    const id = Math.random().toString(32).slice(2) + Math.random().toString(32).slice(2);
    if (!key || this.currentId in key && !(key[this.currentId] instanceof this.data.constructor)) {
      this.currentId = id;
      this.knownIds.push(id);
    }

    return this.currentId;
  }

  /**
   * WealStorage.ensureKey - checkes whether given key is valid.
   * A valid key is either an object or a function.
   *
   * @param {*} key
   */
  ensureKey(key) {
    if (typeof key !== 'object' && typeof key !== 'function' || key === null) {
      throw new TypeError('Key is invalid');
    }
  }

  has(key, returnId) {
    this.ensureKey(key);
    let foundId;
    this.knownIds
      .every(id => id in key && key[id] instanceof this.data ? (foundId = id, false) : true);
    return returnId ? foundId : foundId !== undefined;
  }

  set(key, value) {
    this.ensureKey(key);
    const id = this.ensureUniq(key);
    Object.defineProperty(key, id, {
      value: Object.freeze(new this.Data(value)),
    });
  }

  get(key) {
    this.ensureKey(key);
    const foundKey = this.has(key, true);
    if (foundKey) return key[foundKey].data;
  }

  delete(key) {
    this.ensureKey(key);
    const foundKey = this.has(key, true);
    if (foundKey) {
      delete key[foundKey];
      return true;
    }

    return false;
  }
};