/*jshint node: true */
'use strict';
const gulp = require('gulp');
const babel = require('gulp-babel');

gulp.task('default', function () {
  gulp.start('tests');
  gulp.start('watch');
});

gulp.task('watch', function () {
  gulp.watch('src/*', ['tests']);
});

gulp.task('tests', function () {
	return gulp.src('src/*.es6')
    .pipe(babel())
		.pipe(gulp.dest('dist/tests'));
});