/* jshint mocha: true, node: true */
"use strict";
const chai = require('chai');
const { tryCatch } = require('../src/utils.es6');

const { assert } = chai;

describe('Utils', function () {
  describe('#assign()', function () {
  });
  describe('#tryCatch()', function () {
    it('should call the try function', function () {
      let called = false;
      tryCatch(function () {
        called = true;
      });

      assert.equal(called, true);
    });

    it('shouldn\'t call catch when the try succeeded', function () {
      let called = false;
      tryCatch(Function, function () {
        called = true;
      });

      assert.equal(called, false);
    });

    it('should call catch when the try failed', function () {
      let called = false;
      tryCatch(function () {
        throw new Error();
      },
      function () {
        called = true;
      });

      assert.equal(called, true);
    });
  });
});